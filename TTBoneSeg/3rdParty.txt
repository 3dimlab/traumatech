Used 3rd Party Libraries
------------------------

Here is a list of all external 3rd party libraries used by the project TraumaTech:

- 3DimViewer (https://bitbucket.org/3dimlab/3dimviewer)
- VPL (https://bitbucket.org/3dimlab/vpl)
- Qt Toolkit (http://qt.nokia.com/products/)
- OpenCV (http://opencv.org/)
- DCMTk toolkit (http://dicom.offis.de/dcmtk)

Some of these libraries might be distributed under a more restrictive license
then the TraumaTech one. Please see licenses of individual 3rd party libraries
for more details.
