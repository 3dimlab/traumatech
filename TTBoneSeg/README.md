**********************************************************************
* This file is part of
* 
* TraumaTech - Computer-based Planning in Trauma Treatment
* Copyright 2015 3Dim Laboratory s.r.o.
* All rights reserved.
* 
* This software was created with the support of TA CR (https://www.tacr.cz/)
* under project number TA04011606.
* 
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
* 
*   http://www.apache.org/licenses/LICENSE-2.0
* 
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
**********************************************************************

Table of Contents

1. What is TTBoneSeg?
    1. Basic Features
2. Installation
    1. Compilation using MS Visual Studio
3. Bug Reporting
4. References
5. Contacts


1 What is TTBoneSeg?
====================

1.1 Basic Features

Application implements various edge detection techniques. Current version
of the application applies basic image preprocessing and then uses 
advanced techniques based on superpixels and structured forests for edge 
detection.

---

2 Installation
=============

2.1 Compilation using MS Visual Studio

Build as a VPL module. VPL repository can be found at
https://bitbucket.org/3dimlab/vpl
Disable default OpenCV cmake files for compilation with OpenCV 3.

---

3 Bug Reporting
===============

We don't promise that this software is completely bug free. If you encounter
any issue, please let us now.

* Report the bug using 
  [Bitbucket's issue tracker](https://bitbucket.org/3dimlab/traumatech/issues),
* mail your bug reports to info(at)3dim-laboratory.cz,

or

* fork the repository, fix it, and send us a pull request.


---

4 References
============

[1] Comparison of State-Of-The-Art Superpixel Algorithms
http://davidstutz.de/projects/superpixelsseeds/

[2] SLIC Superpixels Compared to State-of-the-art Superpixel Methods
http://infoscience.epfl.ch/record/177415/files/Superpixel_PAMI2011-2.pdf

[3] Efficient High-Quality Superpixels: SEEDS Revised
http://davidstutz.de/efficient-high-quality-superpixels-seeds-revised/

[4] Structured Forests for Fast Edge Detection
http://research.microsoft.com/pubs/202540/DollarICCV13edges.pdf

---

5 Contacts
==========

* 3Dim Laboratory s.r.o.
* E-mail: info@3dim-laboratory.cz
* Web: http://www.3dim-laboratory.cz/
