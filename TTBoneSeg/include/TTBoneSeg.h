/**********************************************************************
* This file is part of
* 
* TraumaTech - Computer-based Planning in Trauma Treatment
* Copyright 2015 3Dim Laboratory s.r.o.
* All rights reserved.
* 
* This software was created with the support of TA CR (https://www.tacr.cz/)
* under project number TA04011606.
* 
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
* 
*   http://www.apache.org/licenses/LICENSE-2.0
* 
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
**********************************************************************/

#ifndef VPL_SLICEBONEEDGES_H
#define VPL_SLICEBONEEDGES_H

#include <VPL/Module/Module.h>


//==============================================================================
/*!
 * Module loads an image in DICOM format and applies advanced edge detection technique
 */
class CSliceBoneEdges : public vpl::mod::CModule
{
public:
    //! Smart pointer type
    //! - Declares type tSmartPtr
    VPL_SHAREDPTR(CSliceBoneEdges);

public:
    //! Default constructor
    CSliceBoneEdges(const std::string& sDescription);

    //! Virtual destructor
    virtual ~CSliceBoneEdges();

protected:
    //! Command line arguments
    int m_nAlgorithm, m_nFilter;

    //! Virtual method called on startup
    virtual bool startup();

    //! Virtual method called by the processing thread
    virtual bool main();

    //! Called on console shutdown
    virtual void shutdown();

    //! Called on writing a usage statement
    virtual void writeExtendedUsage(std::ostream& Stream);
};


//==============================================================================
/*!
 * Smart pointer to console application.
 */
typedef CSliceBoneEdges::tSmartPtr   CSliceBoneEdgesPtr;


#endif // VPL_SLICEBONEEDGES_H

