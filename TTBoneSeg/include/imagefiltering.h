///////////////////////////////////////////////////////////////////////////////
//
// TraumaTech - Modern Image Processing Techniques and Computer-based Planning in Trauma Treatment (TA04011606)
//
// Copyright 2015 3Dim Laboratory s.r.o.
//
// This software was created with financial support from TA CR (https://www.tacr.cz/).
//
///////////////////////////////////////////////////////////////////////////////

#ifndef IMAGEFILTERING_H
#define IMAGEFILTERING_H

#include <VPL/Image/ImageFilter.h>


namespace vpl
{
namespace img
{

//==============================================================================
/*!
 * Sobel operator (horizontal version).
 * - Parameter I is a used image type.
 * - Kernel definition: \n
 *   1 [ -1  0  1 ] \n
 *   - [ -2  0  2 ] \n
 *   4 [ -1  0  1 ] \n
 */
template <class I, template <typename> class N = CDefaultFilterPolicy>
class CSobel : public CNormImageFilter<I,N>
{
public:
    //! Image filter base.
    typedef CNormImageFilter<I,N> base;
    typedef typename base::tImage tImage;
    typedef typename base::tPixel tPixel;
    typedef typename base::tResult tResult;

    //! Number used to divide filter response
	const double m_denom;

public:
    //! Default constructor.
    CSobel(double denom = 4) : m_denom ( 1.0/denom) { }

    //! Destructor.
    ~CSobel() {}

    //! Filtering of input/source image. Point filter responses are written
    //! to the destination image.
    //! - Returns false on failure.
    bool operator()(const tImage& SrcImage, tImage& DstImage);

    //! Returns filter response at specified image point.
    //! - Value is not normalized!
    inline tResult getHorizontalResponse(const tImage& SrcImage, tSize x, tSize y);

	//! Returns filter response at specified image point.
    //! - Value is not normalized!
    inline tResult getVerticalResponse(const tImage& SrcImage, tSize x, tSize y);

	//! Returns filter response at specified image point.
    //! - Value is not normalized!
    inline tResult getDiagonalResponse(const tImage& SrcImage, tSize x, tSize y);

	//! Returns filter response at specified image point.
    //! - Value is not normalized!
    inline tResult getAntiDiagonalResponse(const tImage& SrcImage, tSize x, tSize y);

    //! Returns filter standard deviation.
    tSize getSize() const { return 3; }
};




template <typename I, template <typename> class N>
bool CSobel<I,N>::operator()(const tImage& SrcImage, tImage& DstImage)
{
    // Image size
    tSize XCount = vpl::math::getMin(SrcImage.getXSize(), DstImage.getXSize());
    tSize YCount = vpl::math::getMin(SrcImage.getYSize(), DstImage.getYSize());

    // Filter the image
#pragma omp parallel for schedule(static) default(shared)
    for( tSize y = 0; y < YCount; ++y )
    {
        for( tSize x = 0; x < XCount; ++x )
        {
            tResult hor = getHorizontalResponse(SrcImage, x, y);
			tResult ver = getVerticalResponse(SrcImage, x, y);
			tResult diag = getDiagonalResponse(SrcImage, x, y);
			tResult anti = getAntiDiagonalResponse(SrcImage, x, y);
			tResult Value = ver;
			if (::abs(hor)>::abs(Value))
				Value = hor;
			if (::abs(diag)>::abs(Value))
				Value = diag;
			if (::abs(anti)>::abs(Value))
				Value = anti;
            DstImage.set(x, y, base::normalize(Value));
        }
    }

    // O.K.ce
    return true;
}

// Image filter response
template <typename I, template <typename> class N>
inline typename CSobel<I,N>::tResult CSobel<I,N>::getHorizontalResponse(const tImage& SrcImage, tSize x, tSize y)
{
    // Compute filter response
    tResult Value = (1/6.0) * (                        
						+ tResult(SrcImage(x + 2, y - 2))
                        + tResult(SrcImage(x + 2, y - 1))
						+ 2 * tResult(SrcImage(x + 2, y))
                        + tResult(SrcImage(x + 2, y + 1))                        
						+ tResult(SrcImage(x + 2, y + 2))
						- tResult(SrcImage(x - 2, y - 2))
                        - tResult(SrcImage(x - 2, y - 1))
						- 2 * tResult(SrcImage(x - 2, y))
                        - tResult(SrcImage(x - 2, y + 1))
						- tResult(SrcImage(x - 2, y + 2))
                        );
    return Value;
}

// Image filter response
template <typename I, template <typename> class N>
inline typename CSobel<I,N>::tResult CSobel<I,N>::getVerticalResponse(const tImage& SrcImage, tSize x, tSize y)
{
    // Compute filter response
    tResult Value = (1/6.0) * (
						+ tResult(SrcImage(x - 2, y + 2))
						+ tResult(SrcImage(x - 1, y + 2))
                        + 2 * tResult(SrcImage(x, y + 2))
                        + tResult(SrcImage(x + 1, y + 2))
                        + tResult(SrcImage(x + 2, y + 2))
						- tResult(SrcImage(x + 2, y - 2))
						- tResult(SrcImage(x + 1, y - 2))
                        - 2 * tResult(SrcImage(x, y - 2))                        
                        - tResult(SrcImage(x - 1, y - 2))
						- tResult(SrcImage(x - 2, y - 2))
                     );
    return Value;
}

// Image filter response
template <typename I, template <typename> class N>
inline typename CSobel<I,N>::tResult CSobel<I,N>::getDiagonalResponse(const tImage& SrcImage, tSize x, tSize y)
{
    // Compute filter response
    tResult Value = m_denom * (
                        + tResult(SrcImage(x + 1, y ))
                        + 2 * tResult(SrcImage(x + 1, y + 1))
                        + tResult(SrcImage(x , y + 1))
                        - tResult(SrcImage(x - 1 , y))
                        - tResult(SrcImage(x , y - 1))
                        - 2* tResult(SrcImage(x - 1, y - 1))
                     );
    return Value;
}

// Image filter response
template <typename I, template <typename> class N>
inline typename CSobel<I,N>::tResult CSobel<I,N>::getAntiDiagonalResponse(const tImage& SrcImage, tSize x, tSize y)
{
    // Compute filter response
    tResult Value = m_denom * (
                        + tResult(SrcImage(x + 1, y ))
                        + 2 * tResult(SrcImage(x + 1, y - 1))
                        + tResult(SrcImage(x , y - 1))
                        - tResult(SrcImage(x - 1 , y))
                        - tResult(SrcImage(x , y + 1))
                        - 2* tResult(SrcImage(x - 1, y + 1))
                     );
    return Value;
}


///////////////////////////////////////////////////////////////////////////////

template <class I, template <typename> class N = CDefaultFilterPolicy>
class CBoxFilter : public CNormImageFilter<I,N>
{
public:
    //! Image filter base.
    typedef CNormImageFilter<I,N> base;
    typedef typename base::tImage tImage;
    typedef typename base::tPixel tPixel;
    typedef typename base::tResult tResult;

public:
    //! Default constructor.
    CBoxFilter() { }

    //! Destructor.
    ~CBoxFilter() {}

    //! Filtering of input/source image. Point filter responses are written
    //! to the destination image.
    //! - Returns false on failure.
    bool operator()(const tImage& SrcImage, tImage& DstImage);

    //! Returns filter response at specified image point.
    //! - Value is not normalized!
	inline tResult getHorizontalResponse(const tImage& SrcImage, tSize x, tSize y);
	inline tResult getVerticalResponse(const tImage& SrcImage, tSize x, tSize y);

    //! Returns filter standard deviation.
    tSize getSize() const { return 3; }
};


template <typename I, template <typename> class N>
bool CBoxFilter<I,N>::operator()(const tImage& SrcImage, tImage& DstImage)
{
    // Image size
    tSize XCount = vpl::math::getMin(SrcImage.getXSize(), DstImage.getXSize());
    tSize YCount = vpl::math::getMin(SrcImage.getYSize(), DstImage.getYSize());

	tImage tmpDst(DstImage);
    // Filter the image
#pragma omp parallel for schedule(static) default(shared)
    for( tSize y = 0; y < YCount; ++y )
    {
        for( tSize x = 0; x < XCount; ++x )
        {
            tResult Value = getHorizontalResponse(SrcImage, x, y);
            tmpDst.set(x, y, base::normalize(Value));
        }
    }

#pragma omp parallel for schedule(static) default(shared)
	for( tSize y = 0; y < YCount; ++y )
    {        
		for( tSize x = 0; x < XCount; ++x )    
        {
            tResult Value = getVerticalResponse(tmpDst, x, y);
            DstImage.set(x, y, base::normalize(Value));
        }
    }

    // O.K.ce
    return true;
}

// Image filter response
template <typename I, template <typename> class N>
inline typename CBoxFilter<I,N>::tResult CBoxFilter<I,N>::getHorizontalResponse(const tImage& SrcImage, tSize x, tSize y)
{
    // Compute filter response
    tResult Value = (1/3.0) * (                        
						+ tResult(SrcImage(x - 1, y ))
						+ tResult(SrcImage(x	, y ))
						+ tResult(SrcImage(x + 1, y ))
                        );
    return Value;
}


// Image filter response
template <typename I, template <typename> class N>
inline typename CBoxFilter<I,N>::tResult CBoxFilter<I,N>::getVerticalResponse(const tImage& SrcImage, tSize x, tSize y)
{
    // Compute filter response
    tResult Value = (1/3.0) * (                        
						+ tResult(SrcImage(x , y - 1 ))
						+ tResult(SrcImage(x , y ))
						+ tResult(SrcImage(x , y + 1 ))
                        );
    return Value;
}



}
}


vpl::img::CDImage* getEdgeMap(vpl::img::CDImage* spRTGSlice);

#endif