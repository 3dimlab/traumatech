///////////////////////////////////////////////////////////////////////////////
//
// TraumaTech - Modern Image Processing Techniques and Computer-based Planning in Trauma Treatment (TA04011606)
//
// Copyright 2015 3Dim Laboratory s.r.o.
//
// This software was created with financial support from TA CR (https://www.tacr.cz/).
//
///////////////////////////////////////////////////////////////////////////////

#include "imagefiltering.h"
#include <VPL/Image/ImageFunctions.h>
//#include <VPL/Image/ImageFiltering.h>
//#include <VPL/Image/Filters/Median.h>
#include <VPL/Image/Filters/Gaussian.h>
#include <VPL/Image/Filters/Sobel.h>
#include <VPL/Image/VolumeHistogram.h>
#include <VPL/System/Sleep.h>

#include <Eigen/SVD>


#undef assert
#define assert(x) if (!(x)) __debugbreak();


vpl::img::CDImage* getEdgeMap(vpl::img::CDImage* spRTGSlice)
{
	vpl::img::CDImage *pDiffData = new vpl::img::CDImage(spRTGSlice->getSize(), 15);
	*pDiffData = *spRTGSlice;
	pDiffData->mirrorMargin();
	vpl::img::CSobel<vpl::img::CDImage> sobel;
	vpl::img::CDImage tmp(*pDiffData);
	sobel(*pDiffData,tmp);
//	tmp = tmp.abs();
    vpl::img::CGaussFilter<vpl::img::CDImage> Filter((vpl::tSize)15);
	Filter(tmp, *pDiffData);
	if(1)
	{
		vpl::img::CBoxFilter<vpl::img::CDImage> box;
        vpl::img::CGaussFilter<vpl::img::CDImage> FilterS((vpl::tSize)9);
		FilterS(tmp, tmp);
		*pDiffData /= vpl::CScalari(8);
		tmp /= vpl::CScalari(16);
		//box(tmp, tmp);
		//box(tmp, tmp);

		*pDiffData *= tmp;	
	}
	return pDiffData;
}





#define PIX_SWAP(a,b) { unsigned int temp=(a);(a)=(b);(b)=temp; }
#define PIX_SORT(a,b) { if ((a)>(b)) PIX_SWAP((a),(b)); }

unsigned int opt_med9(unsigned int* p)
{
	PIX_SORT(p[1], p[2]) ; PIX_SORT(p[4], p[5]) ; PIX_SORT(p[7], p[8]) ;
	PIX_SORT(p[0], p[1]) ; PIX_SORT(p[3], p[4]) ; PIX_SORT(p[6], p[7]) ;
	PIX_SORT(p[1], p[2]) ; PIX_SORT(p[4], p[5]) ; PIX_SORT(p[7], p[8]) ;
	PIX_SORT(p[0], p[3]) ; PIX_SORT(p[5], p[8]) ; PIX_SORT(p[4], p[7]) ;
	PIX_SORT(p[3], p[6]) ; PIX_SORT(p[1], p[4]) ; PIX_SORT(p[2], p[5]) ;
	PIX_SORT(p[4], p[7]) ; PIX_SORT(p[4], p[2]) ; PIX_SORT(p[6], p[4]) ;
	PIX_SORT(p[4], p[2]) ; return(p[4]) ;
}

