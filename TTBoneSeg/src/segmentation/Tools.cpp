/**
 * Implementation of the superpixel algorithm called SEEDS [1] described in
 * 
 *  [1] M. van den Bergh, X. Boix, G. Roig, B. de Capitani, L. van Gool.
 *      SEEDS: Superpixels extracted via energy-driven sampling.
 *      Proceedings of the European Conference on Computer Vision, pages 13–26, 2012.
 * 
 * If you use this code, please cite [1] and [2]:
 * 
 *  [2] D. Stutz, A. Hermans, B. Leibe.
 *      Superpixel Segmentation using Depth Information.
 *      Bachelor thesis, RWTH Aachen University, Aachen, Germany, 2014.
 * 
 * [2] is available online at 
 * 
 *      http://davidstutz.de/bachelor-thesis-superpixel-segmentation-using-depth-information/
 * 
 * Note that all results published in [2] are based on an extended version
 * of the Berkeley Segmentation Benchmark [3], the Berkeley Segmentation
 * Dataset [3] and the NYU Depth Dataset [4].
 * 
 * [3] P. Arbeláez, M. Maire, C. Fowlkes, J. Malik.
 *     Contour detection and hierarchical image segmentation.
 *     Transactions on Pattern Analysis and Machine Intelligence, 33(5):898–916, 2011.
 * [4] N. Silberman, D. Hoiem, P. Kohli, R. Fergus.
 *     Indoor segmentation and support inference from RGBD images.
 *     Proceedings of the European Conference on Computer Vision, pages 746–760, 2012.
 * 
 * The extended version of the Berkeley Segmentation Benchmark will be
 * published on GitHub [6]:
 * 
 *  [5] https://github.com/davidstutz
 * 
 * The code is published under the BSD 3-Clause:
 * 
 * Copyright (c) 2014, David Stutz
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#ifdef OPENCV_AVAILABLE

#include "segmentation/Tools.h"
#include "segmentation/SeedsRevised.h"

cv::Mat Draw::contourImage(int** labels, const cv::Mat &image, int* bgr) {
    
    cv::Mat newImage = image.clone();
    
    int label = 0;
    int labelTop = -1;
    int labelBottom = -1;
    int labelLeft = -1;
    int labelRight = -1;
    
    for (int i = 0; i < newImage.rows; i++) {
        for (int j = 0; j < newImage.cols; j++) {
            
            label = labels[i][j];
            
            labelTop = label;
            if (i > 0) {
                labelTop = labels[i - 1][j];
            }
            
            labelBottom = label;
            if (i < newImage.rows - 1) {
                labelBottom = labels[i + 1][j];
            }
            
            labelLeft = label;
            if (j > 0) {
                labelLeft = labels[i][j - 1];
            }
            
            labelRight = label;
            if (j < newImage.cols - 1) {
                labelRight = labels[i][j + 1];
            }
            
            if (label != labelTop || label != labelBottom || label!= labelLeft || label != labelRight) {
                newImage.at<cv::Vec3b>(i, j)[0] = bgr[0];
                newImage.at<cv::Vec3b>(i, j)[1] = bgr[1];
                newImage.at<cv::Vec3b>(i, j)[2] = bgr[2];
            }
        }
    }
    
    return newImage;
}

cv::Mat Draw::meanImage(int** labels, const cv::Mat &image) {
    assert(image.channels() == 3);
    
    cv::Mat newImage = image.clone();
    int numberOfSuperpixels = Integrity::countSuperpixels(labels, newImage.rows, newImage.cols);
    
    int meanB = 0;
    int meanG = 0;
    int meanR = 0;
    int count = 0;
    
    for (int label = 0; label < numberOfSuperpixels; label++) {
        meanB = 0;
        meanG = 0;
        meanR = 0;
        count = 0;
        
        for (int i = 0; i < newImage.rows; i++) {
            for (int j = 0; j < newImage.cols; j++) {
                if (labels[i][j] == label) {
                    meanB += image.at<cv::Vec3b>(i, j)[0];
                    meanG += image.at<cv::Vec3b>(i, j)[1];
                    meanR += image.at<cv::Vec3b>(i, j)[2];
                    
                    count++;
                }
            }
        }
        
        if (count > 0) {
            meanB = (int) meanB/count;
            meanG = (int) meanG/count;
            meanR = (int) meanR/count;
        }
        
        for (int i = 0; i < newImage.rows; i++) {
            for (int j = 0; j < newImage.cols; j++) {
                if (labels[i][j] == label) {
                    newImage.at<cv::Vec3b>(i, j)[0] = meanB;
                    newImage.at<cv::Vec3b>(i, j)[1] = meanG;
                    newImage.at<cv::Vec3b>(i, j)[2] = meanR;
                }
            }
        }
    }
    
    return newImage;
}

cv::Mat Draw::labelImage(int** labels, const cv::Mat &image) {
    cv::Mat newImage = image.clone();
    
    int maxLabel = 0;
    for (int i = 0; i < newImage.rows; i++) {
        for (int j = 0; j < newImage.cols; j++) {
            // assert(labels[i][j] >= 0);
            
            if (labels[i][j] > maxLabel) {
                maxLabel = labels[i][j];
            }
        }
    }
    
    // Always add 1 to allow -1 as label.
    int** colors = new int*[maxLabel + 2];
    for (int k = 0; k < maxLabel + 2; ++k) {
        colors[k] = new int[3];
        colors[k][0] = rand() % 256;
        colors[k][1] = rand() % 256;
        colors[k][2] = rand() % 256;
    }
    
    for (int i = 0; i < newImage.rows; ++i) {
        for (int j = 0; j < newImage.cols; ++j) {
            if (labels[i][j] >= 0) {
                int label = labels[i][j];
                newImage.at<cv::Vec3b>(i, j)[0] = colors[label + 1][0];
                newImage.at<cv::Vec3b>(i, j)[1] = colors[label + 1][1];
                newImage.at<cv::Vec3b>(i, j)[2] = colors[label + 1][2];
            }
            else {
                newImage.at<cv::Vec3b>(i, j)[0] = 0;
                newImage.at<cv::Vec3b>(i, j)[1] = 0;
                newImage.at<cv::Vec3b>(i, j)[2] = 0;
            }
        }
    }
    
    for (int k = 0; k < maxLabel + 1; ++k) {
        delete[] colors[k];
    }
    delete[] colors;
    
    return newImage;
}

int Integrity::countSuperpixels(int** labels, int rows, int cols) {
    assert(rows > 0);
    assert(cols > 0);
    
    int maxLabel = 0;
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            assert(labels[i][j] >= 0);
            
            if (labels[i][j] > maxLabel) {
                maxLabel = labels[i][j];
            }
        }
    }
    
    bool* foundLabels = new bool[maxLabel + 1];
    for (int k = 0; k < maxLabel + 1; k++) {
        foundLabels[k] = false;
    }
    
    int count = 0;
    int label = 0;
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            label = labels[i][j];
            
            if (foundLabels[label] == false) {
                foundLabels[label] = true;
                count++;
            }
        }
    }
    
    // Remember to free.
    delete[] foundLabels;
    
    return count;
}

void Integrity::relabel(int** labels, int rows, int cols) {
    assert(rows > 0);
    assert(cols > 0);
    
    int maxLabel = 0;
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            if (labels[i][j] > maxLabel) {
                maxLabel = labels[i][j];
            }
        }
    }
    
    int* relabeling = new int[maxLabel + 1];
    for (int l = 0; l < maxLabel + 1; ++l) {
        relabeling[l] = -1;
    }
    
    int label = 0;
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            if (relabeling[labels[i][j]] < 0) {
                relabeling[labels[i][j]] = label;
                ++label;
            }
            
            labels[i][j] = relabeling[labels[i][j]];
        }
    }
    
    delete[] relabeling;
}

#define PIX_SWAP(a,b) { int temp=(a);(a)=(b);(b)=temp; }
#define PIX_SORT(a,b) { if ((a)>(b)) PIX_SWAP((a),(b)); }

int opt_med9(std::vector<int>& p)
{
    PIX_SORT(p[1], p[2]) ; PIX_SORT(p[4], p[5]) ; PIX_SORT(p[7], p[8]) ;
    PIX_SORT(p[0], p[1]) ; PIX_SORT(p[3], p[4]) ; PIX_SORT(p[6], p[7]) ;
    PIX_SORT(p[1], p[2]) ; PIX_SORT(p[4], p[5]) ; PIX_SORT(p[7], p[8]) ;
    PIX_SORT(p[0], p[3]) ; PIX_SORT(p[5], p[8]) ; PIX_SORT(p[4], p[7]) ;
    PIX_SORT(p[3], p[6]) ; PIX_SORT(p[1], p[4]) ; PIX_SORT(p[2], p[5]) ;
    PIX_SORT(p[4], p[7]) ; PIX_SORT(p[4], p[2]) ; PIX_SORT(p[6], p[4]) ;
    PIX_SORT(p[4], p[2]) ; return(p[4]) ;
}


struct Edge
{
    double var[3];  // average difference of members
    int ptVar;
};

void Integrity::getSuperPixels(std::map<int, SuperPixel>& res, int** labels, int rows, int cols, cv::Mat& src, int mergeiter)
{
	// predict superpixel count
    int maxLabel = 0;
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            if (labels[i][j] > maxLabel)
                maxLabel = labels[i][j];
        }
    }
	// initialize structures
    res.clear();
	//res.resize(maxLabel+1);
	for (int i = 0; i <= maxLabel; ++i) 
	{
		res[i].id=i;
		res[i].map=-1;
		res[i].mean[0]=0;
		res[i].mean[1]=0;
		res[i].mean[2]=0;
		res[i].points=0;
		res[i].around.clear();
		res[i].state=0;
		res[i].center[0] = res[i].center[1] = 0;
        res[i].var[0] = res[i].var[1] = res[i].var[2] = 0;
        res[i].ptVar = 0;
	}

    // info on superpixel edges
    std::map<int, std::map<int, Edge> > edges;

	// gather superpixels info
    for (int i = 0; i < rows; ++i) 
	{
        for (int j = 0; j < cols; ++j) 
		{
			int label = labels[i][j];
			res[label].points++;
            cv::Vec3b pv = src.at<cv::Vec3b>(i, j);
			for( int c = 0; c < 3; c++ )
                res[label].mean[c] += pv[c];
            int nClean = 0;
            if (mergeiter>0) // gather addition information for merge
            {
                if (i > 0)
                {
                    cv::Vec3b pvn = src.at<cv::Vec3b>(i - 1, j);
                    const int labeln = labels[i - 1][j];
                    if (labeln != label)
                    {
                        res[label].around.push_back(labeln);
                        nClean++;

                        int l1 = std::min(label, labeln);
                        int l2 = std::max(label, labeln);
                        for (int c = 0; c < 3; c++)
                            edges[l1][l2].var[c] += abs(pv[c] - pvn[c]);
                        edges[l1][l2].ptVar++;
                    }
                    else
                    {
                        for (int c = 0; c < 3; c++)
                            res[label].var[c] += abs(pv[c] - pvn[c]);
                        res[label].ptVar++;
                    }
                }
                if (j > 0)
                {
                    cv::Vec3b pvn = src.at<cv::Vec3b>(i, j - 1);
                    const int labeln = labels[i][j - 1];
                    if (labeln != label)
                    {
                        res[label].around.push_back(labeln);
                        nClean++;

                        int l1 = std::min(label, labeln);
                        int l2 = std::max(label, labeln);
                        for (int c = 0; c < 3; c++)
                            edges[l1][l2].var[c] += abs(pv[c] - pvn[c]);
                        edges[l1][l2].ptVar++;
                    }
                    else
                    {
                        for (int c = 0; c < 3; c++)
                            res[label].var[c] += abs(pv[c] - pvn[c]);
                        res[label].ptVar++;
                    }
                }
            }
            else
            {
                if (i > 0)
                {
                    const int labeln = labels[i - 1][j];
                    if (labeln != label)
                    {
                        res[label].around.push_back(labeln);
                        nClean++;
                    }
                }
                if (j > 0)
                {
                    const int labeln = labels[i][j - 1];
                    if (labeln != label)
                    {
                        res[label].around.push_back(labeln);
                        nClean++;
                    }
                }
            }
			if (i<rows-1)
				if (labels[i+1][j]!=label)
				{
					res[label].around.push_back(labels[i+1][j]);
                    nClean++;
				}
			if (j<cols-1)
				if (labels[i][j+1]!=label)
				{
					res[label].around.push_back(labels[i][j+1]);
                    nClean++;
				}
			{
				res[label].center[0] += j;
				res[label].center[1] += i;
			}
            if (nClean>0)
			{
				int nAround = res[label].around.size();
                for (int k = nAround - 1; k > nAround - 1 - nClean; k--)
				{
					for(int l = 0; l < k; l++)
					{
						if (res[label].around[k]==res[label].around[l])
						{
							//for(int x=0;x<nAround;x++)
								//assert(res[label].around[x]>=0 && res[label].around[x]<=maxLabel);
							res[label].around.erase(res[label].around.begin()+k);
							nAround--; k--; l=0;
							//for(int x=0;x<nAround;x++)
								//assert(res[label].around[x]>=0 && res[label].around[x]<=maxLabel);
						}
					}
				}
			}
		}
	}
	// update mean values
	for (int i = 0; i <= maxLabel; ++i) 
	{
		if (res[i].points>0 && res[i].map<0)
		{
			res[i].mean[0]/=res[i].points;
			res[i].mean[1]/=res[i].points;
			res[i].mean[2]/=res[i].points;
			res[i].center[0]/=res[i].points;
			res[i].center[1]/=res[i].points;
            if (res[i].ptVar > 1)
            {
                res[i].var[0] /= res[i].ptVar;
                res[i].var[1] /= res[i].ptVar;
                res[i].var[2] /= res[i].ptVar;
                res[i].ptVar = 1;
            }
		}
		else
			res.erase(i);
	}	

    // update edges values
    for (std::map<int, std::map<int, Edge> >::iterator eit = edges.begin(); eit != edges.end(); eit++)
    {
        std::map<int, Edge>& elist = eit->second;
        for (std::map<int, Edge>::iterator it = elist.begin(); it != elist.end(); it++)
        {
            if (it->second.ptVar > 1)
            {
                for (int c = 0; c < 3; c++)
                    it->second.var[c] /= it->second.ptVar;
                it->second.ptVar = 1;
            }
        }
    }

	// prepare merge of similar superpixels
    if (mergeiter>0)
	{
		std::map<int,int> mergeMap;
		for (int i = 0; i <= maxLabel; ++i) 
		{
			mergeMap[i]=i;
			if (res[i].points>0)
			{
				int nAround = res[i].around.size();
				for (int j = 0; j < nAround; ++j) 
				{
					int id = res[i].around[j];
					if (res[id].map>=0)
						id = res[id].map;
	#define TOLERANCE	20
					if (fabs(res[i].mean[0]-res[id].mean[0])<TOLERANCE && 
						fabs(res[i].mean[1]-res[id].mean[1])<TOLERANCE && 
						fabs(res[i].mean[2]-res[id].mean[2])<TOLERANCE)
					{
						res[i].map = id;
						mergeMap.at(i) = id;
						break;
					}
                    else
                    {
                        // compute variance on edge and compare it with superpixels, if smaller than any of two, merge them
                        int l1 = std::min(i, id);
                        int l2 = std::max(i, id);
                        const Edge& e = edges[l1][l2];                        
                        if (1==e.ptVar) // is valid
                        {
                            if ((e.var[0] < res[i].var[0] &&
                                e.var[1] < res[i].var[1] &&
                                e.var[2] < res[i].var[2]) ||
                                (e.var[0] < res[id].var[0] &&
                                 e.var[1] < res[id].var[1] &&
                                 e.var[2] < res[id].var[2]))
                            {
                                res[i].map = id;
                                mergeMap.at(i) = id;
                                break;
                            }
                        }
                    }
				}
			}
		}
		//
		for (int i = 0; i < rows; ++i) 
		{
			for (int j = 0; j < cols; ++j) 
			{
				int label = labels[i][j];
				labels[i][j] = mergeMap[label];
			}
		}
        getSuperPixels(res, labels, rows, cols, src, mergeiter-1);
	}
}

#endif