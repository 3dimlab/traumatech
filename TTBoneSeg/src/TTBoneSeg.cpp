/**********************************************************************
* This file is part of
* 
* TraumaTech - Computer-based Planning in Trauma Treatment
* Copyright 2015 3Dim Laboratory s.r.o.
* All rights reserved.
* 
* This software was created with the support of TA CR (https://www.tacr.cz/)
* under project number TA04011606.
* 
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
* 
*   http://www.apache.org/licenses/LICENSE-2.0
* 
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
**********************************************************************/

#include "TTBoneSeg.h"

#include <VPL/ImageIO/DicomSlice.h>
#include <VPL/Base/Logging.h>
#include <VPL/Image/ImageFunctions.h>
//#include <VPL/Image/ImageFiltering.h>
//#include <VPL/Image/Filters/Median.h>
#include <VPL/Image/Filters/Gaussian.h>
#include <VPL/Image/Filters/Sobel.h>

// STL
#include <iostream>
#include <string>

#include "opencv/structured_edge_detection.hpp"
#include "opencv/seeds.hpp"
#include <opencv2\highgui.hpp>

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"


#include "segmentation/SeedsRevised.h"
#include "segmentation/Tools.h"
#include "segmentation/slic.h"
#include "imagefiltering.h"


//==============================================================================
/*
 * Global module constants.
 */

//! Module description
const std::string MODULE_DESCRIPTION    = "Module detect edges on image in DICOM format";

//! Additional command line arguments
const std::string MODULE_ARGUMENTS      = "algorithm:filter";

const std::string MODULE_ARGUMENT_ALGORITHM = "algorithm";
const std::string MODULE_ARGUMENT_FILTER = "filter";


//==============================================================================
/*
 * Implementation of the class CSliceBoneEdges.
 */
CSliceBoneEdges::CSliceBoneEdges(const std::string& sDescription)
	: vpl::mod::CModule(sDescription)
{
	m_nAlgorithm = 0;
	m_nFilter = 0;
	allowArguments(MODULE_ARGUMENTS);
}


CSliceBoneEdges::~CSliceBoneEdges()
{
}


bool CSliceBoneEdges::startup()
{
	// Note
	VPL_LOG_INFO("Module startup");

	std::string sAlgorithm;
	m_Arguments.value(MODULE_ARGUMENT_ALGORITHM, sAlgorithm);
	m_nAlgorithm = atoi(sAlgorithm.c_str());

	std::string sFilter;
	m_Arguments.value(MODULE_ARGUMENT_FILTER, sFilter);
	m_nFilter = atoi(sFilter.c_str());

	// Test of existence of input and output channel
	if( getNumOfInputs() != 1 || getNumOfOutputs() != 1 )
	{
		VPL_LOG_ERROR('<' << m_sFilename << "> Wrong number of input and output channels" << std::endl);
		return false;
	}

	// O.K.
	return true;
}


bool CSliceBoneEdges::main()
{
	// Note
	VPL_LOG_INFO("Module main function");

	// I/O channels
	vpl::mod::CChannel *pIChannel = getInput(0);
	vpl::mod::CChannel *pOChannel = getOutput(0);

	// Is any input?
	if( !pIChannel->isConnected() )
	{
		return false;
	}

	// Smart pointer to DICOM slice
	vpl::img::CDicomSlicePtr spSlice(new vpl::img::CDicomSlice);
	//vpl::img::CSlicePtr spSlice(new vpl::img::CSlice());

	// Wait for data
	if( pIChannel->wait(1000) )
	{
		// Read and process data from the input channel
		// Load DICOM data from input channel
		//if (readInput(pIChannel,spSlice.get()))
		if( spSlice->loadDicom(pIChannel) )
		{
			{
				if (2 == m_nFilter)
				{
					vpl::img::CDicomSlice *pDiffData = new vpl::img::CDicomSlice(spSlice->getXSize(), spSlice->getYSize(), 15);
					*pDiffData = *spSlice.get();
					pDiffData->mirrorMargin();
					vpl::img::CSobel<vpl::img::CDImage> sobel;
					vpl::img::CDicomSlice tmp(*pDiffData);
					sobel(*pDiffData, tmp);
					//	tmp = tmp.abs();
					vpl::img::CGaussFilter<vpl::img::CDicomSlice> Filter((vpl::tSize)15);
					Filter(tmp, *pDiffData);
					if (1)
					{
						vpl::img::CBoxFilter<vpl::img::CDicomSlice> box;
                        vpl::img::CGaussFilter<vpl::img::CDicomSlice> FilterS((vpl::tSize)9);
						FilterS(tmp, tmp);
						*pDiffData /= vpl::CScalari(8);
						tmp /= vpl::CScalari(16);
						//box(tmp, tmp);
						//box(tmp, tmp);

						*pDiffData *= tmp;
					}
					*spSlice.get() = *pDiffData;
				}

                if (3 == m_nFilter)
                {
#define UNSHARP_SIZE 45
                    vpl::img::CDicomSlice tmp(spSlice->getXSize(), spSlice->getYSize(), UNSHARP_SIZE);
                    tmp = *spSlice.get();
                    tmp.mirrorMargin();
                    vpl::img::CGaussFilter<vpl::img::CDicomSlice> Filter((vpl::tSize)UNSHARP_SIZE);
                    vpl::img::CDicomSlice tmp2(spSlice->getXSize(), spSlice->getYSize(), UNSHARP_SIZE);
                    Filter(tmp, tmp2);
                    // perform unsharp mask
                    tmp -= tmp2;
                    tmp *= vpl::CScalari(5);
                    *spSlice.get() += tmp;
#undef UNSHARP_SIZE
                }

				int width = spSlice->width();
				int height = spSlice->height();                
				cv::Mat image(spSlice->height(), spSlice->width(), CV_8UC3);
				for (int i = 0; i < image.rows; ++i)
				{
					for (int j = 0; j < image.cols; ++j)
					{
						int val = spSlice->at(j, i) / 16;
						//val = std::max(std::min(val, 255), 0);
                        val = std::max(val, 0);
                        val = std::min(val, 255);

						image.at<cv::Vec3b>(i, j)[0] = image.at<cv::Vec3b>(i, j)[1] = image.at<cv::Vec3b>(i, j)[2] = val;
					}
				}

				// downscale image
				cv::resize(image,image,cv::Size(width/2,height/2),0,0,cv::INTER_CUBIC);
				width = width/2;
				height = height/2;

				if (1 == m_nFilter) // enhance edges by adding a difference between erosion and dilation
				{
					if (1)
					{
						cv::Mat erosion(height, width, CV_8UC1);
						cv::Mat dilation(height, width, CV_8UC1);
						cv::erode(image, erosion, cv::Mat(), cv::Point(-1, -1), 3);
						cv::dilate(image, dilation, cv::Mat(), cv::Point(-1, -1), 3);
						image = image + 2 * (dilation - erosion);
					}

					{
						cv::Mat imageF(height, width, CV_8UC1);
						cv::bilateralFilter(image, imageF, 0, 30, 10);
						image = imageF;
					}
				}


				if(0==m_nAlgorithm) // SEEDS superpixels
				{
					cv::Ptr<cv::ximgproc::SuperpixelSEEDS> seeds;
					int num_superpixels = 800;
					int num_levels = 4;
					int num_histogram_bins = 8;
					int prior = 2;
					int num_iterations = 4;
					bool double_step = false;
					seeds = cv::ximgproc::createSuperpixelSEEDS(width, height, image.channels(), num_superpixels, num_levels, prior, num_histogram_bins, double_step);
					cv::Mat converted;
					cvtColor(image, converted, cv::COLOR_BGR2HSV);

					//double t = (double) getTickCount();

					seeds->iterate(converted, num_iterations);
					/* retrieve the segmentation result */
					cv::Mat labels;
					seeds->getLabels(labels);
					/* get the contours for displaying */
					cv::Mat mask;
					seeds->getLabelContourMask(mask, false);
					image.setTo(cv::Scalar(0, 0, 255), mask);
				}
				//

				if (1==m_nAlgorithm) // seeds revised
				{
					double alpha = 1.5;
					double beta = 0;
					cv::Mat kernel = cv::getGaussianKernel(257,64);
					const double gmax = kernel.at<double>(128);
					for(int i  = 0;i < 257;i++)
					{
						kernel.at<double>(i) = kernel.at<double>(i)/gmax;
						//qDebug() << kernel.at<double>(i);
					}
		#pragma omp parallel for
					for( int y = 0; y < image.rows; y++ )
					{ 
						for( int x = 0; x < image.cols; x++ )
						{ 
							for( int c = 0; c < 3; c++ )
							{
								int val = image.at<cv::Vec3b>(y,x)[c];
								image.at<cv::Vec3b>(y,x)[c] =
									cv::saturate_cast<uchar>( alpha*kernel.at<double>(val)*val + beta );
							}
						}
					}
					cv::medianBlur(image,image,11);

					int iterations = 16;

					// Number of desired superpixels.
					int superpixels = 1200;
	
					// Number of bins for color histograms (per channel).
					int numberOfBins = 8;
	
					// Size of neighborhood used for smoothing term, see [1] or [2].
					// 1 will be sufficient, >1 will slow down the algorithm.
					int neighborhoodSize = 1;
	
					// Minimum confidence, that is minimum difference of histogram intersection
					// needed for block updates: 0.1 is the default value.
					float minimumConfidence = 0.01;
	
					// The weighting of spatial smoothing for mean pixel updates - the euclidean
					// distance between pixel coordinates and mean superpixel coordinates is used
					// and weighted according to:
					//  (1 - spatialWeight)*colorDifference + spatialWeight*spatialDifference
					// The higher spatialWeight, the more compact superpixels are generated.
					float spatialWeight = 0.45;

		#define FR 1
					cv::Mat imageSmall(height / FR, width / FR, CV_8UC3);
					cv::resize(image, imageSmall, cv::Size(width / FR, height / FR), 0, 0, cv::INTER_LINEAR);
	
					// Instantiate a new object for the given image.
					SEEDSRevisedMeanPixels seeds(imageSmall, superpixels, numberOfBins, neighborhoodSize, minimumConfidence, spatialWeight);
					//SEEDSRevised seeds(image, superpixels, numberOfBins, neighborhoodSize, minimumConfidence);

					// Initializes histograms and labels.
					seeds.initialize();
					// Runs a given number of block updates and pixel updates.
					seeds.iterate(iterations);

					int** labels = seeds.getLabels();
					int** labelsF = new int*[height];
					for (int y = 0; y < height; ++y)
						labelsF[y] = new int[width];
		#pragma omp parallel for
					for (int y = 0; y < height; ++y)
					{
						int sy = std::min(y / FR, height/FR - 1 );
						for (int x = 0; x < width; ++x)
						{
							int sx = std::min(x / FR, width / FR - 1);
							labelsF[y][x] = labels[sy][sx];
						}
					}		
	
					// bgr color for contours:
					int bgr[] = {0, 0, 204};
	
					// seeds.getLabels() returns a two-dimensional array containing the computed
					// superpixel labels.
					cv::Mat contourImage = Draw::contourImage(labelsF , image, bgr);
					//cv::Mat contourImage = Draw::labelImage(seeds.getLabels(), image);            

					for (int y = 0; y < height; ++y)
						delete labelsF[y];
					delete labelsF;

					image = contourImage;
				}

				if (2==m_nAlgorithm)
				{
					unsigned char* pImageData = new unsigned char[width*height*4];
					for(int y=0;y<height;y++)
					{
						unsigned char* pLine = pImageData+y*width*4;
						for(int x=0;x<width;x++)
						{
							int val = image.at<cv::Vec3b>(y,x)[0];
							//val = std::max(std::min(val,255),0);

							pLine[0]=pLine[1]=pLine[2]=pLine[3]=val;
							pLine+=4;
						}
					}

					int* labels = new int[width*height];
					int m_spcount = 800;
					int numlabels(0);
					SLIC slic;
					slic.PerformSLICO_ForGivenK((const unsigned int*)pImageData, width, height, labels, numlabels, m_spcount, 0);//for a given number K of superpixels
					//slic.PerformSLICO_ForGivenStepSize(img, width, height, labels, numlabels, m_stepsize, m_compactness);//for a given grid step size
					slic.DrawContoursAroundSegments((unsigned int*)pImageData, labels, width, height, 0x0FF/*0xFFFFFFFF*/);//for black contours around superpixels
					//slic.DrawContoursAroundSegmentsTwoColors((unsigned int*)pImageData, labels, width, height);//for black-and-white contours around superpixels
					for(int y=0;y<height;y++)
					{
						unsigned char* pLine = pImageData+y*width*4;
						for(int x=0;x<width;x++)
						{
							image.at<cv::Vec3b>(y,x)[0] = pLine[0];
							image.at<cv::Vec3b>(y,x)[1] = pLine[1];
							image.at<cv::Vec3b>(y,x)[2] = pLine[2];
							//pLine[0]=pLine[1]=pLine[2]=pLine[3]=spRTGSlice->at(x,y);
							pLine+=4;
						}
					}
					delete labels;
					delete pImageData;
				}

				//
				if(3==m_nAlgorithm) // structured forests
				{
					cv::Mat imx(image);

					cv::String modelName = "model.yml.gz";
					std::ifstream infile(modelName); // test file existence
					if (infile.good())
					{
						// structured forests
						infile.close();
						cv::Ptr<cv::ximgproc::RFFeatureGetter> pGetter = cv::ximgproc::createRFFeatureGetter();
						cv::Ptr<cv::ximgproc::StructuredEdgeDetection> pDollar =
							cv::ximgproc::createStructuredEdgeDetection(modelName);

						double alpha = 3;
						double beta = 0;
						cv::Mat kernel = cv::getGaussianKernel(257,64);
						const double gmax = kernel.at<double>(128);
						for(int i  = 0;i < 257;i++)
						{
							kernel.at<double>(i) = kernel.at<double>(i)/gmax;
						}
						for( int y = 0; y < image.rows; y++ )
						{ 
							for( int x = 0; x < image.cols; x++ )
							{ 
								for( int c = 0; c < 3; c++ )
								{
									int val = image.at<cv::Vec3b>(y,x)[c];
									image.at<cv::Vec3b>(y,x)[c] =
										cv::saturate_cast<uchar>( alpha*kernel.at<double>(val)*val + beta );
								}
							}
						}
						image.convertTo( image, cv::DataType<float>::type, 1/255.0 );
						cv::Mat copy(image);
						pDollar->detectEdges(copy,image);
					}
				}



				//
				cv::namedWindow("output", cv::WINDOW_NORMAL | cv::WINDOW_KEEPRATIO);               
				cv::resizeWindow("output", width , height );
				//cv::resizeWindow("output", spSlice->width() / 2, spSlice->height() / 2);
				//cv::resize(image,image,cv::Size(width/2,height/2),0,0,cv::INTER_CUBIC);
				cv::imshow("output", image);
				cv::waitKey();
			}
			// Write it to the output channel as slice
			/*if( !writeOutput(pOChannel, spSlice.get()) )
			{
				VPL_LOG_ERROR('<' << m_sFilename << "> Failed to write the output slice" << std::endl);
			}*/
		}
		else
		{
			VPL_LOG_ERROR('<' << m_sFilename << "> Failed to read input DICOM data" << std::endl);
			return false;
		}
	}
	else
	{
		VPL_LOG_INFO("Wait timeout");
	}

	// Returning 'true' means to continue processing the input channel
	return true;
}


void CSliceBoneEdges::shutdown()
{
	// Note
	VPL_LOG_INFO("Module shutdown");
}


void CSliceBoneEdges::writeExtendedUsage(std::ostream& Stream)
{
	Stream << "Extended usage: [-algorithm 0-3] [-filter 0-3]" << std::endl;
	Stream << "Options:" << std::endl;
	Stream << "  -algorithm Bone edge detection technique." << std::endl;
	Stream << "  -filter    Image preprocessing." << std::endl;
	Stream << std::endl;

	Stream << "Input: DICOM image" << std::endl;
	//Stream << "Output: MDSTk image/slice" << std::endl;
}


//==============================================================================
/*
 * Function main() which creates and executes console application.
 */
int main(int argc, char *argv[])
{
	// Creation of a module using smart pointer
	CSliceBoneEdgesPtr spModule(new CSliceBoneEdges(MODULE_DESCRIPTION));

	// Initialize and execute the module
	if( spModule->init(argc, argv) )
	{
		spModule->run();
	}

	// Console application finished
	return 0;
}

