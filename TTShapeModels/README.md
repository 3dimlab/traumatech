    **********************************************************************
    * This file is part of
    * 
    * TraumaTech - Computer-based Planning in Trauma Treatment
    * Copyright 2015 3Dim Laboratory s.r.o.
    * All rights reserved.
    * 
    * This software was created with the support of TA CR (https://www.tacr.cz/)
    * under project number TA04011606.
    * 
    * Licensed under the Apache License, Version 2.0 (the "License");
    * you may not use this file except in compliance with the License.
    * You may obtain a copy of the License at
    * 
    *   http://www.apache.org/licenses/LICENSE-2.0
    * 
    * Unless required by applicable law or agreed to in writing, software
    * distributed under the License is distributed on an "AS IS" BASIS,
    * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    * See the License for the specific language governing permissions and
    * limitations under the License.
    *
    **********************************************************************

Table of Contents

1. What is TTShapeModels?
    1. Basic Features
      1.1.1 Basic model alignment
      1.1.2 Correspondences finding
      1.1.3 Optimal alignment of the corresponding models 
      1.1.4 Building of the statistical model 
2. Installation
3. Bug Reporting
4. Contacts

1. What is TTShapeModels?
========================
    
This 3D registration toolkit consists of source code, this documentation file and installation instructions for software library and several applications. After the build the whole processing pipeline can be built using included scripts. In the input should be stl files (3D meshes) and in the output is one Statismo file. This file can be viewed by StatismoViewer:

https://github.com/statismo/statismo/wiki/Statismo%20Viewer

Whole package is separated into two libraries stored in include and src directories: 
- Registration3D contains all implemented algorithms - three versions of ICP, rigid and non-rigid registration and generalised Procrustes analysis. It also includes utility methods (NannoFlann KD-tree encapsulation for example).
- StatismoTools includes helper objects and methods encapsulating Statismo API 

1.1 Basic Features
-------------------
Whole processing pipeline is splitted into the four steps:

1.1.1 Basic model alignment 

This is the first step of the 3D registration. Loaded model is rigidly (without any changes on mesh shape) aligned to the target model. Plane-to-plane version ICP algorithm is used. More information can be found in [1]. Output transformation may or may not contain scale factor. In our case scaling is used because we need as simillar as possible models for the next step.  

Application: Registration3D
Script: test/registration.bat 

1.1.2 Correspondences finding 

The second step of 3D registration has the purpose to modify the shape of the input mesh to the shape of the static model without changig triangle mesh topology. It is also necessary that the resulting model must stay manifold. This can be seen as non-rigid registration phase - points of the input model are moved to the corresponding points on the static model to achieve the best best possible accordance. In our case we are transforming one of the meshes to all the others - we want to have set of models with the same triangle topolgy and with different shapes. 

Application: nonRigid
Script: test/nonrigid.bat

1.1.3 Optimal alignment of the corresponding models 

Iterative Generalised Procrustes analysys [2] is used. This is last step of the 3D registration - now we have set of the models with the same topology aligned together.

Application: GPAApp
Script: test/gpa.bat

1.1.4 Building of the statistical model 

Statismo library is used in this step. Already registered meshes with the same number of corresponding vertices are used to build statistical model. This is stored in the standard HDF5 file. 

Application: StatisticModelCreatorApp
Script: statismo.bat

All aplications source files are stored in the App directory.

References:
[1] Aleksandr V. Segal, Dirk Haehnel, and Sebastian Thrun. "Generalized-ICP". Robotics: Science and Systems 2009.
[2] Gower, J. C. (1975). "Generalized procrustes analysis". Psychometrika 40: 33�51.


2. Installation
===============

To build the toolkit you will need following libraries:
VPL -  https://bitbucket.org/3dimlab/vpl
OpenMesh -  http://www.openmesh.org/
NanoFlann -  https://github.com/jlblancoc/nanoflann
Boost -  http://www.boost.org/
Statismo -  https://github.com/statismo/statismo
HDF5 - https://www.hdfgroup.org/HDF5/

The whole project uses CMake building scripts (www.cmake.org). All you will need for toolkit building is to run cmake and set all libraries paths. Than build it with selected compiler. 

If you want to test whole system and use included data for it, just copy all built applications executables to the test directory and run included script all.bat. In the output directory you will find (after some calculation time) ssm.h5 file which can be viewed by statismo viewer app (see above). 


3 Bug Reporting
===============

We don't promise that this software is completely bug free. If you encounter
any issue, please let us know.

* Report the bug using 
  [Bitbucket's issue tracker](https://bitbucket.org/3dimlab/traumatech/issues),
* mail your bug reports to info(at)3dim-laboratory.cz,

or

* fork the repository, fix it, and send us a pull request.


---

4 Contacts
==========

* 3Dim Laboratory s.r.o.
* E-mail: info@3dim-laboratory.cz
* Web: http://www.3dim-laboratory.cz/
 


