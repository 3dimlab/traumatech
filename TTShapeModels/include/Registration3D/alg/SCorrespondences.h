///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#ifndef Correspondences_H_included
#define Correspondences_H_included

#include <base/CModel.h>

/**
 * \brief   Correspondences.
 */
struct SCorrespondences
{
    //! Source mesh, target mesh
    CMeshPtr mesh_source, mesh_target;

    //! Mesh indexing source, mesh indexing target
    CMeshIndexingPtr mesh_indexing_source, mesh_indexing_target;

    // Type of correspondence pair - index in source indexing, index in target indexing
    typedef std::pair<size_t, size_t> tCorrespondence;

    // Correspondences vector type
    typedef std::vector<tCorrespondence> tCorrespondences;

    //! Correspondences 
    tCorrespondences correspondences;

}; // SCorrespondences

// Correspondences_H_included
#endif

