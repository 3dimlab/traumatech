///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#ifndef ICP_H_included
#define ICP_H_included

#include <base/CModel.h>
#include <VPL/Math/TransformMatrix.h>
#include <VPL/Math/StaticVector.h>
#include <kdtree/kdtree.h>

/**
    Base of all ICP algorithms
*/
class CICPBase
{
protected:
    typedef vpl::math::CDVector3 tVector;
    typedef vpl::math::CTransformMatrix<double> tTransform;
    typedef vpl::math::CQuat<double> tQuat;
    typedef vpl::math::CDMatrix3x3 tMatrix3;

    //! Correspondence type
    struct SCorrespondence
    {
        tVector first, second;
        size_t idFirst, idSecond;
        double distance;
        double weight;
    };

    //! Vector of correspondences
    typedef std::vector<SCorrespondence> tCorrespondenceVec;

public:
    //! Use point mask type
    typedef std::vector<bool> tPointsUseMask;

public:
    //! Simple constructor
    CICPBase(double max_inlier_distance = 1.0);

    //! Apply algorithm on two models 
    virtual bool apply(CMesh *target, CMesh *source, int num_iterations, bool estimate_inlier_distance_automaticaly = true, bool estimate_step_limits = true);

    //! Get estimated transform matrix
    const tTransform getTransform() { return m_tm; }

    //! Try to estimate max inlier distance from model size (= maximal bounding box size / 10)
    double estimateMaxInlierDistance(CMesh *model, bool set_it = true);

    //! Set maximal inlier distance
    void setMaximalInlierDistance(double distance) { m_maxInlierDistance = distance; }

    //! Set maximal distance step for one iteration
    void setMaximalDistanceStep(double step) {m_maxDistanceStep = step;}

    //! Set maximal rotation step for one iteration in radians
    void setMaximalRotationStep(double step) {m_maxRotationAngleStep = step;}

    //! Set maximal rotation step for one iteration in degrees
    void setMaximalRotationStepDeg(double step) {m_maxRotationAngleStep = vpl::math::deg2Rad(step);}

    //! Compute scale too (true by default)?
    void setComputeScale(bool compute) {m_bComputeScale = compute;}

    //! Change minimal change threshold (default is 0.05)
    void setMinimalChangeThreshold(double value){m_minimalChangeThreshold = value;}

    //! Set usability masks
    void setMasks(const tPointsUseMask &mask_source, const tPointsUseMask &mask_target) {m_maskSource = mask_source; m_maskTarget = mask_target;}

    //! Reset masks
    void resetMasks() { m_maskSource.clear(); m_maskTarget.clear(); }

protected:
    //! Apply algorithm - internal version
    virtual bool applyInternal(tTransform &transform, int num_steps) = 0;

    //! Compute first transform estimation
    double estimateInitialTransform(CMesh *smodel, CMesh *model);

    //! Get sum of squared differences between matrices
    double sumDifferences(const tTransform &m1, const tTransform &m2);

    //! Get weight between points
    double weight(size_t idSource, size_t idTarget);

    //! Compute by Umeyama method
    tTransform computeUmeyama(const tCorrespondenceVec &correspondences, size_t last_correspondence, bool solve_scaling);


protected:
    //! KD tree for static model
    CKDTreeOM m_kdtreeTarget;

    //! Input model indexing 
    CMeshIndexingPtr m_modelIdxSource;

    //! Estimated transform
    tTransform m_tm;

    //! Maximal inlier distance
    double m_maxInlierDistance;

    //! Minimal matrix summed change
    double m_minimalChangeThreshold;

    //! Distance constraint
    double m_maxDistanceStep;

    //! Rotation constraint (in radians)
    double m_maxRotationAngleStep;

    //! Divider of max distance used to estimate maximal distance step
    size_t m_msDivider;

    //! Should scale be computed?
    bool m_bComputeScale;

    //! Use points mask - true means use point for computation
    tPointsUseMask m_maskSource, m_maskTarget;
};

class CPointPointICP : public CICPBase
{
protected:
    //! Apply algorithm - internal version
    virtual bool applyInternal(tTransform &transform, int num_iterations);

};

class CPointPlaneICP : public CICPBase
{
protected:
    //! Hessian normal form of one plane
    struct SHNF
    {
        tVector n;
        double p;
    };

    //! Vector of HNF
    typedef std::vector<SHNF> tHNFVec;

protected:
    //! Compute planes 
    void computePlanes(const CMeshIndexing &indexing, tHNFVec &planes); 

    //! Apply algorithm - internal version
    virtual bool applyInternal(tTransform &transform, int num_iterations);

protected:
    //! Planes
    tHNFVec m_planes;
};

class CPlanePlaneICP : public CICPBase
{
protected:
    //! Eigen matrix
    typedef Eigen::Matrix3d tEMatrix;

    //! Matrices vector
    typedef std::vector<tEMatrix> tEMatrixVec;

public:
    //! Apply algorithm on two models 
    virtual bool apply(CMesh *target, CMesh *source, int num_iterations, bool estimate_inlier_distance_automaticaly = true);

protected:
    //! Apply algorithm - internal version
    virtual bool applyInternal(tTransform &transform, int num_iterations);

    //! Compute covariance matrices for given points
    void computeCovariantMatrices(const CKDTreeOM &tree, tEMatrixVec &matrices, size_t num_neighbors = 20);

protected:
    //! Covariances
    tEMatrixVec m_covSource, m_covTarget;

    //! Target tree
    CKDTreeOM m_kdtreeSource;
};

#endif