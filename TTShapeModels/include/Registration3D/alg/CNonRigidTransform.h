///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#ifndef CNonRigidTransform_H_included
#define CNonRigidTransform_H_included

#include "CTransformInterpolator.h"
//#include "CGridTransformInterpolator.h"
#include "base/CModel.h"


/** 
 * Class encapsulates "non rigid" transform computation. Algorithm computes several
 * ICP transformations on parts of mesh and than initializes transform interpolator
 * object.
 */

class CNonRigidSimple
{
public:
    //! Constructor
    CNonRigidSimple(size_t num_levels = 3) : m_numLevels(num_levels) {}

    //! Set number of levels
    void setNumLevels(size_t num_levels) {m_numLevels = num_levels;}

    //! Compute transformations
    bool solve(CMesh &target, CMesh &source);
    
    //! Get interpolator object
    const CTransformInterpolator &getInterpolator() const {return m_interpolator;}

    //! Apply interpolation on mesh
    bool applyOnMesh(CMesh &mesh);

protected:
    //! Point use mask
    typedef std::vector<bool> tPointsUseMask;

    //! Transform matrix type
    typedef CTransformInterpolator::tTransform tTransform;

    //! Vector of transform matrices
    typedef CTransformInterpolator::tTransformVec tTransformVec;

protected:
    //! Interpolator object
    CTransformInterpolator m_interpolator;

    //! Number of levels
    size_t m_numLevels;

};
/*
class CNonRigidGrid
{
public:
    //! Constructor
    CNonRigidGrid(size_t num_levels_x = 2, size_t num_levels_y = 2, size_t num_levels_z = 3) 
        : m_numLevelsX(num_levels_x), m_numLevelsY(num_levels_y), m_numLevelsZ(num_levels_z) {}

    //! Set number of levels
    void setNumLevels(size_t num_levels_x, size_t num_levels_y, size_t num_levels_z) 
        {m_numLevelsX = num_levels_x; m_numLevelsY = num_levels_y; m_numLevelsZ = num_levels_z; }

    //! Compute transformations
    bool solve(CMesh &target, CMesh &source);

    //! Get interpolator object
    const CGridTransformInterpolator &getInterpolator() const {return m_interpolator;}

    //! Apply interpolation on mesh
    bool applyOnMesh(CMesh &mesh);

protected:
    //! Point use mask
    typedef std::vector<bool> tPointsUseMask;

    //! Transform matrix type
    typedef CGridTransformInterpolator::tTransform tTransform;

    //! Limits vector type
    typedef CGridTransformInterpolator::tVector tVector;

    //! Vector of transform matrices
    typedef CGridTransformInterpolator::tTransformVec tTransformVec;

    //! Grid of transforms
    typedef CGridTransformInterpolator::tXYZTransformGrid tXYZTransformGrid;

protected:
    //! Interpolator object
    CGridTransformInterpolator m_interpolator;

    //! Number of levels
    size_t m_numLevelsX, m_numLevelsY, m_numLevelsZ;
};
*/
#endif