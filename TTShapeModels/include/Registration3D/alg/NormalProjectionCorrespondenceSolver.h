///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#ifndef NormalProjectionCorrespondenceSolver_H_included
#define NormalProjectionCorrespondenceSolver_H_included

#include <base/CModel.h>
#include <kdtree/kdtree.h>
#include <VPL/Math/TransformMatrix.h>
#include <VPL/Math/StaticVector.h>
#include <array>
#include <functional>
#include <random>

class CNormalProjectionCorrespondenceSolver
{
public:
    typedef vpl::math::CDVector3 tVector;
    typedef vpl::math::CTransformMatrix<double> tTransform;

public:
    //! Constructor
    CNormalProjectionCorrespondenceSolver() {}

    //! Apply algorithm on two models and the result store in third
    CMesh * apply(CMesh *target, CMesh *source, const tTransform &transformScaled, const tTransform &transformSimple);

protected:
    //! Flags vector type
    typedef std::vector<bool> tBoolVec;

    //! Indexes vector type
    typedef std::vector<size_t> tIndexesVec;

    //! Vertex handles vector type
    typedef std::vector<CMesh::VHandle> tVHandleVec;

    //! Triangle points array
    typedef std::array<tVector, 3> tTriangle;

    //! Intersection information structure
    struct SIntersection
    {
        tVector point;
        tVector barycentric;
        CMesh::FHandle face;
    };

    //! Position change description structure
    struct SChange
    {
        SChange() : shift(0.0, 0.0, 0.0), changed(false) {}
        void reset() {changed = false; shift.zeros(); }
        void invalidate() {changed = true;}
        tVector shift;
        bool changed;
        CMesh::VHandle handle;
    };

    //! Vector of changes
    typedef std::vector<SChange> tSChangeVec;

    //! Vector of intersections
    typedef std::vector<SIntersection> tSIntersectionVec;

    //! Vector of points
    typedef std::vector<tVector> tVectorVec;

    // Handle -> id map type
    typedef std::map<CMesh::VHandle, size_t> tHandleIdMap; 

    //! Random double generator
    class CRandDouble
    {
    public:
        CRandDouble(double low, double high)
            :r(std::bind(std::uniform_real_distribution<>(low,high),std::default_random_engine())){}

        double operator()(){ return r(); }

    private:
        std::function<double()> r;
    }; // CRandDouble


protected:
    //! Get nearest intersection of ray and mesh
    bool findNearIntersections( CMesh *mesh, CKDTreeOMPtr kdTreeResult, const tVector &point, const tVector &direction, tSIntersectionVec &intersections);

    //! Compute ray-triangle intersection point
    bool rayTriangleIntersection(const tVector &rayOrigin, const tVector &rayDirection, const tTriangle &triangle, tVector &result, tVector &resultBarymetric, bool parallel);

    //! Compute ray-triangle intersection point
    bool rayTriangleIntersection(const tVector &rayOrigin, const tVector &rayDirection, const tTriangle &triangle, tVector &result);

    //! Sort found intersections by distance form given point
    void sortByDistance(const tVector &point, tSIntersectionVec &points);

    //! Estimate missing points by normal interpolation
    bool estimateMissingPoints(tSChangeVec &changes, CMesh &mesh, double weight);

    //! Compute barycentric coordinates of the point
    tVector barycentric(const tVector &point, const tTriangle &t);

    //! Compute cartesian coordinates of the barycentric point
    tVector cartesian(const tVector &point, const tTriangle &t);

    //! Try to compute normal of point
    tVector computeNormal(CMesh::VHandle point_handle, CMesh &mesh, size_t ring = 1);

    //! Compute point in the center of current point neighbors
    tVector averagePosition( CMesh::VertexHandle vh, CMesh &mesh );

    //! Smooth operator
    void smooth(const tSChangeVec &changes, CMesh &mesh, double smoothing_weight);

    //! Try to filter wrong mesh 
    void finalize(CMesh &mesh, double max_move);

    //! Randomly move all vertices to prevent collapsing
    void shake(CMesh &mesh, double max_move);

protected:
    //! Handle-id map for result tree
    tHandleIdMap m_resultHI;

    //! Maximal distance 
    double m_maxUsableDistance;
};

// NormalProjectionCorrespondenceSolver_H_included
#endif

