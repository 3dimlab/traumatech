///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#ifndef CProcrustes_H_included
#define CProcrustes_H_included

#include <VPL/Math/TransformMatrix.h>
#include <vector>
#include "base/CModel.h"

/**
 * \brief   Procrustes analysis computation helper class.
 */
class CProcrustes3D
{
public:
    //! Transform matrix type
    typedef vpl::math::CTransformMatrix<double> tTransform;

    //! 3D point type
    typedef vpl::math::CDVector3 tVector;

public:
    //! Constructor
    CProcrustes3D();

    //! Destructor
    ~CProcrustes3D() {reset();}

    //! Remove all sources and target
    void reset(); 

    //! Add source mesh
    size_t addSource(CMesh &mesh);

    //! Apply analysis
    bool apply(size_t num_steps = 5);

    //! Get transformed mesh 
    bool getMesh(CMesh &mesh, size_t id) const;
    
    //! Get last used target mesh
    bool getTargetMesh(CMesh &mesh) const;

protected:
    //! Vector of coordinates - internal representation of point "cloud"
    typedef std::vector<tVector> tVectorVec;

    //! Vector of vectors of points
    typedef std::vector<tVectorVec *> tVectorVecVec;

    //! Matrices vector
    typedef std::vector<tTransform> tTransformVec;

protected:
    //! Compute mean point of points
    tVector mean(const tVectorVec &points);

    //! Estimate transform
    tTransform estimate(const tVectorVec &source, const tVectorVec &target, bool solve_scaling);

    //! Convert mesh to internal representation
    void convert(CMesh &mesh, tVectorVec &points);

    //! Test if input data is usable.
    bool testInputs();

protected:
    //! Source meshes
    tVectorVecVec m_sources;

    //! Currently used target mesh
    tVectorVec m_target;

    //! Transformation matrices
    tTransformVec m_transforms;
};

#endif