///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#ifndef CGridTransformInterpolator_H_included
#define CGridTransformInterpolator_H_included

#include <VPL/Math/TransformMatrix.h>
#include <VPL/Math/StaticVector.h>
#include <vector>
#include <array>

#define TGrid(T) std::vector< std::vector< std::vector< T > > > 

class CGridTransformInterpolator
{
public:
    //! Transform matrix type
    typedef vpl::math::CTransformMatrix<double> tTransform;

    //! Vector of transform matrices
    typedef std::vector<tTransform> tTransformVec;

    // Vector of vector of vector of transform matrices
    typedef TGrid(tTransform) tXYZTransformGrid; 

    //! 3D point type
    typedef vpl::math::CDVector3 tVector;

    //! Quaternion type
    typedef vpl::math::CDQuat tQuat;

    //! Level limit structure
    struct SLevelLimit
    {
        double minX, minY, maxX, maxY;
    };

    //! Limits vector
    typedef std::vector<SLevelLimit> tLimitsVec;

public:
    //! Constructor
    CGridTransformInterpolator() : m_bAllValid(false) {}

    void setTransforms(const tXYZTransformGrid &transforms) { m_transforms = transforms; update(); }

    //! Set limits
    void setLimits(double zmin, double zmax, const tLimitsVec &level_limits); 

    //! Set all parameters
    void setParameters(double zmin, double zmax, const tLimitsVec &level_limits, const tXYZTransformGrid &transforms)
        { m_transforms = transforms; setLimits(zmin, zmax, level_limits); }

    //! Get transform matrix
    tTransform getTransform(const tVector &t) const;

    //! Transform point
    tVector transform(const tVector &p, const tVector &t) const;

    //! Inverse transform of point
    tVector iTransform(const tVector &p, const tVector &t) const;

    //! Another transform method
    template<typename S>
    void transform(S &x, S &y, S &z, const tVector &t) const { tVector v(x, y, z); v = transform(v, t); x = v(0); y = v(1); z = v(2); }

    //! Another inverse transform method
    template<typename S>
    void iTransform(S &x, S &y, S &z, const tVector &t) const { tVector v(x, y, z); v = iTransform(v, t); x = v(0); y = v(1); z = v(2); }

    //! Another transform method uses point coordinates as parameters too
    template<typename S>
    void transform(S &x, S &y, S &z)
        { tVector v(x, y, z); v = transform(v, tVector(x, y, z)); x = v(0); y = v(1); z = v(2); }

    //! Another inverse transform method uses point coordinates as parameters too
    template<typename S>
    void iTransform(S &x, S &y, S &z)
        { tVector v(x, y, z); v = iTransform(v, tVector(x, y, z)); x = v(0); y = v(1); z = v(2); }

    //! Is interpolator valid
    bool isValid() const { return m_bAllValid; }

protected:
    //! Static vector of integers
    typedef std::array<int, 3> tIntVec;

    //! Vector of vectors
    typedef std::vector<tVector> tVectorVec;

    //! Local coordinate struct
    struct SCoord
    {
        tVector local_t;
        tIntVec segment;
    };

    //! Grid of 3D vectors
    typedef TGrid(tVector) tVectorGrid;

    //! Grid of Quaternions
    typedef TGrid(tQuat) tQuatGrid; 

protected:
    //! Get segment and local parameter
    void toSegment(const tVector &t, SCoord &local) const;

    //! Update internals
    void update();

    //! Interpolate translation
    tVector interpolateTranslation(const SCoord &c) const;

    //! Interpolate rotation
    tQuat interpolateRotation(const SCoord &c) const;

    //! Interpolate scale
    tVector interpolateScale(const SCoord &c) const;

    //! Test input values
    bool testInputs();

    template<typename TG>
    void resizeGrid(TG &grid, tIntVec sizes);

    template<typename TG, typename RV>
    RV getGridValue(const TG &grid, const tIntVec &coords) const
    {
        return grid[coords[0]][coords[1]][coords[2]];
    }

protected:
    //! Is all valid?
    bool m_bAllValid;

    //! Input transformations
    tXYZTransformGrid m_transforms;

    //! Grid size
    tIntVec m_numSegments;

    double m_minZ, m_maxZ;

    //! Grid limits
    tLimitsVec m_levelLimits;

    //! Z-axis segment lengths
    double m_zSegmentLength;

    //! Levels segments lengths 
    tVectorVec m_segmentLengths;

    //! Translation knots
    tVectorGrid m_transKnots;

    //! Rotation knots
    tQuatGrid m_rotKnots;

    //! Scale knots
    tVectorGrid m_scaleKnots;

};

/**
 * \brief   Resize grid.
 *
 * \param [in,out]  grid    The grid.
 * \param   sizes           The sizes.
 */
template<typename TG>
void CGridTransformInterpolator::resizeGrid(TG &grid, tIntVec sizes)
{
    grid.resize(sizes[0]);
    for(size_t x = 0; x < sizes[0]; ++x)
    {
        grid[x].resize(sizes[1]);
        for(size_t y = 0; y < sizes[1]; ++y)
        {
            grid[x][y].resize(sizes[2]);
        }
    }
}

// CGridTransformInterpolator_H_included
#endif

