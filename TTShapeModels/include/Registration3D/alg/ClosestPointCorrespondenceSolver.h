///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#ifndef ClosestPointCorrespondenceSolver_H_included
#define ClosestPointCorrespondenceSolver_H_included

#include <base/CModel.h>
#include <VPL/Math/TransformMatrix.h>
#include <VPL/Math/StaticVector.h>
#include <array>

class ClosestPointCorrespondenceSolver
{
public:
    typedef vpl::math::CDVector3 tVector;
    typedef vpl::math::CTransformMatrix<double> tTransform;

public:
    //! Constructor
    ClosestPointCorrespondenceSolver(){}

    //! Apply algorithm on two models and the result store in third
    CMesh * apply(CMesh *target, CMesh *source, const tTransform &transformScaled, const tTransform &transformSimple);

protected:
    //! Triangle points description 
    typedef std::array<tVector, 3> tTrianglePoints;

    struct STriangle
    {
        STriangle() : valid(true) {}
        tTrianglePoints points;
        tVector edge0, edge1;
        double a00, a01, a11, det; 
        bool valid;
    };

    typedef std::vector<STriangle> tSTriangleVec;

protected:
    //! Fill triangle structures
    void fillTriangles(CMesh *mesh);

    //! Find closest point to the mesh
    double findClosestPoint(const tVector &p, tVector &result);

    //! Try to find closest point
    bool findClosestPointToTriangle(const tVector &p, const STriangle &t, tVector &result);

protected:
    //! Triangles
    tSTriangleVec m_triangles;

}; // class ClosestPointCorrespondenceSolver

#endif