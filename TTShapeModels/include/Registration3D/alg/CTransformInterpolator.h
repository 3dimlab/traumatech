///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#ifndef CTransformInterpolator_H_included
#define CTransformInterpolator_H_included

#include <VPL/Math/TransformMatrix.h>
#include <VPL/Math/StaticVector.h>
#include <vector>

class CTransformInterpolator
{
public:
    //! Transform matrix type
    typedef vpl::math::CTransformMatrix<double> tTransform;

    //! Vector of transform matrices
    typedef std::vector<tTransform> tTransformVec;

    //! 3D point type
    typedef vpl::math::CDVector3 tVector;

    //! Quaternion type
    typedef vpl::math::CDQuat tQuat;

public:
    //! Simple constructor
    CTransformInterpolator() : m_bAllValid(false) {}

    //! Initializing constructor
    CTransformInterpolator(const tTransformVec &transforms, double t_min = 0.0, double t_max = 1.0) 
        : m_transforms(transforms), m_tMin(t_min), m_tMax(t_max) {update();}

    //! Set transformations
    void setTransforms(const tTransformVec &transforms) { m_transforms = transforms; update(); }

    //! Set parameter limits
    void setLimits(double t_min, double t_max) { m_tMin = t_min; m_tMax = t_max; update(); }

    //! Set parameters
    void setParameters(double t_min, double t_max, const tTransformVec &transforms) { m_tMin = t_min; m_tMax = t_max; m_transforms = transforms; update(); }

    //! Get transform matrix
    tTransform getTransform(double t) const;

    //! Transform point
    tVector transform(const tVector &p, double t) const;

    //! Inverse transform of point
    tVector iTransform(const tVector &p, double t) const;

    //! Another transform method
    template<typename S>
    void transform(S &x, S &y, S &z, double t) const { tVector v(x, y, z); v = transform(v, t); x = v(0); y = v(1); z = v(2); }

    //! Another inverse transform method
    template<typename S>
    void iTransform(S &x, S &y, S &z, double t) const { tVector v(x, y, z); v = iTransform(v, t); x = v(0); y = v(1); z = v(2); }

    //! Is interpolator valid
    bool isValid() const { return m_bAllValid; }
protected:
    //! Vector of 3D vectors
    typedef std::vector<tVector> tVectorVec;
    
    //! Vector of Quaternions
    typedef std::vector<tQuat> tQuatVec; 

protected:
    //! Convert input parameter to limits
    double toLimits(double t) const;

    //! Get segment and local parameter
    void toSegment(double t, size_t &segment, double &local_t) const;

    //! Update internals
    void update();

    //! Interpolate translation
    tVector interpolateTranslation(size_t segment, double t) const;

    //! Interpolate rotation
    tQuat interpolateRotation(size_t segment, double t) const;

    //! Interpolate scale
    tVector interpolateScale(size_t segment, double t) const;

protected:
    //! Transformation matrices
    tTransformVec m_transforms;

    //! Parameter limits
    double m_tMin, m_tMax;
    
    //! Segment length
    double m_segmentLength;

    //! Is everything valid?
    bool m_bAllValid;

    //! Number of segments available
    int m_numSegments;

    //! Translation knots
    tVectorVec m_transKnots;

    //! Rotation knots
    tQuatVec m_rotKnots;

    //! Scale knots
    tVectorVec m_scaleKnots;

    /* DEBUG ONLY */
    mutable int min_seg, max_seg;
    mutable double min_t, max_t;
    mutable double min_lt, max_lt;

}; // class CTransformInterpolator

#endif

