///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#ifndef smoothing_H_included
#define smoothing_H_included

#include "base/CModel.h"
#include <OpenMesh/Core/System/config.hh>
#include <OpenMesh/Core/Utils/Property.hh>



class CSmooth
{
public:
    enum EType
    {
        SIMPLE_CENTERING,
        LARGE_GAUSSIAN,
        JACOBI_LAPLACE, 
    };

public:
    //! Constructor
    CSmooth(EType type = SIMPLE_CENTERING);

    //! Apply smoothing
    void apply(CMesh &mesh, float weight);

    //! Set type of smoothing algorithm
    void setType(EType type) {m_type = type;}

    void setCenteringNormalForceWeight(double w) { m_centeringNormalForceWeight = w; }

    void setNumIterations(size_t num) {m_loops = num;}

protected:
    void applySimpleCentering(CMesh &mesh, float weight);
    void applyLargeGaussian(CMesh &mesh, float weight);
    void applyJacobiLaplace(CMesh &mesh, float weight);
protected:
    //! Current type of smoothing algorithm
    EType m_type;

    //! Centering - weight of force along normal
    double m_centeringNormalForceWeight;
    
    //! Number of loops - used in jacobian and laplacian smoothing
    size_t m_loops;
};



// smoothing_H_included
#endif