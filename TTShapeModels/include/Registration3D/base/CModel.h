///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#ifndef CModel_H_included
#define CModel_H_included

#define _USE_MATH_DEFINES

#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/TriMeshT.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
#include <VPL/Base/SharedPtr.h>
#include <VPL/Math/Base.h>
#include <VPL/Math/Matrix.h>
#include <VPL/Math/StaticVector.h>
#include <VPL/Math/TransformMatrix.h>

// define our OM mesh
struct OMTraits : public OpenMesh::DefaultTraits
{
	VertexAttributes(OpenMesh::Attributes::Status | OpenMesh::Attributes::Normal);
	FaceAttributes(OpenMesh::Attributes::Status | OpenMesh::Attributes::Normal);
	EdgeAttributes(OpenMesh::Attributes::Status);
};

typedef OpenMesh::TriMesh_ArrayKernelT<OMTraits> OMMesh;

class CMesh : public vpl::base::CObject, public OMMesh
{
public:
	//! Smart pointer type.
	//! - Declares type tSmartPtr.
	VPL_SHAREDPTR(CMesh);

    typedef vpl::math::CTransformMatrix<double> tTransform;
    typedef vpl::math::CDVector3 tVector;

	//! Calculate bounding box
	bool calc_bounding_box(Point &min, Point &max);

    //! Calculate oriented bounding box
    bool calc_oriented_bounding_box(tTransform &tm, tVector &extent);

    //! Calculate oriented bounding box from triangles
    bool calc_oriented_bb_triangles(tTransform &tm, tVector &extent);

    //! calculates average vertex of model
    bool calc_average_vertex(CMesh::Point &average);

    //! Transform mesh vertices
    void translate(float x, float y, float z);

    //! Do complete transform by eigen matrix
    void transform(const tTransform &tm);

    //! Save mesh to file
    void save(const std::string &filename);

protected:
    // Compute oriented bounding box from the covariance matrix
    bool calc_obb_from_cm(vpl::math::CDMatrix &cm, tTransform &tm, tVector &extent);
};

typedef CMesh::tSmartPtr CMeshPtr;

struct CMeshIndexDataSimple
{
    typedef vpl::math::CDVector3 tVec;

    CMesh::VertexHandle vh;
    tVec point, normal;
};

/** 
 *  Mesh indexing vector
 */
class CMeshIndexing : public vpl::base::CObject, public std::vector<CMeshIndexDataSimple>
{
public:
    VPL_SHAREDPTR(CMeshIndexing);

    //! Vector type
    typedef CMeshIndexDataSimple::tVec tVec;

    //! Vertex handle to index map
    typedef std::map<CMesh::VertexHandle, size_t> tVHIdMap;

public:
    //! Constructor simple
    CMeshIndexing() {}

    //! Constructor initializing
    CMeshIndexing(CMesh *mesh) {create(mesh);}

    //! Construct indexing from the mesh
    void create(CMesh *mesh);    

    //! Actualize indexing
    void actualize();

    //! Map from vertex handle to index
    tVHIdMap vhid_map;

    //! Source mesh pointer
    CMeshPtr meshPtr;

    //! Compute mesh point again
    tVec meshPoint(size_t id) 
    {
        const CMesh::Point &p(meshPtr->point((*this)[id].vh));
        return tVec(p[0], p[1], p[2]);
    }
};

typedef CMeshIndexing::tSmartPtr CMeshIndexingPtr;

// CModel_H_included
#endif	

