///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#ifndef kdtree_h_included
#define kdtree_h_included

#include <nanoflann.hpp>
#include <base/CModel.h>
#include <vector>

/**
	Interface between nanoflann and openmesh model
*/
class CPointCloudAdaptorOM : public vpl::base::CObject
{
public:
	VPL_SHAREDPTR(CPointCloudAdaptorOM);

    typedef vpl::math::CDVector3 tVec;

public:
    //! Simple constructor
    CPointCloudAdaptorOM();

	//! Constructor
	CPointCloudAdaptorOM(CMesh * model);

    // Returns number of points
    size_t kdtree_get_point_count() const {return m_indexing->size();}

	// Returns the distance between the vector "p1[0:size-1]" and the data point with index "idx_p2" stored in the class:
	inline double kdtree_distance(const double *p1, const size_t idx_p2,size_t size) const;

	// Returns the dim'th component of the idx'th point in the class:
	// Since this is inlined and the "dim" argument is typically an immediate value, the
	//  "if/else's" are actually solved at compile time.
	inline double kdtree_get_pt(const size_t idx, int dim) const;

	// Optional bounding-box computation: return false to default to a standard bbox computation loop.
	//   Return true if the BBOX was already computed by the class and returned in "bb" so it can be avoided to redo it again.
	//   Look at bb.size() to find out the expected dimensionality (e.g. 2 or 3 for point clouds)
	template <class BBOX>
	bool kdtree_get_bbox(BBOX &bb) const 
	{
		if(m_bbmin[0]>m_bbmax[0])
			return false;

		for(size_t dim = 0; dim < bb.size(); ++dim)
		{
			bb[dim].low =	m_bbmin[dim];
			bb[dim].high =	m_bbmax[dim];
		}

		return true;
	}

	//! Get vertex handle by id
	CMesh::VertexHandle &getVH(size_t id);

    //! Get vertex coordinates in vpl format
    const tVec &getPointVPL(size_t id){return (*m_indexing)[id].point;}

    //! Get normal coordinates in VPL format
    const tVec &getNormalVPL(size_t id){return (*m_indexing)[id].normal;}

    //! Get indexing
    const CMeshIndexing &getIndexing() const {return *m_indexing;}

    //! Get mesh indexing pointer - full access, dangerous!
    CMeshIndexing *getIndexingPtr() {return m_indexing;}

    //! Does it have data?
    bool hasData() const {return m_indexing != 0 && m_indexing->size() > 0; }

protected:
	//! Model pointer
	CMeshPtr m_mesh;

	//! Mesh vertices 
	CMeshIndexingPtr m_indexing;

	//! Mesh bounding box
	CMesh::Point m_bbmin, m_bbmax;
};

//! Smart pointer to the point cloud adaptor
typedef CPointCloudAdaptorOM::tSmartPtr CPointCloudAdaptorOMPtr;

/** 
 *	KD tree for OpenMesh 
 */
class CKDTreeOM : public vpl::base::CObject
{
public:
	// Smart pointer definition
	VPL_SHAREDPTR(CKDTreeOM);

    //! Index vector type
    typedef std::vector<size_t> tIndexVec;

    //! Distance vector type
    typedef std::vector<double> tDistanceVec;

    //! Neighbor searches output type
    struct SIndexDistancePairs
    {
        // Constructors
        SIndexDistancePairs(){}
        SIndexDistancePairs(size_t allocated) : indexes(allocated), distances(allocated) {}

        tIndexVec indexes;
        tDistanceVec distances;
    };

protected:
	//! KD tree type
	typedef nanoflann::KDTreeSingleIndexAdaptor< nanoflann::L2_Simple_Adaptor<double, CPointCloudAdaptorOM>, CPointCloudAdaptorOM, 3>  tKDTreeAdaptor;

    //! Coordinate vector type
    typedef vpl::math::CDVector3 tVec;

public:
	//! Simple constructor
	CKDTreeOM();

	//! Destructor
	~CKDTreeOM();

	//! Initializing constructor
	CKDTreeOM(CMesh *mesh);

	//! Initialize tree
	bool create(CMesh *mesh);

    //! Has tree data?
    bool hasData() const {return m_tree != 0 && m_tree->size() > 0;}

    //! Get closest point index and distance
    bool getClosestPoints(const tVec &point, SIndexDistancePairs &result) const;

    //! Get point vertex handle
    const CMesh::VertexHandle getPointVH(size_t idx) const { return m_pc->getVH(idx);}

    //! Get point coordinates in vpl format
    const tVec &getPointVPL(size_t idx) const {return m_pc->getPointVPL(idx);}

    //! Get normal coordinates
    const tVec &getNormalVPL(size_t idx) const {return m_pc->getNormalVPL(idx);}

    //! Get indexing 
    const CMeshIndexing &getIndexing() const { return m_pc->getIndexing(); } 

    //! Get mesh indexing pointer - full access, dangerous!
    CMeshIndexing *getIndexingPtr() { return m_pc->getIndexingPtr(); }

    //! Size of tree - number of stored points
    size_t size() const {return m_tree->size();}

protected:
	//! Delete all previous data
	void clear();

protected:
	//! Mesh adaptor
	CPointCloudAdaptorOMPtr m_pc;

	//! KD tree
	tKDTreeAdaptor *m_tree;
};

//! Smart pointer for KD tree 
typedef CKDTreeOM::tSmartPtr CKDTreeOMPtr;

// kdtree_h_included
#endif