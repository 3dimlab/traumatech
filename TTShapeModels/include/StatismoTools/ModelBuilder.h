///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#ifndef ModelBuilder_H_included
#define ModelBuilder_H_included

#include "StatismoModel.h"
#include <VPL/Math/TransformMatrix.h>
#include "base/CModel.h"

namespace statismo
{
    class CModelBuilder : public vpl::base::CObject
    {
    public:
        VPL_SHAREDPTR(CModelBuilder);

        //! Transform matrix type
        typedef vpl::math::CTransformMatrix<double> tTransform;

        //! 3D point type
        typedef vpl::math::CDVector3 tVector;

    public:
        //! Constructor
        CModelBuilder();

        //! Destructor
        ~CModelBuilder() { reset(); }

        //! Add model to builder
        bool addModel(CMesh &mesh);

        //! Reset builder
        void reset();

        //! Build output model
        CStatismoModel *buildModel();      

    protected:
        //! Build model from representer and samples matrix
        CStatismoModel *buildModel(const COMMeshRepresenter &representer, const MatrixType &Samples);

    protected:
        //! Meshes vector type
        typedef std::vector<CMeshPtr> tMeshVec;

    protected:
        //! Stored meshes data
        tMeshVec m_meshes;

        //! Number of needed points
        size_t m_numPoints;
    };

    //! Smart pointer type
    typedef CModelBuilder::tSmartPtr CModelBuilderPtr;

} // namespace statismo


// ModelBuilder_H_included
#endif
