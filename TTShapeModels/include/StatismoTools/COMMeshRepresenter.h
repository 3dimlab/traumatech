///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#ifndef COMMeshRepresenter_H_included
#define COMMeshRepresenter_H_included

#include <H5Cpp.h>
// Open mesh
#include "base/CModel.h"
// Statismo
#include <Representer.h>

namespace statismo {

    /**
     * \brief   Representer traits based on OpenMesh mesh representation.
     */
    template <>
    struct RepresenterTraits<CMesh> {
        typedef CMesh *DatasetPointerType;
        typedef const CMesh *DatasetConstPointerType;

        typedef CMesh::Point PointType;
        typedef CMesh::Point ValueType;

        ///@}
    };

    /**
     * \brief   Mesh representer which stores representer data in OpenMesh format.
     */
    class COMMeshRepresenter : public Representer<CMesh>, public vpl::base::CObject
    {
    public:
        VPL_SHAREDPTR(COMMeshRepresenter);

    public:
        //! Create new representer
        static COMMeshRepresenter* Create() {
            return new COMMeshRepresenter();
        }

        //! Create copy of representer
        static COMMeshRepresenter* Create(DatasetConstPointerType reference) {
            return new COMMeshRepresenter(reference);
        }

        //! Load representer data from H5 group
        void Load(const H5::Group& fg);

        //! Clone representer
        COMMeshRepresenter* Clone() const;

        //! Delete myself
        void Delete() const {
            delete this;
        }

        //! Destructor
        virtual ~COMMeshRepresenter();

        //! Get representer name as a string value
        std::string GetName() const {
            return "COMMeshRepresenter";
        }

        //! Get model dimensions 
        unsigned GetDimensions() const {
            return 3;
        }

        //! Get representer version 
        std::string GetVersion() const {
            return "0.1" ;
        }

        //! Get type of representer
        RepresenterDataType GetType() const {
            return POLYGON_MESH;
        }

        //! Get representer domain
        const DomainType& GetDomain() const {
            return m_domain;
        }

        //! Delete dataset as object
        void DeleteDataset(DatasetPointerType d) const {
            // We are using smart pointer - this deletes dataset
            d = 0;
        };

        //! Clone dataset (in our case use copy constructor)
        DatasetPointerType CloneDataset(DatasetConstPointerType d) const {
            return new CMesh(*d);
        }

        //! Get reference to the data
        DatasetConstPointerType GetReference() const {
            return m_reference;
        }

        //! Convert stored point to the vector
        statismo::VectorType PointToVector(const PointType& pt) const;
        //! Convert mesh to statismo vector of vectors
        statismo::VectorType SampleToSampleVector(DatasetConstPointerType sample) const;
        //! Convert statismo sample format to mesh
        DatasetPointerType SampleVectorToSample(const statismo::VectorType& sample) const;
        //! Get point of mesh with given number
        ValueType PointSampleFromSample(DatasetConstPointerType sample, unsigned ptid) const;
        //! Convert OM point to statismo vector
        statismo::VectorType PointSampleToPointSampleVector(const ValueType& v) const;
        //! Convert statismo vector to OM vector
        ValueType PointSampleVectorToPointSample(const statismo::VectorType& pointSample) const;

        //! Save data to the H5 file
        void Save(const H5::Group& fg) const;
        //! Returns number of mesh points 
        unsigned GetNumberOfPoints() const { return (unsigned)m_reference->n_vertices(); }
        //! Convert point to unique id
        unsigned GetPointIdForPoint(const PointType& point) const;

        //! Get mesh access
        CMesh &getMesh() {return *m_reference;}
    private:
 
        COMMeshRepresenter() : m_reference(0) {}
        COMMeshRepresenter(const std::string& reference) {}
        COMMeshRepresenter(const DatasetConstPointerType reference) : m_reference(0) { SetReference(reference); }
        COMMeshRepresenter(const COMMeshRepresenter& orig) {}
        COMMeshRepresenter& operator=(const COMMeshRepresenter& rhs) {}

        void SetReference(const DatasetConstPointerType reference);

        CMesh* LoadRefLegacy(const H5::Group& fg) const;
        CMesh* LoadRef(const H5::Group& fg) const;

        DatasetPointerType m_reference;

        DomainType m_domain;

      
    }; // class COMMeshRepresenter

    //! Smart pointer type
    typedef COMMeshRepresenter::tSmartPtr COMMeshRepresenterPtr;

} // namespace statismo

#endif