///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#ifndef StatismoModel_H_included
#define StatismoModel_H_included

// VPL
#include <VPL/Base/Object.h>

// Representer type
#include "COMMeshRepresenter.h"

// Statismo
#include "StatisticalModel.h"

namespace statismo
{

class CStatismoModel : public vpl::base::CObject
{
public:
    VPL_SHAREDPTR(CStatismoModel);

    //! Vector of components type
    typedef std::vector<double> tCoefficients;

    //! Type of statismo implementation object
    typedef statismo::StatisticalModel<CMesh> tStatismoImpl;

public:
    //! Default constructor
    CStatismoModel() : m_statismo(0) {}

    //! Copy constructor
    CStatismoModel(const CStatismoModel &model);

    //! Create statismo model from base statismo object
    CStatismoModel(tStatismoImpl *s) : m_statismo(s) {}

    //! Destructor
    ~CStatismoModel() { setNewSModel(0); }

    //! Load model from file
    bool Load(const std::string &filename);
    
    //! Load model from open H5 group
    bool Load(const H5::Group& modelRoot);

    //! Save model to file
    bool Save(const std::string &filename);

    //! Get model representer
    COMMeshRepresenter &getRepresenter() {return *m_representer;}

    //! Get number of principal components
    size_t numCoefficients() const {return m_statismo->GetNumberOfPrincipalComponents();}

    //! Create mean model
    COMMeshRepresenter::DatasetPointerType mean() const;
    
    //! Create random model
    COMMeshRepresenter::DatasetPointerType random() const;

    //! Get variances vector
    size_t getVariances(tCoefficients &variances) const;

    //! Create model with given setting
    COMMeshRepresenter::DatasetPointerType sample(const tCoefficients &coefficients) const;

protected:
    //! Set new statistical model
    void setNewSModel(tStatismoImpl *smodel);

protected:
    //! Mesh representer
    COMMeshRepresenterPtr m_representer;

    //! Statismo computation object
    tStatismoImpl *m_statismo;

}; // class CStatismoModel

//! Declare smart pointer type
typedef CStatismoModel::tSmartPtr CStatismoModelPtr;

} // namespace statismo


// StatismoModel_H_included
#endif