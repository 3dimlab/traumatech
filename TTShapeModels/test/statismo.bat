:: Statistic models creator loads all files in the given directory (with given
:: file name mask) and creates statistic model file 
StatisticModelCreatorApp -id ./working/ -of ./ssm.h5 -mask *.stl 