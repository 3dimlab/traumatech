///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#include <alg/icp.h>
#include <VPL/Math/Vector.h>
#include <VPL/Math/MatrixFunctions.h>

// ================================================================================================
// CLASS CICPBase
// ================================================================================================

/**
 * \brief   Constructor.
 *
 * \param   max_inlier_distance The maximum inlier distance.
 */
CICPBase::CICPBase(double max_inlier_distance /*= 1.0*/)
    : m_maxInlierDistance(max_inlier_distance)
    , m_minimalChangeThreshold(0.05) 
    , m_maxDistanceStep(1.0)
    , m_maxRotationAngleStep(vpl::math::deg2Rad(5.0))
    , m_msDivider(10.0)
    , m_bComputeScale(true)
{
}

/**
 * \brief	Applies this object.
 *
 * \param	smodel	The smodel.
 * \param	model 	The model.
 *
 * \return	true if it succeeds, false if it fails.
 */

bool CICPBase::apply(CMesh *target, CMesh *source, int num_iterations, bool estimate_inlier_distance_automaticaly /*= true*/, bool estimate_step_limits /*= true*/)
{
    // Reset initial transform
    m_tm.unit();

    // Build KD-tree for static model
    m_kdtreeTarget.create(target);

    // Build indexing for transformed model
    m_modelIdxSource = new CMeshIndexing(source);

    if(estimate_inlier_distance_automaticaly)
        estimateMaxInlierDistance(target, true);

    // Estimate first transform matrix
    double current_distance(estimateInitialTransform(target, source));

    if(estimate_step_limits)
    {
        m_maxDistanceStep = current_distance / double(m_msDivider);
        m_maxRotationAngleStep = vpl::math::deg2Rad(5.0);
    }

    // Create dummy masks if no masks given or masks differs in number of points
    if(m_maskSource.size() != m_modelIdxSource->size() || m_maskTarget.size() != m_kdtreeTarget.getIndexing().size())
    {
        // All input points should be used
        m_maskSource.resize(m_modelIdxSource->size(), true);
        m_maskTarget.resize(m_kdtreeTarget.getIndexing().size(), true);
    }

    // Modify maximal inlier distance to surely reach on currently transformed model 
//    m_maxInlierDistance += current_distance;

    // Apply internal algorithm
    bool rv(applyInternal(m_tm, num_iterations));

    return rv;
}

/**
 * \brief   Estimate transform.
 *
 * \param [in,out]  smodel  If non-null, the smodel.
 * \param [in,out]  model   If non-null, the model.
 */
double CICPBase::estimateInitialTransform(CMesh *smodel, CMesh *model)
{
    // Compute bounding boxes for both models
    CMesh::Point min, max, csource, ctarget;
    smodel->calc_bounding_box(min, max);
    double max_distance(std::max(std::max(max[0] - min[0], max[1] - min[1]), max[2] - min[2]));
    ctarget = (max + min)/2.0;
    model ->calc_bounding_box(min, max);
    csource = (max + min)/2.0;
    max_distance = std::max<double>(max_distance, std::max(std::max(max[0] - min[0], max[1] - min[1]), max[2] - min[2]));

    // Movement direction is from source to target and distance should on greater model size
    CMesh::Point direction(ctarget - csource);
//     direction.normalize();
//     CMesh::Point new_center(ctarget + direction*max_distance);  // new center lays in the direction from target to source on the max_distance distance
//     direction = new_center - csource; // Compute translation vector from source to new center point
    m_tm.setTrans(direction[0], direction[1], direction[2]);
 //   m_tm.makeIdentity();
    // Return distance from new center to the target center
    return max_distance;
}

/**
 * \brief   Estimate maximum inlier distance.
 *
 * \param [in,out]  model   If non-null, the model.
 * \param   set_it          true to set iterator.
 *
 * \return  .
 */
double CICPBase::estimateMaxInlierDistance(CMesh *model, bool set_it /*= true*/)
{
    // Get model simple bounding box
    CMesh::Point min, max;
    model->calc_bounding_box(min, max);

    double max_distance(std::max(std::max(max[0] - min[0], max[1] - min[1]), max[2] - min[2]));
    max_distance /= 1.5;

    if(set_it)
        m_maxInlierDistance = max_distance;

    return max_distance;
}

/**
 * \brief   Sum differences.
 *
 * \param   m1  The first const tTransform &amp;
 * \param   m2  The second const tTransform &amp;
 *
 * \return  sum of squared differences.
 */
double CICPBase::sumDifferences(const tTransform &m1, const tTransform &m2)
{
    double sum(.0), d;
    double sum_matrix(0.0);
    for( int j = 0; j < 4; ++j)
        for( int i = 0; i < 4; ++i)
        {
            sum_matrix += fabs(m1(i, j));
            d = m1(i, j) - m2(i, j);
            sum += fabs(d);//*d;

        }

    return sum / sum_matrix;
}


/**
 * \brief   Calculates transformation by the Umeyama method.
 * 			The algorithm is based on: "Least-squares estimation of transformation parameters between two point patterns", 
 * 			Shinji Umeyama, PAMI 1991, DOI: 10.1109/34.88573
 * 			
 *          http://web.stanford.edu/class/cs273/refs/umeyama.pdf    17. 2. 2015
 *
 * \param   correspondences     The correspondences.
 * \param   last_correspondence The last correspondence.
 * \param   solve_scaling       true to solve scaling.
 *
 * \return  The calculated transform.
 */
CICPBase::tTransform CICPBase::computeUmeyama(const tCorrespondenceVec &correspondences, size_t last_correspondence, bool solve_scaling)
{
    // Convert points to the Eigen format
    typedef Eigen::Matrix<double, 3, Eigen::Dynamic> EPointMatrix;
    EPointMatrix points_src (3, last_correspondence);
    EPointMatrix points_tgt (3, last_correspondence);

    tCorrespondenceVec::const_iterator it(correspondences.begin());
    for(size_t lc = 0; lc < last_correspondence; ++lc, ++it)
    {
        // Get source point
        const tVector &source((*m_modelIdxSource)[it->idFirst].point);

        // Get target point
        const tVector &target(m_kdtreeTarget.getPointVPL(it->idSecond));

        points_src(0, lc) = source(0);
        points_src(1, lc) = source(1);
        points_src(2, lc) = source(2);

        points_tgt(0, lc) = target(0);
        points_tgt(1, lc) = target(1);
        points_tgt(2, lc) = target(2);

    }

    // Some typedefs
    typedef vpl::math::CDMatrix3x3::tStorage tDMatrix3x3;   // Used computational matrix - 3x3 doubles
    typedef vpl::math::CDVector3::tStorage  tDVector3;      // Vector type - three doubles
    typedef Eigen::internal::plain_matrix_type_row_major<EPointMatrix>::type RowMajorMatrixType;

    // Means are divided by number of correspondences
    const double div_num_correspondences(1.0 / static_cast<double>(last_correspondence));

    // computation of means - sum of vectors divided by number of points
    const tDVector3 mean_src(points_src.rowwise().sum() * div_num_correspondences);
    const tDVector3 mean_tgt(points_tgt.rowwise().sum() * div_num_correspondences);

    // demeaning: point = point - mean point
    const RowMajorMatrixType demeaned_src(points_src.colwise () - mean_src);
    const RowMajorMatrixType demeaned_tgt(points_tgt.colwise () - mean_tgt);

    // Variance computation: Eq. (36)-(37)
    const double variance_src(demeaned_src.rowwise ().squaredNorm ().sum () * div_num_correspondences);

    // Covariance matrix computation: Eq. (38)
    const tDMatrix3x3 sigma(div_num_correspondences * demeaned_tgt * demeaned_src.transpose());

    // Singular values decomposition object
    Eigen::JacobiSVD<tDMatrix3x3> svd (sigma, Eigen::ComputeFullU | Eigen::ComputeFullV);

    // Estimated transform matrix - identity in the beginning
    tTransform::tStorage CTM(tTransform::tStorage::Identity(4, 4));

    // Prepare scale vector: Eq. (39)
    tDVector3 S(tDVector3::Ones(3));
    if (sigma.determinant () < 0) 
        S(2) = -1;

    // Determine optimal parameters
    const tDVector3& d = svd.singularValues();

    size_t rank = 0; 
    // Find rank much bigger than the others
    for (size_t i = 0; i < 3; ++i) 
        if (!Eigen::internal::isMuchSmallerThan (d.coeff (i), d.coeff (0))) 
            ++rank;

    // Eq. (44, 45)
    if (rank == 2) 
    {
        if (svd.matrixU().determinant() * svd.matrixV().determinant() > 0) 
            CTM.block(0, 0, 3, 3).noalias() = svd.matrixU() * svd.matrixV().transpose ();
        else 
        {
            const double s(S(2)); 
            S(2) = -1;
            CTM.block(0, 0, 3, 3).noalias() = svd.matrixU() * S.asDiagonal() * svd.matrixV().transpose();
            S(2) = s;
        }
    } 
    else 
    {
        // Eq. 40 - 42
        CTM.block(0, 0, 3, 3).noalias() = svd.matrixU() * S.asDiagonal() * svd.matrixV().transpose ();
    }

    // If scaling should be solved
    if (solve_scaling)
    {
        // Eq. (42)
        const double c(1.0 / variance_src * svd.singularValues().dot(S));

        // Eq. (41)
        CTM.col(3).head(3) = mean_tgt;
        CTM.col(3).head(3).noalias () -= c * CTM.topLeftCorner(3, 3) * mean_src;
        CTM.block(0, 0, 3, 3) *= c;
    }
    else
    {
        CTM.col(3).head(3) = mean_tgt;
        CTM.col(3).head(3).noalias() -= CTM.topLeftCorner(3, 3) * mean_src;
    }

    return tTransform(CTM);
}


// ================================================================================================
// CLASS CPointPointICP
// ================================================================================================

/**
 * \brief   Applies the internal.
 *
 * \param [in,out]  smodel      If non-null, the smodel.
 * \param [in,out]  model       If non-null, the model.
 * \param [in,out]  transform   The transform.
 *
 * \return  true if it succeeds, false if it fails.
 */
bool CPointPointICP::applyInternal(tTransform &transform, int num_iterations)
{
    // Not enough points
    if((!m_kdtreeTarget.hasData()) || m_modelIdxSource->size() == 0)
        return false;

    // Current transform matrix
    tTransform ctm(transform), lastMatrix(transform);

    int step(0);

    size_t num_points(m_modelIdxSource->size());
    size_t num_selected(num_points/5);

    bool finished(step >= num_iterations);

    // Found pairs array
    tCorrespondenceVec correspondences(num_points);
    size_t last_correspondence(0);

    // Distance query results
    size_t num_kd_result(10);
    CKDTreeOM::SIndexDistancePairs result(num_kd_result);

    // Create shuffled vector of indexes for RANSAC
    std::vector<unsigned int> indices(num_points);
    std::iota(indices.begin(), indices.end(), 0);
 
    // Main loop
    while(!finished)
    {
        // "clear" correspondences vector
        last_correspondence = 0;

        std::random_shuffle(indices.begin(), indices.end());

        // For all model points
        for(size_t id = 0; id < num_selected; ++id)
        {
            // Get index
            size_t pid(indices[id]);

            // Test with mask
            if(!m_maskSource[pid])
                continue;

            // Transform input point with current matrix
            tVector transformed(ctm * (*m_modelIdxSource)[pid].point);

            // Find closest target point
            m_kdtreeTarget.getClosestPoints(transformed, result);

            // Try to find first usable point
            size_t rid(0);
            for(; rid < num_kd_result; ++rid)
                if(m_maskTarget[result.indexes[rid]])
                    break;

            // No near target point found
            if(rid == num_kd_result)
                continue;

            // If not too far
            if(sqrtf(result.distances[rid]) < m_maxInlierDistance)
            {
                // Store points distance to the correspondence vector
                correspondences[last_correspondence].idFirst = pid;
                correspondences[last_correspondence].idSecond = result.indexes[rid];
                correspondences[last_correspondence].first = transformed;
                correspondences[last_correspondence].second = m_kdtreeTarget.getPointVPL(result.indexes[rid]);
                correspondences[last_correspondence].distance = result.distances[rid];
                correspondences[last_correspondence].weight = fabs((*m_modelIdxSource)[pid].normal*m_kdtreeTarget.getNormalVPL(result.indexes[rid]));
                ++last_correspondence;
            }

        }

//        std::cout << last_correspondence << std::endl;
        if(last_correspondence == 0)
        {
            finished = true;
            break;
        }

        // Compute and accumulate the transformation
        ctm = computeUmeyama(correspondences, last_correspondence, m_bComputeScale);

        // Modify maximal inlier distance 
//         if(step < m_msDivider)
//             m_maxInlierDistance -= m_maxDistanceStep;

        // Increase step and test end conditions
        ++step;

        // Compute what has changed
        double sd(sumDifferences(lastMatrix, ctm));
        lastMatrix = ctm;

        // Set finished condition value
        finished = step >= num_iterations || sd < m_minimalChangeThreshold;

//*
        std::cout << "Transform :" << sd << ", " << step << std::endl;
//         for( int i = 0; i < 4; ++i)
//         {
//             for( int j = 0; j < 3; ++j)
//             {
//                 std::cout << ctm(i, j) << ", ";
//             }
//             std::cout << ctm(i, 3) << std::endl;
//         }

//*/
    }

    transform = ctm;

    tVector v(ctm.getTrans<double>());

    return true;
}

// ================================================================================================
// CLASS CPointPlaneICP
// ================================================================================================

/**
 * \brief   Applies the internal.
 *
 * \param [in,out]  transform   The transform.
 * \param   num_iterations      Number of iterations.
 *
 * \return  true if it succeeds, false if it fails.
 */

bool CPointPlaneICP::applyInternal(tTransform &transform, int num_iterations)
{
     // Not enough points
    if((!m_kdtreeTarget.hasData()) || m_modelIdxSource->size() == 0)
        return false;

    // Compute planes
    computePlanes(m_kdtreeTarget.getIndexing(), m_planes);

    // Current transform matrix
    tTransform ctm(transform), lastMatrix(transform);

    int step(0);

    size_t num_points(m_modelIdxSource->size());
    size_t num_selected(num_points/5);

    // Number of tested points
    size_t K(std::min<size_t>(num_points, 10));

    bool finished(step >= num_iterations);

    // Found pairs array
    tCorrespondenceVec correspondences(num_points);
    size_t last_correspondence(0);

    // Distance query results
    CKDTreeOM::SIndexDistancePairs result(K);

    // Create shuffled vector of indexes for RANSAC
    std::vector<unsigned int> indices(num_points);
    std::iota(indices.begin(), indices.end(), 0);

    // Main loop
    while(!finished)
    {
        // "clear" correspondences vector
        last_correspondence = 0;

        // Shuffle indexes
        std::random_shuffle(indices.begin(), indices.end());

        // For all model points
        for(size_t nid = 0; nid < num_selected; ++nid)
        {
            // Get index
            size_t pid(indices[nid]);

            // Test with mask
            if(!m_maskSource[pid])
                continue;

            // Transform input point with current matrix
            tVector transformed(ctm * (*m_modelIdxSource)[pid].point);

            // Find K closest target point
            m_kdtreeTarget.getClosestPoints(transformed, result);

            // Find best fit
            size_t best_id(0); 
            double best_distance(std::numeric_limits<double>::max());
            for(size_t id = 0; id < K; ++id)
            {
                // Test with target mask
                if(!m_maskTarget[result.indexes[id]])
                    continue;

                // Get plane stats
                const SHNF &plane(m_planes[result.indexes[id]]);

                // Compute point to plane distance
                double distance(abs(plane.n * transformed + plane.p)); 

                // Too far?
                if(distance > m_maxInlierDistance)
                    continue;

                // Compare with current best
                if(distance < best_distance)
                {
                    best_distance = distance;
                    best_id = id;
                }
            }

            if(best_id < K)
            {
                // Store points distance to the correspondence vector
                correspondences[last_correspondence].idFirst = pid;
                correspondences[last_correspondence].idSecond = result.indexes[best_id];
                correspondences[last_correspondence].first = transformed;
                correspondences[last_correspondence].second = m_kdtreeTarget.getPointVPL(result.indexes[best_id]);
                correspondences[last_correspondence].distance = result.distances[best_id];
                correspondences[last_correspondence].weight = fabs((*m_modelIdxSource)[pid].normal*m_kdtreeTarget.getNormalVPL(result.indexes[best_id]));
                ++last_correspondence;
            }
            
        }

//        std::cout << last_correspondence << std::endl;
        if(last_correspondence == 0)
        {
            finished = true;
            break;
        }


        // Compute and accumulate the transformation
        ctm = computeUmeyama(correspondences, last_correspondence, m_bComputeScale);

        // Modify maximal inlier distance (it was increased to reach on distant model)
//         if(step < m_msDivider)
//             m_maxInlierDistance -= m_maxDistanceStep;

        // Increase step and test end conditions
        ++step;

        // Compute what has changed
        double sd(sumDifferences(lastMatrix, ctm));
        lastMatrix = ctm;

        // Set finished condition value
        finished = step >= num_iterations || sd < m_minimalChangeThreshold;

//*
        std::cout << "Transform " << step << ": " << sd << std::endl;
/*
        for( int i = 0; i < 4; ++i)
        {
            for( int j = 0; j < 3; ++j)
            {
                std::cout << ctm(i, j) << ", ";
            }
            std::cout << ctm(i, 3) << std::endl;
        }
//*/
    }

    transform = ctm;
    return true;
}

/**
 * \brief   Calculates the planes.
 *
 * \param   indexing        The indexing.
 * \param [in,out]  planes  The planes.
 */
void CPointPlaneICP::computePlanes(const CMeshIndexing &indexing, tHNFVec &planes)
{
    VPL_ASSERT(indexing.size() > 0);

    // Resize output vector
    planes.resize(indexing.size());

    // For all points
    CMeshIndexing::const_iterator iti(indexing.begin()), itiEnd(indexing.end());
    size_t id(0);
    for(; iti != itiEnd; ++iti, ++id)
    {
        // Reference to plane
        SHNF &plane(planes[id]);

        // Store and normalize normal vector
        plane.n = iti->normal;
        plane.n.normalize();

        // For normalized normal vector p == d
        plane.p = -(plane.n*iti->point);
    }
}

// ================================================================================================
// CLASS CPlanePlaneICP
// ================================================================================================

/**
 * \brief   Applies this object.
 *
 * \param [in,out]  target                          If non-null, the target model.
 * \param [in,out]  source                          If non-null, the source model.
 * \param   num_iterations                          Number of iterations.
 * \param   estimate_inlier_distance_automaticaly   true to estimate inlier distance
 *                                                  automatically.
 *
 * \return  true if it succeeds, false if it fails.
 *
 * ### param [in,out]   transform   The transformation matrix.
 */

bool CPlanePlaneICP::apply(CMesh *target, CMesh *source, int num_iterations, bool estimate_inlier_distance_automaticaly /*= true*/)
{
    // Reset initial transform
    m_tm.unit();

    // Build KD-tree for source model
    m_kdtreeSource.create(source);

    *m_modelIdxSource = m_kdtreeSource.getIndexing();

    // Build KD-tree for target model
    m_kdtreeTarget.create(target);

    // Estimate maximal inlier distance if it should be done
    if(estimate_inlier_distance_automaticaly)
        estimateMaxInlierDistance(target, true);

    // Create dummy masks if no masks given or masks differs in number of points
    if(m_maskSource.size() != m_modelIdxSource->size() || m_maskTarget.size() != m_kdtreeTarget.getIndexing().size())
    {
        // All input points should be used
        m_maskSource.resize(m_modelIdxSource->size(), true);
        m_maskTarget.resize(m_kdtreeTarget.getIndexing().size(), true);
    }

    // Estimate first transform matrix
    estimateInitialTransform(target, source);

    // Apply internal algorithm
    bool rv(applyInternal(m_tm, num_iterations));

    return rv;

}

/**
 * \brief   Applies the internal.
 *
 * \param [in,out]  transform   The transform.
 * \param   num_iterations      Number of iterations.
 *
 * \return  true if it succeeds, false if it fails.
 */

bool CPlanePlaneICP::applyInternal(tTransform &transform, int num_iterations)
{
    // Not enough points
    if((!m_kdtreeSource.hasData()) || (!m_kdtreeTarget.hasData()))
        return false;

    // Compute covariant matrices for source and target
    computeCovariantMatrices(m_kdtreeSource, m_covSource);
    computeCovariantMatrices(m_kdtreeTarget, m_covTarget);

    // Get size of source and target 
    size_t numSource(m_kdtreeSource.size());

    // Current transform matrix
    tTransform ctm(transform), lastMatrix(transform);

    int step(0);

    size_t num_points(numSource);
    size_t num_selected(num_points/5);

    // 
    bool finished(step >= num_iterations);

    // Found pairs array
    tCorrespondenceVec correspondences(num_points);
    size_t last_correspondence(0);

    // Distance query results
    size_t num_kd_points(10);
    CKDTreeOM::SIndexDistancePairs result(num_kd_points);

    // Create shuffled vector of indexes for RANSAC
    std::vector<unsigned int> indices(num_points);
    std::iota(indices.begin(), indices.end(), 0);

    // Convert guess matrix to eigen format

    // Main loop
    while(!finished)
    {
        // Create eigen transformation matrix for easier computation with eigen covariance matrices
        Eigen::Matrix3d R = ctm.asEigen().topLeftCorner<3,3> ();

        // "clear" correspondences vector
        last_correspondence = 0;

        std::random_shuffle(indices.begin(), indices.end());

        // For all model points
        for(size_t nid = 0; nid < num_selected; ++nid)
        {
            // Get index
            size_t pid(indices[nid]);

            // Test with mask
            if(!m_maskSource[pid])
                continue;

            // Transform input point with current matrix
            tVector transformed(ctm * m_kdtreeSource.getPointVPL(pid));

            // Find closest target point
            if(!m_kdtreeTarget.getClosestPoints(transformed, result))
                continue;

            // Try to find first useable point
            size_t rid(0);
            for(; rid < num_kd_points; ++rid)
                if(m_maskTarget[result.indexes[rid]])
                    break;

            // No near target point found
            if(rid == num_kd_points)
                continue;

            // Test distance 
            if(result.distances[rid] < m_maxInlierDistance)
            {
                // Source covariance matrix
                tEMatrix &C1 = m_covSource[nid];
                // Target covariance matrix
                tEMatrix &C2 = m_covTarget[result.indexes[rid]];
                // M = R*C1
                tEMatrix M(R * C1);

                // temp = M*R' + C2 = R*C1*R' + C2
                Eigen::Matrix3d temp = M * R.transpose();        
                temp += C2;

                // M = temp^-1
                M = temp.inverse ();

                // Store correspondence
                correspondences[last_correspondence].idFirst = pid;
                correspondences[last_correspondence].idSecond = result.indexes[rid];
                correspondences[last_correspondence].first = transformed;
                correspondences[last_correspondence].second = m_kdtreeTarget.getPointVPL(result.indexes[rid]);
                
                const tVector &tpoint(correspondences[last_correspondence].second);
                Eigen::Vector3d res(transformed[0] - tpoint[0], transformed[1] - tpoint[1], transformed[2] - tpoint[2]);
                Eigen::Vector3d tmp(M * res);
                correspondences[last_correspondence].weight = fabs(double(res.transpose() * tmp));
                ++last_correspondence;
            }
        } // For all source model points
        
        // No correspondences found? Do nothing
        if(last_correspondence == 0)
        {
            finished = true;
            break;
        }

        // Estimate transform
        ctm = computeUmeyama(correspondences, last_correspondence, m_bComputeScale);

        // Increase step and test end conditions
        ++step;

        // Compute what has changed
        double sd(sumDifferences(lastMatrix, ctm));
        lastMatrix = ctm;

        // Set finished condition value
        finished = step >= num_iterations || sd < m_minimalChangeThreshold;

/*
        std::cout << "Transform " << step << ": " << sd << std::endl;
/*
        for( int i = 0; i < 4; ++i)
        {
            for( int j = 0; j < 3; ++j)
            {
                std::cout << ctm(i, j) << ", ";
            }
            std::cout << ctm(i, 3) << std::endl;
        }

//         std::cout << "Scale: " << std::endl;
//         vpl::math::CDVector3 sc(ctm.getScale<double>());
//         for( int j = 0; j < 3; ++j)
//         {
//             std::cout << sc(j) << ", ";
//         }

        std::cout << "SD: " << sd << ", min SD: " << m_minimalChangeThreshold << std::endl;
//*/
    } // While ! finished

    transform = ctm;

    return true;
}

/**
 * \brief   Calculates the covariant matrices.
 *
 * \param   tree                The tree.
 * \param [in,out]  matrices    The matrices.
 * \param   num_neighbors       Number of neighbors.
 */

void CPlanePlaneICP::computeCovariantMatrices(const CKDTreeOM &tree, tEMatrixVec &matrices, size_t num_neighbors /*= 20*/)
{
    // Nothing to do?
    if(tree.size() == 0)
        return;

    // just for sure...
    size_t numN(std::min(num_neighbors, tree.size()));

    // Distance query results
    CKDTreeOM::SIndexDistancePairs result(numN);

    // Get indexing
    const CMeshIndexing &indexing(tree.getIndexing());

    // Resize matrices vector
    matrices.resize(indexing.size());

    // Mean buffer
    Eigen::Vector3d mean;

    // For all points and all matrices
    tEMatrixVec::iterator itm(matrices.begin());
    CMeshIndexing::const_iterator itp(indexing.begin()), itpEnd(indexing.end());
    for(; itp != itpEnd; ++itp, ++itm)
    {
        // Get matrix reference
        tEMatrix &cov(*itm);

        // Clear output buffers
        cov.setZero();
        mean.setZero();

        // Get point neighbors
        if(!tree.getClosestPoints(itp->point, result))
            continue;

        tVector::tElement x, y, z;
        // Compute covariance matrix
        // For all neighbor points
        for(size_t j = 0; j < numN; ++j)
        {
            // Get neighbor point coordinates
            const tVector &pt(tree.getPointVPL(result.indexes[j]));
          
            // Modify mean vector
            mean[0] += x = pt[0];
            mean[1] += y = pt[1];
            mean[2] += z = pt[2];

            // Modify covariance matrix
            cov(0,0) += x*x;

            cov(1,0) += y*x;
            cov(1,1) += y*y;

            cov(2,0) += z*x;
            cov(2,1) += z*y;
            cov(2,2) += z*z;    
        }

        // Divider
        double divider(1.0/static_cast<double> (numN));
        // Finalize mean vector
        mean *= divider;

        // Finalize covariance matrix
        for (int k = 0; k < 3; ++k)
            for (int l = 0; l <= k; ++l) 
            {
                cov(k,l) *= divider;
                cov(k,l) -= mean[k]*mean[l];
                cov(l,k) = cov(k,l);
            }

        // Covariance matrix is symmetric so we can use SVD to decompose covariance matrix 
        Eigen::JacobiSVD<Eigen::Matrix3d> svd(cov, Eigen::ComputeFullU);
        cov.setZero ();
        Eigen::Matrix3d U = svd.matrixU ();

        // Finalize covariance matrix
        for(int k = 0; k < 3; ++k) 
        {
            Eigen::Vector3d col = U.col(k);
            double v = 1.0; 

            if(k == 2)   
                v = 0.0001; // Small number

            cov+= v * col * col.transpose(); 
        }
    }

}
