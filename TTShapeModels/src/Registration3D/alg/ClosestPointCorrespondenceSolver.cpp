///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#include "alg/ClosestPointCorrespondenceSolver.h"

/**
 * \brief   Applies this object.
 *
 * \param [in,out]  target  If non-null, target for the.
 * \param [in,out]  source  If non-null, source for the.
 * \param   transformScaled The transform scaled.
 * \param   transformSimple The transform simple.
 *
 * \return  null if it fails, else.
 */
CMesh * ClosestPointCorrespondenceSolver::apply(CMesh *target, CMesh *source, const tTransform &transformScaled, const tTransform &transformSimple)
{
    // Get number of target points
    size_t num_points(target->n_vertices());
    if(num_points == 0)
        return 0;

    // Compute back transform from scaled model to source and next to the unscaled aligned model
    tTransform transform_scaled_inverse(transformScaled.asEigen().inverse());
    tTransform transform_simple_inverse(transformSimple.asEigen().inverse());

    // Create result model as copy of target mesh - topology should be the same
    target->garbage_collection();
    CMesh *result(new CMesh(*target));

    // Compute positioned and scaled version of the source mesh mesh
    CMeshPtr scaled_source(new CMesh(*source));
    scaled_source->transform(transformScaled);

    // Precompute triangle structures
    fillTriangles(scaled_source);

    // For all target mesh points
    size_t counter(0);
    CMesh::VIter rit(result->vertices_begin());
    double min_distance(std::numeric_limits<float>::max()), max_distance(-std::numeric_limits<float>::max()), avg_distance(0.0);
    for (CMesh::VertexIter pit = target->vertices_begin(); pit != target->vertices_end(); ++pit, ++rit, ++counter)
    {
        // Get point in vpl format
        const CMesh::Point &p(target->point(pit)); 
        const tVector p_vpl(p[0], p[1], p[2]);
        tVector c_vpl;

        // Find corresponding point on the transformed source mesh 
        double distance(findClosestPoint(p_vpl, c_vpl));

        // Stats
        min_distance = std::min(distance, min_distance);
        max_distance = std::max(distance, max_distance);
        avg_distance += distance;

        // We have intersection - transform it back from scaled transform and use simple transform to align the model
        c_vpl = transform_scaled_inverse * c_vpl;

        // Store point to result mesh
        result->set_point(rit, CMesh::Point(c_vpl(0), c_vpl(1), c_vpl(2)));
    }

    avg_distance /= float(num_points);

    std::cout << "Minimal distance: " << min_distance << ", maximal distance: " << max_distance << ", average: " << avg_distance << std::endl;
    // All done
    return result;

}

/**
 * \brief   Fill triangle precomputed triangle structures.
 *
 * \param [in,out]  mesh    If non-null, the mesh.
 */

void ClosestPointCorrespondenceSolver::fillTriangles(CMesh *mesh)
{
    // Get number of faces
    size_t num_triangles(mesh->n_faces());
    if(num_triangles == 0)
        return;

    // Resize structure
    m_triangles.resize(num_triangles);

    // For all faces
    size_t id(0);
    for(CMesh::FaceIter fit = mesh->faces_begin(); fit != mesh->faces_end(); ++fit, ++id)
    {
        // Get current structure reference
        STriangle &triangle(m_triangles[id]);

        // Get face points
        size_t count(0);
        for (CMesh::FaceVertexIter fvit = mesh->fv_begin(*fit); fvit != mesh->fv_end(*fit) && count < 3; ++fvit, ++count)
        {
            const CMesh::Point &p(mesh->point(fvit.handle()));
            triangle.points[count] = tVector(p[0], p[1], p[2]);
        }

        // Not enough points?
        if(count < 2)
            triangle.valid = false;

        triangle.edge0 = triangle.points[1] - triangle.points[0];
        triangle.edge1 = triangle.points[2] - triangle.points[0];
        triangle.a00 = (triangle.edge0 * triangle.edge0);
        triangle.a01 = (triangle.edge0 * triangle.edge1);
        triangle.a11 = (triangle.edge1 * triangle.edge1);
        triangle.det = triangle.a00 * triangle.a11 - triangle.a01 * triangle.a01;
        
        triangle.valid = (triangle.edge0 ^ triangle.edge1).length2() > std::numeric_limits<double>::min();
    }
}

/**
 * \brief   Searches for the nearest point of the triangle.
 *
 * \param   p               The const tVector &amp; to process.
 * \param   t               The const STriangle &amp; to process.
 * \param [in,out]  result  The result.
 *
 * \return  true if it succeeds, false if it fails.
 */
bool ClosestPointCorrespondenceSolver::findClosestPointToTriangle(const tVector &p, const STriangle &t, tVector &result)
{
    if(!t.valid)
        return false;

    tVector diff = p - t.points[0];
    double b0 = -(diff * t.edge0);
    double b1 = -(diff * t.edge1);
    double const zero = (double)0;
    double const one = (double)1;
    double t0 = t.a01 * b1 - t.a11 * b0;
    double t1 = t.a01 * b0 - t.a00 * b1;

    if (t0 + t1 <= t.det)
    {
        if (t0 < zero)
        {
            if (t1 < zero)  // region 4
            {
                if (b0 < zero)
                {
                    t1 = zero;
                    if (-b0 >= t.a00)  // V0
                    {
                        t0 = one;
                    }
                    else  // E01
                    {
                        t0 = -b0 / t.a00;
                    }
                }
                else
                {
                    t0 = zero;
                    if (b1 >= zero)  // V0
                    {
                        t1 = zero;
                    }
                    else if (-b1 >= t.a11)  // V2
                    {
                        t1 = one;
                    }
                    else  // E20
                    {
                        t1 = -b1 / t.a11;
                    }
                }
            }
            else  // region 3
            {
                t0 = zero;
                if (b1 >= zero)  // V0
                {
                    t1 = zero;
                }
                else if (-b1 >= t.a11)  // V2
                {
                    t1 = one;
                }
                else  // E20
                {
                    t1 = -b1 / t.a11;
                }
            }
        }
        else if (t1 < zero)  // region 5
        {
            t1 = zero;
            if (b0 >= zero)  // V0
            {
                t0 = zero;
            }
            else if (-b0 >= t.a00)  // V1
            {
                t0 = one;
            }
            else  // E01
            {
                t0 = -b0 / t.a00;
            }
        }
        else  // region 0, interior
        {
            double invDet = one / t.det;
            t0 *= invDet;
            t1 *= invDet;
        }
    }
    else
    {
        double tmp0, tmp1, numer, denom;

        if (t0 < zero)  // region 2
        {
            tmp0 = t.a01 + b0;
            tmp1 = t.a11 + b1;
            if (tmp1 > tmp0)
            {
                numer = tmp1 - tmp0;
                denom = t.a00 - 2.0*t.a01 + t.a11;
                if (numer >= denom)  // V1
                {
                    t0 = one;
                    t1 = zero;
                }
                else  // E12
                {
                    t0 = numer / denom;
                    t1 = one - t0;
                }
            }
            else
            {
                t0 = zero;
                if (tmp1 <= zero)  // V2
                {
                    t1 = one;
                }
                else if (b1 >= zero)  // V0
                {
                    t1 = zero;
                }
                else  // E20
                {
                    t1 = -b1 / t.a11;
                }
            }
        }
        else if (t1 < zero)  // region 6
        {
            tmp0 = t.a01 + b1;
            tmp1 = t.a00 + b0;
            if (tmp1 > tmp0)
            {
                numer = tmp1 - tmp0;
                denom = t.a00 - 2.0 * t.a01 + t.a11;
                if (numer >= denom)  // V2
                {
                    t1 = one;
                    t0 = zero;
                }
                else  // E12
                {
                    t1 = numer / denom;
                    t0 = one - t1;
                }
            }
            else
            {
                t1 = zero;
                if (tmp1 <= zero)  // V1
                {
                    t0 = one;
                }
                else if (b0 >= zero)  // V0
                {
                    t0 = zero;
                }
                else  // E01
                {
                    t0 = -b0 / t.a00;
                }
            }
        }
        else  // region 1
        {
            numer = t.a11 + b1 - t.a01 - b0;
            if (numer <= zero)  // V2
            {
                t0 = zero;
                t1 = one;
            }
            else
            {
                denom = t.a00 - 2.0 * t.a01 + t.a11;
                if (numer >= denom)  // V1
                {
                    t0 = one;
                    t1 = zero;
                }
                else  // 12
                {
                    t0 = numer / denom;
                    t1 = one - t0;
                }
            }
        }
    }

    result = t.points[0] + t.edge0 * t0 + t.edge1 * t1;
    return true;
}

/**
 * \brief   Searches for the nearest point.
 *
 * \param   p               The const tVector &amp; to process.
 * \param [in,out]  result  The result.
 *
 * \return  The found point.
 */
double ClosestPointCorrespondenceSolver::findClosestPoint(const tVector &p, tVector &result)
{
    double min_distance(std::numeric_limits<double>::max()), distance;
    tVector r;

    // For all triangles
    tSTriangleVec::const_iterator itt(m_triangles.begin()), ittEnd(m_triangles.end());
    for(; itt != ittEnd; ++itt)
    {
        findClosestPointToTriangle(p, *itt, r);

        // Compute distance
        distance = (p-r).length2();

        if(distance < min_distance)
        {
            // Store point and minimal distance
            min_distance = distance;
            result = r;
        }
    }

    // Minimal distance is squared
    return sqrtf(min_distance);
}

