///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#include "alg/CTransformInterpolator.h"

/**
 * \brief   Gets a transform.
 *
 * \param   t   The double to process.
 *
 * \return  The transform.
 */

CTransformInterpolator::tTransform CTransformInterpolator::getTransform(double t) const
{
    if(!m_bAllValid)
        return tTransform();

    // Get local parameter coordinates
    double local_t;
    size_t segment;
    toSegment(t, segment, local_t);

    // Get interpolations
    tVector translation(interpolateTranslation(segment, local_t));

    // Finalize matrix
    tTransform tm(tTransform::translate(translation));

    return tm;
}

/**
 * \brief   Transforms.
 *
 * \param   p   The const tVector &amp; to process.
 * \param   t   The double to process.
 *
 * \return  .
 */
CTransformInterpolator::tVector CTransformInterpolator::transform(const tVector &p, double t) const
{
    if(!m_bAllValid)
        return p;

    // Get local parameter coordinates
    double local_t;
    size_t segment;
    toSegment(t, segment, local_t);

    // Get interpolations
    tVector translation(interpolateTranslation(segment, local_t));
    tQuat   rotation(interpolateRotation(segment, local_t));
    tVector scale(interpolateScale(segment, local_t));

    tTransform tm(tTransform::translate(translation)*tTransform::rotate(rotation)*tTransform::scale(scale));
//    tTransform tm(tTransform::scale(scale)*tTransform::rotate(rotation)*tTransform::translate(translation));

    // Finalize transform
    return tm * p;
}

/**
 * \brief   Transforms.
 *
 * \param   p   The const tVector &amp; to process.
 * \param   t   The double to process.
 *
 * \return  .
 */
CTransformInterpolator::tVector CTransformInterpolator::iTransform(const tVector &p, double t) const
{
    if(!m_bAllValid)
        return p;

    // Get local parameter coordinates
    double local_t;
    size_t segment;
    toSegment(t, segment, local_t);

    // Get interpolations
    tVector translation(interpolateTranslation(segment, local_t));

    // Finalize transform
    return p - translation;
}

/**
 * \brief   Converts a t to the limits.
 *
 * \param   t   The double to process.
 *
 * \return  t as a double.
 */
double CTransformInterpolator::toLimits(double t) const
{
    return(std::max<double>(std::min<double>(t, m_tMax), m_tMin));
}

/**
 * \brief   Converts this object to a segment.
 *
 * \param   t               The double to process.
 * \param [in,out]  segment The segment.
 * \param [in,out]  local_t The local t.
 */
void CTransformInterpolator::toSegment(double t, size_t &segment, double &local_t) const
{
    // Limited version of t
    double limited_t(toLimits(t));
    
    limited_t -= m_tMin;

    segment = static_cast<size_t>(floor(limited_t / m_segmentLength));

    min_seg = std::min<int>(min_seg, segment);
    max_seg = std::max<int>(max_seg, segment);

    segment = std::min<size_t>(segment, m_numSegments - 1);
    local_t = fmod(limited_t, m_segmentLength) / m_segmentLength;

    /* DEBUG ONLY */

    min_t = std::min<double>(min_t, t);
    max_t = std::max<double>(max_t, t);
    min_lt = std::min<double>(min_lt, local_t);
    max_lt = std::max<double>(max_lt, local_t);
}

/**
 * \brief   Updates this object.
 */
void CTransformInterpolator::update()
{
    // Test inputs
    m_bAllValid = m_transforms.size() > 0 && m_tMax > m_tMin;

    if(!m_bAllValid)
        return;

    // Resize internal arrays
    size_t num_trans(m_transforms.size());
    m_numSegments = num_trans;
    m_segmentLength = (m_tMax - m_tMin) / static_cast<double>(num_trans);

    m_transKnots.resize(num_trans);
    m_rotKnots.resize(num_trans);
    m_scaleKnots.resize(num_trans);

    std::cout << "Transforms: " << std::endl;
    // For all transforms
    for(size_t i = 0; i < num_trans; ++i)
    {
        // Decompose transformation
        tVector tv, sv;
        tQuat rq;
        m_transforms[i].decompose(tv, rq, sv);
        tv = m_transforms[i].getTrans<double>();

//         std::cout << "T: " << tv[0] << ", " << tv[1] << ", " << tv[2] << std::endl;
//         std::cout << "R: " << rq[0] << ", " << rq[1] << ", " << rq[2] << ", " << rq[3] << std::endl;
//         std::cout << "S: " << sv[0] << ", " << sv[1] << ", " << sv[2] << std::endl;

        // Update helper computational structures
        m_transKnots[i] = tv;
        m_rotKnots[i] = rq;
        m_scaleKnots[i] = sv;
    }

    /* DEBUG ONLY */
    min_seg = std::numeric_limits<int>::max();
    max_seg = std::numeric_limits<int>::min();
    min_t = min_lt = std::numeric_limits<double>::max();
    max_t = max_lt = -std::numeric_limits<double>::max();
}

/**
 * \brief   Interpolate translation.
 *
 * \param   segment The segment.
 * \param   t       The double to process.
 *
 * \return  .
 */
CTransformInterpolator::tVector CTransformInterpolator::interpolateTranslation(size_t segment, double t) const
{
//    return m_transKnots[segment];

    // Get previous, current and next segment indexes
    int segPrev(std::max<int>(0, static_cast<int>(segment) - 1)), segCurr(segment), segNext(std::min<int>(m_numSegments-1, segment + 1));
     bool test(fabs(t) < 0.00001 || fabs(t-0.5) < 0.000001 || fabs(t-1.0) < 0.0000001);
    tVector p0, p1;
    double w0, w1;
    //
    if(t < 0.5)
    {
        // Use previous and current segment positions
        p0 = m_transKnots[segPrev];
        p1 = m_transKnots[segCurr];

        w0 = 0.5 - t;
        w1 = 0.5 + t;
    }
    else
    {
        // Use current and next segment positions
        p0 = m_transKnots[segCurr];
        p1 = m_transKnots[segNext];

        w0 = 1.5 - t;
        w1 = t - 0.5;
    }

    // Just linear interpolation
    return p0 * w0 + p1 * w1;
}

/**
 * \brief   Interpolate rotation.
 *
 * \param   segment The segment.
 * \param   t       The double to process.
 *
 * \return  .
 */
CTransformInterpolator::tQuat CTransformInterpolator::interpolateRotation(size_t segment, double t) const
{
//    return m_rotKnots[segment];

    // Get previous, current and next segment indexes
    int segPrev(std::max<int>(0, static_cast<int>(segment) - 1)), segCurr(segment), segNext(std::min<int>(m_numSegments-1, segment + 1));

    tQuat p0, p1;
    double weight(t);
    bool test(fabs(t) < 0.00001 || fabs(t-0.5) < 0.000001 || fabs(t-1.0) < 0.0000001);
    //
    if(t < 0.5)
    {
        // Use previous and current segment positions
        p0 = m_rotKnots[segPrev];
        p1 = m_rotKnots[segCurr];

        weight = 0.5 + t;
    }
    else
    {
        // Use current and next segment positions
        p0 = m_rotKnots[segCurr];
        p1 = m_rotKnots[segNext];

        weight = t - 0.5;
    }

    // Weight is currently in interval <0.0, 0.5)

    return tQuat::slerp(weight, p0, p1);
}

/**
 * \brief   Interpolate scale.
 *
 * \param   segment The segment.
 * \param   t       The double to process.
 *
 * \return  .
 */
CTransformInterpolator::tVector CTransformInterpolator::interpolateScale(size_t segment, double t) const
{
//    return m_scaleKnots[segment];

    // Get previous, current and next segment indexes
    int segPrev(std::max<int>(0, static_cast<int>(segment) - 1)), segCurr(segment), segNext(std::min<int>(m_numSegments-1, segment + 1));
    bool test(fabs(t) < 0.00001 || fabs(t-0.5) < 0.000001 || fabs(t-1.0) < 0.0000001);
    tVector p0, p1;
    double w0, w1;
    //
    if(t < 0.5)
    {
        // Use previous and current segment positions
        p0 = m_scaleKnots[segPrev];
        p1 = m_scaleKnots[segCurr];

        w0 = 0.5 - t;
        w1 = 0.5 + t;
    }
    else
    {
        // Use current and next segment positions
        p0 = m_scaleKnots[segCurr];
        p1 = m_scaleKnots[segNext];

        w0 = 1.5 - t;
        w1 = t - 0.5;
    }

    // Just linear interpolation
    return p0 * w0 + p1 * w1;
}
