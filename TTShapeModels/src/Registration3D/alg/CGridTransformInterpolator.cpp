///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#include "alg/CGridTransformInterpolator.h"

/**
 * \brief   Sets the limits.
 *
 * \param   min_x   The minimum x coordinate.
 * \param   min_y   The minimum y coordinate.
 * \param   min_z   The minimum z coordinate.
 * \param   max_x   The maximum x coordinate.
 * \param   max_y   The maximum y coordinate.
 * \param   max_z   The maximum z coordinate.
 */
void CGridTransformInterpolator::setLimits(double zmin, double zmax, const tLimitsVec &level_limits)
{
    m_minZ = zmin;
    m_maxZ = zmax;
    m_levelLimits = level_limits;

    update();
}

/**
 * \brief   Gets a transform.
 *
 * \param   t   The const tVector &amp; to process.
 *
 * \return  The transform.
 */

CGridTransformInterpolator::tTransform CGridTransformInterpolator::getTransform(const tVector &t) const
{
    if(!m_bAllValid)
        return tTransform();

    // Get local parameter coordinates
    SCoord local_coord;
    toSegment(t, local_coord);

    // Get interpolations
    tVector translation(interpolateTranslation(local_coord));

    // Finalize matrix
    tTransform tm(tTransform::translate(translation));

    return tm;
}

/**
 * \brief   Transforms point.
 *
 * \param   p   The const tVector &amp; to process.
 * \param   t   The const tVector &amp; to process.
 *
 * \return  .
 */
CGridTransformInterpolator::tVector CGridTransformInterpolator::transform(const tVector &p, const tVector &t) const
{
    if(!m_bAllValid)
        return p;

    // Get local parameter coordinates
    SCoord local_coord;
    toSegment(t, local_coord);

    // Get interpolations
    tVector translation(interpolateTranslation(local_coord));
    tQuat   rotation(interpolateRotation(local_coord));
    tVector scale(interpolateScale(local_coord));

    tTransform tm(tTransform::translate(translation)*tTransform::rotate(rotation)*tTransform::scale(scale));
    //    tTransform tm(tTransform::scale(scale)*tTransform::rotate(rotation)*tTransform::translate(translation));

    // Finalize transform
    return tm * p;

}

/**
 * \brief   Inverse transforms point.
 *
 * \param   p   The const tVector &amp; to process.
 * \param   t   The const tVector &amp; to process.
 *
 * \return  .
 */

CGridTransformInterpolator::tVector CGridTransformInterpolator::iTransform(const tVector &p, const tVector &t) const
{
    if(!m_bAllValid)
        return p;

    // Get local parameter coordinates
    SCoord local_coord;
    toSegment(t, local_coord);

    // Get interpolations
    tVector translation(interpolateTranslation(local_coord));
    tQuat   rotation(interpolateRotation(local_coord));
    tVector scale(interpolateScale(local_coord));

    tTransform tm(tTransform::translate(translation)*tTransform::rotate(rotation)*tTransform::scale(scale));
    //    tTransform tm(tTransform::scale(scale)*tTransform::rotate(rotation)*tTransform::translate(translation));

    // Finalize transform TODO: return inverted transform
    return tm * p;
}

/**
 * \brief   Converts this object to a segment coordinates.
 *
 * \param   t               The const tVector &amp; to process.
 * \param [in,out]  local   The local.
 */
void CGridTransformInterpolator::toSegment(const tVector &t, SCoord &local) const
{
    // Z-limited and zero starting version of t
    tVector limited_t(t);
    limited_t[2] = std::max(m_minZ, std::min(t[2], m_maxZ));
    limited_t[2] -= m_minZ;

    // Find z-axis segment
    local.segment[2] = std::max<size_t>(0, std::min<size_t>(m_numSegments[2] - 1, static_cast<size_t>(floor(limited_t[2] / m_zSegmentLength))));
    size_t level(local.segment[2]);

    // Get z-level sizes
    const SLevelLimit &ll(m_levelLimits[level]);

    // Move limited parameter to origin
    limited_t[0] -= ll.minX;
    limited_t[1] -= ll.minY;
    
    // Compute remaining segments
    local.segment[0] = static_cast<size_t>(floor(limited_t[0] / m_segmentLengths[level][0]));
    local.segment[1] = static_cast<size_t>(floor(limited_t[1] / m_segmentLengths[level][1]));

    local.segment[0] = std::max(0, std::min(m_numSegments[0] - 1, local.segment[0]));
    local.segment[1] = std::max(0, std::min(m_numSegments[1] - 1, local.segment[1]));

    // Compute in-segment position
    local.local_t[0] = fmod(limited_t[0], m_segmentLengths[level][0]) / m_segmentLengths[level][0];
    local.local_t[1] = fmod(limited_t[1], m_segmentLengths[level][1]) / m_segmentLengths[level][1];
    local.local_t[2] = fmod(limited_t[2], m_segmentLengths[level][2]) / m_segmentLengths[level][2];
}

/**
 * \brief   Updates this object.
 */
void CGridTransformInterpolator::update()
{
    // Test inputs
    m_bAllValid = testInputs();

    if(!m_bAllValid)
        return;

    m_numSegments[0] = m_transforms.size();
    m_numSegments[1] = m_transforms[0].size();
    m_numSegments[2] = m_transforms[0][0].size();

    // Compute segments lengths
    m_segmentLengths.resize(m_numSegments[2]);
    for(size_t s = 0; s < m_numSegments[2]; ++s)
    {
        m_segmentLengths[s] = tVector(
            (m_levelLimits[s].maxX - m_levelLimits[s].minX) / m_numSegments[0],
            (m_levelLimits[s].maxY - m_levelLimits[s].minY) / m_numSegments[1],
            (m_maxZ - m_minZ) / m_numSegments[2]
        );
    }


    // Resize knots grids
    resizeGrid(m_transKnots, m_numSegments);
    resizeGrid(m_rotKnots, m_numSegments);
    resizeGrid(m_scaleKnots, m_numSegments);

    // For all transforms
    for(size_t x = 0; x < m_numSegments[0]; ++x)
        for(size_t y = 0; y < m_numSegments[1]; ++y)
            for(size_t z = 0; z < m_numSegments[2]; ++z)
            {
                // Decompose transformation
                tVector tv, sv;
                tQuat rq;
                m_transforms[x][y][z].decompose(tv, rq, sv);
                tv = m_transforms[x][y][z].getTrans<double>();


                // Update helper computational structures
                m_transKnots[x][y][z] = tv;
                m_rotKnots[x][y][z] = rq;
                m_scaleKnots[x][y][z] = sv;
            }

}

/**
 * \brief   Interpolate translation.
 *
 * \param   segment The segment.
 * \param   t       The double to process.
 *
 * \return  .
 */
CGridTransformInterpolator::tVector CGridTransformInterpolator::interpolateTranslation(const SCoord &c) const
{
    tVector rv(0.0, 0.0, 0.0);

    for(size_t i = 0; i < 3; ++i)
    {
        // Get previous, current and next segment indexes
        int segPrev(std::max<int>(0, static_cast<int>(c.segment[i]) - 1)), segNext(std::min<int>(m_numSegments[i]-1, c.segment[i] + 1));
        tVector p0, p1;
        double w0, w1;

        // Compute local coordinates
        tIntVec cPrev(c.segment), cNext(c.segment), cCurr(c.segment);
        cPrev[i] = segPrev;
        cNext[i] = segNext;

        //
        if(c.local_t[i] < 0.5)
        {
            // Use previous and current segment positions
            p0 = getGridValue<tVectorGrid, tVector>(m_transKnots, cPrev);
            p1 = getGridValue<tVectorGrid, tVector>(m_transKnots, cCurr);

            w0 = 0.5 - c.local_t[i];
            w1 = 0.5 + c.local_t[i];
        }
        else
        {
            // Use current and next segment positions
            p0 = getGridValue<tVectorGrid, tVector>(m_transKnots, cCurr);
            p1 = getGridValue<tVectorGrid, tVector>(m_transKnots, cNext);

            w0 = 1.5 - c.local_t[i];
            w1 = c.local_t[i] - 0.5;
        }

        // Just linear interpolation
        rv += p0 * w0 + p1 * w1;
    }

    rv = rv * 1.0/3.0;
    return rv;
}

/**
 * \brief   Interpolate rotation.
 *
 * \param   segment The segment.
 * \param   t       The double to process.
 *
 * \return  .
 */
CGridTransformInterpolator::tQuat CGridTransformInterpolator::interpolateRotation(const SCoord &c) const
{
    tQuat rv[3];

    for(size_t i = 0; i < 3; ++i)
    {
        // Get previous, current and next segment indexes
        int segPrev(std::max<int>(0, static_cast<int>(c.segment[i]) - 1)), segNext(std::min<int>(m_numSegments[i]-1, c.segment[i] + 1));

        // Compute local coordinates
        tIntVec cPrev(c.segment), cNext(c.segment), cCurr(c.segment);
        cPrev[i] = segPrev;
        cNext[i] = segNext;

        tQuat p0, p1;
        double weight(c.local_t[i]);
        //
        if(c.local_t[i] < 0.5)
        {
            // Use previous and current segment positions
            p0 = getGridValue<tQuatGrid, tQuat>(m_rotKnots, cPrev);
            p1 = getGridValue<tQuatGrid, tQuat>(m_rotKnots, cCurr);

            weight = 0.5 + c.local_t[i];
        }
        else
        {
            // Use current and next segment positions
            p0 = getGridValue<tQuatGrid, tQuat>(m_rotKnots, cCurr);
            p1 = getGridValue<tQuatGrid, tQuat>(m_rotKnots, cNext);

            weight = c.local_t[i] - 0.5;
        }

        // Weight is currently in interval <0.0, 0.5)

        rv[i] = tQuat::slerp(weight, p0, p1);
    }

    // Finalize rotation
    return tQuat::slerp(0.5, tQuat::slerp(0.5, rv[0], rv[1]), rv[2]);
}

/**
 * \brief   Interpolate scale.
 *
 * \param   segment The segment.
 * \param   t       The double to process.
 *
 * \return  .
 */
CGridTransformInterpolator::tVector CGridTransformInterpolator::interpolateScale(const SCoord &c) const
{
    tVector rv(0.0, 0.0, 0.0);

    for(size_t i = 0; i < 3; ++i)
    {
        // Get previous, current and next segment indexes
        int segPrev(std::max<int>(0, static_cast<int>(c.segment[i]) - 1)), segNext(std::min<int>(m_numSegments[i]-1, c.segment[i] + 1));
        tVector p0, p1;
        double w0, w1;

        // Compute local coordinates
        tIntVec cPrev(c.segment), cNext(c.segment), cCurr(c.segment);
        cPrev[i] = segPrev;
        cNext[i] = segNext;
        //
        if(c.local_t[i] < 0.5)
        {
            // Use previous and current segment positions
            p0 = getGridValue<tVectorGrid, tVector>(m_scaleKnots, cPrev);
            p1 = getGridValue<tVectorGrid, tVector>(m_scaleKnots, cCurr);

            w0 = 0.5 - c.local_t[i];
            w1 = 0.5 + c.local_t[i];
        }
        else
        {
            // Use current and next segment positions
            p0 = getGridValue<tVectorGrid, tVector>(m_scaleKnots, cCurr);
            p1 = getGridValue<tVectorGrid, tVector>(m_scaleKnots, cNext);

            w0 = 1.5 - c.local_t[i];
            w1 = c.local_t[i] - 0.5;
        }

        // Just linear interpolation
        rv += p0 * w0 + p1 * w1;
    }

    rv = rv * 1.0/3.0;
    return rv;
}

/**
 * \brief   Tests inputs.
 *
 * \return  true if the test passes, false if the test fails.
 */
bool CGridTransformInterpolator::testInputs()
{
    if(m_maxZ <= m_minZ)
        return false;

    for(size_t i = 0; i < 3; ++i)
    {
        if(m_numSegments[i] == 0)
            return false;
    }

    // Test levels
    tLimitsVec::const_iterator it(m_levelLimits.begin()), itEnd(m_levelLimits.end());
    for(; it != itEnd; ++it)
        if(it->maxX <= it->minX || it->maxY <= it->minY)
            return false;
    

    return true;
}
