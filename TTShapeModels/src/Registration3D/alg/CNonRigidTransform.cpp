///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#include "alg/CNonRigidTransform.h"
#include "alg/icp.h"

/**
 * \brief   Solves transformation.
 *
 * \param [in,out]  target  Target for the transformation search.
 * \param [in,out]  source  Source - this model is being transformed.
 *
 * \return  true if it succeeds, false if it fails.
 */

bool CNonRigidSimple::solve(CMesh &target, CMesh &source)
{

    VPL_ASSERT(m_numLevels > 0);
    tTransformVec transforms(m_numLevels);

    // Compute splitting levels - z axis
    double z_min(0.0), z_increment(0.0);
    {
        // Compute bounding boxes of both parts
        CMesh::Point smin, smax;
        source.calc_bounding_box(smin, smax);
        CMesh::Point tmin, tmax;
        target.calc_bounding_box(tmin, tmax);

        // Get z values and compute height and floor
        z_min = std::min<double>(smin[2], tmin[2]);
        double height(std::max<double>(smax[2], tmax[2]) - z_min);

        // Something for rounding errors
        z_min -= 0.000001;
        height += 2.0*0.000001;

        z_increment = height / static_cast<double>(m_numLevels);
    }

    // Mask vectors
    tPointsUseMask mask_source(source.n_vertices()), mask_target(target.n_vertices());

    // Create ICP solver
    //  CPointPointICP icp_solver;
    //  CPointPlaneICP icp_solver;
    CPlanePlaneICP icp_solver;

    icp_solver.setComputeScale(true);
    icp_solver.setMinimalChangeThreshold(0.005);

    // For all levels
    for(size_t level = 0; level < m_numLevels; ++level)
    {
        // Compute level z limits
        double  level_min(z_min + static_cast<double>(level)*z_increment), 
            level_max(z_min + static_cast<double>(level + 1)*z_increment);

        long num_source_points(0), num_target_points(0);
        // Compute and set masks
        {
            // For all source points
            CMesh::ConstVertexIter it(source.vertices_begin()), itEnd(source.vertices_end());
            //             CMesh::VIter itp(copy_source->vertices_begin());
            size_t ind(0);
            for (; it != itEnd; ++it, ++ind) 
            {
                // Get point
                const CMesh::Point &p(source.point(it));

                // Test with limits
                mask_source[ind] = p[2] >= level_min && p[2] <= level_max;

                if(mask_source[ind])
                    ++num_source_points;
                //                 else
                //                     copy_source->delete_vertex(itp);

            }

            // For all target points
            it = target.vertices_begin(); itEnd = target.vertices_end();
            ind = 0;
            for (; it != itEnd; ++it, ++ind) 
            {
                // Get point
                const CMesh::Point &p(target.point(it));

                // Test with limits
                mask_target[ind] = p[2] >= (level_min -  z_increment) && p[2] <= (level_max + z_increment);

                if(mask_target[ind])
                    ++num_target_points;
            }

            icp_solver.setMasks(mask_source, mask_target);
        }

        // Find transform
        if(!icp_solver.apply(&target, &source, 100))
            return false;

        // Store transform
        transforms[level] = icp_solver.getTransform();

    }

    // Limits
    double min = z_min;
    double max = z_min + z_increment * static_cast<double>(m_numLevels);

    // Initialize interpolator
    m_interpolator.setParameters(min, max, transforms);
    
    return true;
}

/**
 * \brief   Applies the on mesh described by mesh.
 *
 * \param [in,out]  mesh    The mesh.
 *
 * \return  true if it succeeds, false if it fails.
 */
bool CNonRigidSimple::applyOnMesh(CMesh &mesh)
{
    if(!m_interpolator.isValid())
        return false;

    // For all mesh points
    CMesh::VIter it(mesh.vertices_begin()), itEnd(mesh.vertices_end());
    for(; it != itEnd; ++it)
    {
        // Get point
        CMesh::Point om_p(mesh.point(it));

        // Compute new position
        m_interpolator.transform(om_p[0], om_p[1], om_p[2], om_p[2]);

        // Move point
        mesh.set_point(it, om_p);
    }

    return true;
}

/**
 * \brief   Solves.
 *
 * \param [in,out]  target  Target for the.
 * \param [in,out]  source  Source for the.
 *
 * \return  true if it succeeds, false if it fails.
 */

/*
bool CNonRigidGrid::solve(CMesh &target, CMesh &source)
{
    VPL_ASSERT(m_numLevelsX > 0 && m_numLevelsY > 0 && m_numLevelsZ > 0);
    tXYZTransformGrid transforms;

    // Resize transforms grid
    transforms.resize(m_numLevelsX);
    for(size_t x = 0; x < m_numLevelsX; ++x)
    {
        transforms[x].resize(m_numLevelsY);
        for(size_t y = 0; y < m_numLevelsY; ++y)
        {
            transforms[x][y].resize(m_numLevelsZ);
        }
    }

    size_t num_transforms(m_numLevelsX * m_numLevelsY * m_numLevelsZ);

    // Compute bounding boxes of both parts
    CMesh::Point smin, smax;
    source.calc_bounding_box(smin, smax);
    CMesh::Point tmin, tmax;
    target.calc_bounding_box(tmin, tmax);

    CGridTransformInterpolator::tLimitsVec limits(m_numLevelsZ);

    // Compute splitting levels - z axis
    double z_min(0.0), z_increment(0.0);
    {
        // Get z values and compute height and floor
        z_min = std::min<double>(smin[2], tmin[2]);
        double size(std::max<double>(smax[2], tmax[2]) - z_min);

        // Something for rounding errors
        z_min -= 0.000001;
        size += 2.0*0.000001;

        z_increment = size / static_cast<double>(m_numLevelsZ);
    }

    

    // Mask vectors
    tPointsUseMask mask_source(source.n_vertices()), mask_target(target.n_vertices());

    // Create ICP solver
    //  CPointPointICP icp_solver;
    //  CPointPlaneICP icp_solver;
    CPlanePlaneICP icp_solver;

    icp_solver.setComputeScale(true);
    icp_solver.setMinimalChangeThreshold(0.005);

    std::cout << "Computing partial ICP transforms: " << std::endl;
    size_t counter(0);    

    for(size_t levelZ = 0; levelZ < m_numLevelsZ; ++levelZ)
    {
        // Compute level limits
        double  level_min_z(z_min + static_cast<double>(levelZ)*z_increment), 
            level_max_z(z_min + static_cast<double>(levelZ + 1)*z_increment);

        typedef std::vector<double> tDoubleVec;

        // Compute level "bounding box"
        double  x_min(std::numeric_limits<double>::max()),
                x_max(-std::numeric_limits<double>::max()),
                y_min(std::numeric_limits<double>::max()),
                y_max(-std::numeric_limits<double>::max());

        {
//             CMesh::ConstVertexIter it(source.vertices_begin()), itEnd(source.vertices_end());
//             for(; it != itEnd; ++it)
//             {
//                 const CMesh::Point &p(source.point(it));
// 
//                 if(p[2] > level_min_z  && p[2] < level_max_z)
//                 {
//                     x_min = std::min<double>(x_min, p[0]);
//                     x_max = std::max<double>(x_max, p[0]);
//                     y_min = std::min<double>(y_min, p[1]);
//                     y_max = std::max<double>(y_max, p[1]);
//                 }
//             }

            x_min = std::min<double>(smin[0], tmin[0]);
            x_max = std::min<double>(smax[0], tmax[0]);

            y_min = std::min<double>(smin[1], tmin[1]);
            y_max = std::min<double>(smax[1], tmax[1]);

            // Increase limits to prevent rounding errors
            x_min -= 0.0000001;
            x_max += 0.0000001;

            y_min -= 0.0000001;
            y_max += 0.0000001;

        }

        // Compute increments
        double  x_increment((x_max - x_min) / static_cast<double>(m_numLevelsX)), 
                y_increment((y_max - y_min) / static_cast<double>(m_numLevelsY));
        

        // Set limits of the current level to helper vector
        CGridTransformInterpolator::SLevelLimit &limit(limits[levelZ]);
        limit.maxX = x_max;
        limit.minX = x_min;
        limit.maxY = y_max;
        limit.minY = y_min;

        for(size_t levelX = 0; levelX < m_numLevelsX; ++levelX)
        {
            // Compute level limits
            double  level_min_x(x_min + static_cast<double>(levelX)*x_increment), 
                    level_max_x(x_min + static_cast<double>(levelX + 1)*x_increment);

            for(size_t levelY = 0; levelY < m_numLevelsY; ++levelY)
            {
                // Compute level limits
                double  level_min_y(y_min + static_cast<double>(levelY) * y_increment), 
                        level_max_y(y_min + static_cast<double>(levelY + 1)*y_increment);

                long num_source_points(0), num_target_points(0);
                // Compute and set masks
                {
                    

                    // For all source points
                    CMesh::ConstVertexIter it(source.vertices_begin()), itEnd(source.vertices_end());
                    size_t ind(0);
                    for (; it != itEnd; ++it, ++ind) 
                    {
                        // Get point
                        const CMesh::Point &p(source.point(it));

                        // Test with limits
                        mask_source[ind] =  p[0] >= level_min_x && p[0] <= level_max_x &&
                                            p[1] >= level_min_y && p[1] <= level_max_y &&
                                            p[2] >= level_min_z && p[2] <= level_max_z;

                        if(mask_source[ind])
                            ++num_source_points;
                        //                 else
                        //                     copy_source->delete_vertex(itp);

                    }

                    // For all target points
                    it = target.vertices_begin(); itEnd = target.vertices_end();
                    ind = 0;
                    for (; it != itEnd; ++it, ++ind) 
                    {
                        // Get point
                        const CMesh::Point &p(target.point(it));

                        // Test with limits
                        mask_target[ind] =  // p[0] >= (level_min_x -  x_increment) && p[0] <= (level_max_x + x_increment) &&
                                            // p[1] >= (level_min_y -  y_increment) && p[1] <= (level_max_y + y_increment) &&
                                            p[2] >= (level_min_z -  z_increment) && p[2] <= (level_max_z + z_increment);

                        if(mask_target[ind])
                            ++num_target_points;
                    }

                    icp_solver.setMasks(mask_source, mask_target);
                    
                }

                // Find transform
                if(num_source_points > 0 && num_target_points > 0)
                {
                    if(!icp_solver.apply(&target, &source, 100))
                        return false;

                    // Store transform
                    transforms[levelX][levelY][levelZ] = icp_solver.getTransform();
                }
                else
                {
                    // Store unit transform
                    transforms[levelX][levelY][levelZ] = tTransform();;
                }

                std::cout << static_cast<float>(++counter) / static_cast<float>(num_transforms) * 100.0f << "%" << std::endl;

            } // For Y
        } // For X
    } // For Z

    // Initialize interpolation object
    m_interpolator.setParameters(z_min, z_min + m_numLevelsZ * z_increment, limits, transforms);

    return true;
}

/**
 * \brief   Applies the on mesh described by mesh.
 *
 * \param [in,out]  mesh    The mesh.
 *
 * \return  true if it succeeds, false if it fails.
 */
/*
bool CNonRigidGrid::applyOnMesh(CMesh &mesh)
{
    if(!m_interpolator.isValid())
        return false;

    // For all mesh points
    CMesh::VIter it(mesh.vertices_begin()), itEnd(mesh.vertices_end());
    for(; it != itEnd; ++it)
    {
        // Get point
        CMesh::Point om_p(mesh.point(it));

        // Compute new position
        m_interpolator.transform(om_p[0], om_p[1], om_p[2]);

        // Move point
        mesh.set_point(it, om_p);
    }

    return true;
}
*/