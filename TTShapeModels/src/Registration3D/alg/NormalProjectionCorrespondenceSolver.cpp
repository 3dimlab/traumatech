///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#include "alg/NormalProjectionCorrespondenceSolver.h"
#include "kdtree/kdtree.h"
#include "alg/icp.h"
#include "base/smoothing.h"
#include <queue>

const size_t NUMBER_OF_ITERATIONS = 100;

/**
 * \brief   Finds correspondences between meshes.
 *
 * \param [in,out]  target  If non-null, target mesh (fixed mesh).
 * \param [in,out]  source  If non-null, source mesh.
 * \param   transformScaled The transform with scaling.
 * \param   transformSimple The transform scaling.
 * \param [out] result      if non-null, the resulting mesh.
 *
 * \return  true if it succeeds, false if it fails.
 */
CMesh * CNormalProjectionCorrespondenceSolver::apply(CMesh *target, CMesh *source, const tTransform &transformScaled, const tTransform &transformSimple)
{
    // Get number of result points - the same as source
    size_t num_points(source->n_vertices());
    size_t num_intersections(0);
    if(num_points == 0)
        return 0;

    // Get model simple bounding box
    CMesh::Point min, max;
    source->calc_bounding_box(min, max);
    float bdx(max[0] - min[0]), bdy(max[1] - min[1]), bdz(max[2] - min[2]);
    m_maxUsableDistance = sqrtf(bdx*bdx + bdy*bdy + bdz*bdz) / 20;

    // Compute back transform from scaled model to source and next to the unscaled aligned model
    tTransform transform_scaled_inverse(transformScaled.asEigen().inverse());
    tTransform transform_simple_inverse(transformSimple.asEigen().inverse());

    // Create result model as copy of target mesh - topology should be the same
    source->garbage_collection();
    CMesh *result(new CMesh(*source));

    // Scale result
    result->transform(transformScaled);

    // Add property to the result mesh
    OpenMesh::VPropHandleT<bool> flags;
    result->add_property(flags, "prop_found_point");

    // Disband collapsed edges
    finalize(*result, 0.00001);
    finalize(*target, 0.00001);

    // We need scaled source to have normals
    if(!target->has_vertex_normals())
        target->request_vertex_normals();
    if(!target->has_face_normals())
        target->request_face_normals();
    target->update_normals();
    
    // Create target mesh KD tree
    CKDTreeOMPtr kdTreeTarget(new CKDTreeOM(target));

    // Prepare changes vector
    tSChangeVec changes(num_points);

    // We will need target mesh normals
    if(!result->has_vertex_normals())
        result->request_vertex_normals();
    if(!result->has_face_normals())
        result->request_face_normals();
    result->update_normals();

    // Fill result handle -> id map
    {
        size_t id(0);
        for (CMesh::VertexIter rit = result->vertices_begin(); rit != result->vertices_end(); ++rit, ++id)
            m_resultHI.insert(std::pair<CMesh::VHandle, size_t>(rit.handle(), id));
    }

    // Weights 
    double weight(3.0/ static_cast<double>(NUMBER_OF_ITERATIONS));
//    double alpha(0.0);

    // Stats
    double  min_distance(std::numeric_limits<double>::max()), 
            max_distance(-std::numeric_limits<double>::max()), 
            avg_distance(0.0);

    // Smoothing operator
    CSmooth smoothing;

    // For all iteration steps
    for(int step = 0; step < NUMBER_OF_ITERATIONS; ++step)
    {

        // For all result mesh points
        size_t point_id(0);
        CMesh::VIter rit(result->vertices_begin());
        for (CMesh::VertexIter rit = result->vertices_begin(); rit != result->vertices_end(); ++rit, ++point_id)
        {
            // Get vertex point and normal
            tSIntersectionVec intersections;
            const CMesh::Point &om_p(result->point(rit)); 
            tVector p(om_p[0], om_p[1], om_p[2]);
            tVector n(computeNormal(rit.handle(), *result, 1));

            // Find corresponding point on the transformed source mesh (if possible)
            size_t index(0);
            bool intersection_found(false);

    //        intersection_found = findNearestPlaneIntersection(p, n, intersections);
    //*
    //        if(findIntersections(scaled_source, p, n, intersections))
            if(findNearIntersections(target, kdTreeTarget, p, n, intersections))
            {
                sortByDistance(p, intersections);

                // Try to find nearest intersection with correct normal orientation
                for(size_t i = 0; i < intersections.size(); ++i, ++index)
                {
                    // Get face normal
                    CMesh::Normal om_fn(target->normal(intersections[i].face));
                    tVector fn(om_fn[0], om_fn[1], om_fn[2]);
                    fn.normalize();

                    if(fn*n > 0.5)
                        break;
                }

                if(index < intersections.size())
                {
                    // Stats
                    double distance((p-intersections[index].point).length());
                    min_distance = std::min(distance, min_distance);
                    max_distance = std::max(distance, max_distance);
                    avg_distance += distance;
                    if(distance < m_maxUsableDistance)
                        intersection_found = true;
                }
            }
    //*/
            // Get current change access
            SChange &change(changes[point_id]);
            change.changed = false;
            change.handle = rit.handle();

            // If intersection found compute shift vector to the intersection
            if(intersection_found)
            {
                // Compute shift direction according to the normal vector
                tVector ipoint(intersections[index].point);
                tVector shift(ipoint - p);

                // Store change
                change.shift = shift;
                change.changed = shift.length2() > std::numeric_limits<double>::min();

                // Intersection found - increase counter
                ++num_intersections;
            }

            // Compute point in the center of current point neighbors
            tVector cpoint(averagePosition(rit.handle(), *result));

            if(change.changed)
            {
                // Finalize shift vector
                change.shift =  change.shift * weight; 
            }
        } // for all vertices

        // Estimate missing points
        estimateMissingPoints(changes, *result, weight);

        // Move all points and reset changes
        point_id = 0;
        for (CMesh::VertexIter rit = result->vertices_begin(); rit != result->vertices_end(); ++rit, ++point_id)
        {
            // Get point position
            CMesh::Point p(result->point(rit));

            // Get current change reference
            SChange &ch(changes[point_id]);

            // Store point to result mesh
            result->set_point(rit, p + CMesh::Point(ch.shift[0], ch.shift[1], ch.shift[2]));
        
//             // Reset change
//             ch.reset();
        }

//        std::cout << "NV: " << result->n_vertices() << std::endl;

        double max_smooth_weight(sqrtf(0.85));
        double smooth_weight(max_smooth_weight * (1.0 - step / NUMBER_OF_ITERATIONS));
        double normal_force_weight(0.5 * (NUMBER_OF_ITERATIONS - step) / NUMBER_OF_ITERATIONS);
        smooth_weight *= smooth_weight;
        if(smooth_weight > 0.0)
        {
            shake(*result, 0.01);
            smoothing.setType(CSmooth::SIMPLE_CENTERING);
            smoothing.setCenteringNormalForceWeight(normal_force_weight);
            smoothing.apply(*result, smooth_weight);
        }

        double laplace_weight(double(NUMBER_OF_ITERATIONS - step) / double(NUMBER_OF_ITERATIONS));
        laplace_weight *= laplace_weight;
        size_t num_steps( laplace_weight * 15.0 + 1);
        if(num_steps > 0)
        {
            smoothing.setType(CSmooth::JACOBI_LAPLACE);
            smoothing.setNumIterations(num_steps);
            smoothing.apply(*result, smooth_weight);
        }

        // Update normals information
        result->update_normals();

        std::cout << "Step: " << step << std::endl;

//         std::stringstream ss;
//         ss << "d:/result_" << step << ".stl";
//         result->save(ss.str());

        //         std::cout << "Num of points: " << num_points << ", num of intersections found: " << num_intersections << ", that is: " << 100.0f*float(num_intersections)/float(num_points) <<"%" << std::endl;
        //         std::cout << "Minimal distance: " << min_distance << ", maximal distance: " << max_distance << ", average: " << avg_distance << std::endl;

        num_intersections = num_points = 0.0;
    } // for all steps


    result->transform(transform_scaled_inverse);

 //   finalize(*result, 0.1);
//     if(num_intersections > 0)
//         avg_distance /= float(num_intersections);

    // All done
    return result;
}


/**
 * \brief   Searches for the first near intersections.
 *
 * \param [in,out]  mesh            If non-null, the mesh.
 * \param   point                   The point.
 * \param   direction               The direction.
 * \param [in,out]  intersections   The intersections.
 *
 * \return  true if it succeeds, false if it fails.
 */
bool CNormalProjectionCorrespondenceSolver::findNearIntersections(CMesh *mesh, CKDTreeOMPtr kdTreeResult, const tVector &point, const tVector &direction, tSIntersectionVec &intersections)
{
    // Normalized direction vector
    tVector ndirection(direction);
    ndirection.normalize();

    // Output variables
    tTriangle triangle;
    SIntersection intersection;
    bool parallel(false);

    // Already tested faces handles
    std::set<CMesh::FHandle> tested_faces;

    // Find nearest points
    CKDTreeOM::SIndexDistancePairs near_points(100);
    if(!kdTreeResult->getClosestPoints(point, near_points))
        return false;

    size_t starting_size(intersections.size());

    // For all near points
    CKDTreeOM::tIndexVec::const_iterator iti(near_points.indexes.begin()), itiEnd(near_points.indexes.end());
    for(; iti != itiEnd; ++iti)
    {
        // Get point handle
        CMesh::VHandle ph(kdTreeResult->getPointVH(*iti));

        // For all vertex faces
        CMesh::VFIter vfit(mesh->vf_begin(ph)), vfitEnd(mesh->vf_end(ph));
        for(; vfit != vfitEnd; ++vfit)
        {
            // Test if face is already tested
            if(tested_faces.find(vfit.handle()) != tested_faces.end())
                continue;

            // Set face as tested
            tested_faces.insert(vfit.handle());

            // Get face points
            size_t count(0);
            for (CMesh::FaceVertexIter fvit = mesh->fv_begin(vfit); fvit != mesh->fv_end(vfit) && count < 3; ++fvit, ++count)
            {
                const CMesh::Point &p(mesh->point(fvit.handle()));
                triangle[count] = tVector(p[0], p[1], p[2]);
            }

            if(count < 2)
                continue;

            // Try to find intersection of ray and triangle
            if(!rayTriangleIntersection(point, ndirection, triangle, intersection.point ))//, intersection.barycentric, parallel))
                continue;

            intersection.face = vfit.handle();
            intersections.push_back(intersection);
        }

    }

    return starting_size < intersections.size();
}


/**
 * \brief   Ray triangle intersection.
 *
 * \param   rayOrigin           The ray origin.
 * \param   rayDirection        The ray direction.
 * \param   triangle            The triangle.
 * \param   result              The result.
 * \param   resultBarymetric    The result barymetric.
 *
 * \return  true if it succeeds, false if it fails.
 */
bool CNormalProjectionCorrespondenceSolver::rayTriangleIntersection(const tVector &rayOrigin, const tVector &rayDirection, const tTriangle &triangle, tVector &result, tVector &resultBarymetric, bool parallel)
{
    tVector e1(triangle[1] - triangle[0])
          , e2(triangle[2] - triangle[0]);  //Edge1, Edge2

    tVector P(rayDirection ^ e2);
    double eps(std::numeric_limits<double>::min());

    //if determinant is near zero, ray lies in plane of triangle
    double det(e1 * P);

    //NOT CULLING
    if(det > -eps && det < eps)
    {
        parallel = true;
        return false;
    }

    parallel = false;

    double inv_det(1.f / det);

    //calculate distance from V1 to ray origin
    tVector T(rayOrigin - triangle[0]);

    //Calculate u parameter and test bound
    double u((T * P) * inv_det);

    //Prepare to test v parameter
    tVector Q(T ^ e1);

    //Calculate V parameter and test bound
    double v((rayDirection * Q) * inv_det);

    //The intersection lies outside of the triangle
    bool outside_triangle(u < 0. || u > 1. || v < 0. || v  > 1. || u + v > 1.0);

//    double t((e2 * Q) * inv_det);

    // Ok, all is computed - finalize coordinates
    resultBarymetric = tVector(1.0-u-v, u, v);
    result = cartesian(resultBarymetric, triangle);//triangle[0] + e1 * u + e2 * v;

    return !outside_triangle;
}

bool CNormalProjectionCorrespondenceSolver::rayTriangleIntersection(const tVector &rayOrigin, const tVector &rayDirection, const tTriangle &triangle, tVector &result)
{
    tVector edge1 = triangle[1] - triangle[0];
    tVector edge2 = triangle[2] - triangle[0];

    // Find the cross product of edge2 and the ray direction
    tVector s1 = (rayDirection ^ edge2);

    // Find the divisor, if its zero, return false as the triangle is
    // degenerated
    double divisor = (s1 * edge1);
    if (fabs(divisor) < std::numeric_limits<double>::min())
    return false; 

    // A inverted divisor, as multipling is faster then division
    float invDivisor = 1.0 / divisor;


    // Calculate the first barycentic coordinate. Barycentic coordinates
    // are between 0.0 and 1.0
    tVector distance = rayOrigin - triangle[0];
    float barycCoord_1 = (distance * s1) * invDivisor;
    if (barycCoord_1 < 0.0 || barycCoord_1 > 1.0)
        return false;

    // Calculate the second barycentic coordinate
    tVector s2 = (distance ^ edge1);
    float barycCoord_2 = (rayDirection * s2) * invDivisor;
    if (barycCoord_2 < 0.0 || (barycCoord_1 + barycCoord_2) > 1.0)
        return false;

    // After doing the barycentic coordinate test we know if the ray hits or 
    // not. If we got this far the ray hits.
    // Calculate the distance to the intersection point
    float intersectionDistance = (edge2 * s2) * invDivisor;

    result = rayOrigin + rayDirection * intersectionDistance;

    return true;      
}
/**
 * \brief   Sort by distance.
 *
 * \param   point           The point.
 * \param [in,out]  points  The points.
 */
void CNormalProjectionCorrespondenceSolver::sortByDistance(const tVector &point, tSIntersectionVec &points)
{
    tVector origin(point);

    std::sort(points.begin(), points.end(), 
        [origin](const SIntersection &l, const SIntersection &r)
        {
            return (l.point-origin).length2() < (r.point-origin).length2();
        }
    );
}


/**
 * \brief   Estimate missing points 2 - by normal interpolation.
 *
 * \param [in,out]  changes The changes.
 * \param [in,out]  mesh    The mesh.
 *
 * \return  true if it succeeds, false if it fails.
 */

bool CNormalProjectionCorrespondenceSolver::estimateMissingPoints(tSChangeVec &changes, CMesh &mesh, double weight)
{
    // Some typedefs
    typedef std::set<CMesh::VHandle> tHandleSet;
    typedef std::queue<CMesh::VHandle> tHandleQueue;

    // How many points should be found?
    size_t num_points(10);

    typedef std::pair<tVector, double> tShiftWeightPair;
    typedef std::vector<tShiftWeightPair> tSWVec;

    tSWVec shifts(num_points);

    // For all points
    size_t point_id(0);
    size_t num_changed(0);
    tSChangeVec::iterator it(changes.begin()), itEnd(changes.end());
    for(; it != itEnd; ++it, ++point_id)
    {
        // Already done point
        if(it->changed)
            continue;

        ++num_changed;

        const CMesh::Point &om_tp(mesh.point(it->handle));

        // Already tested points
        tHandleSet tested_points;
        // Points to test
        tHandleQueue points_to_test;

        // Store starting point
        points_to_test.push(it->handle);

        // Number of currently found points
        size_t num_found(0);

        double sum_weights(0.0);
        while(!points_to_test.empty() && num_found < num_points)
        {
            // Get handle
            CMesh::VHandle phandle(points_to_test.front());
            points_to_test.pop();

            // Is point already tested?
            if(tested_points.find(phandle) != tested_points.end())
                continue;

            // Store point as tested
            tested_points.insert(phandle);

            // Get point id
            size_t pid(m_resultHI[phandle]);
            
            // Test if point is suitable
            if(changes[pid].changed)
            {
                // Get point 
                const CMesh::Point &om_p(mesh.point(phandle));

                // Compute weight of move - distance from the starting point
                double w((om_tp - om_p).length());
                sum_weights += w;

                // Store shift to vector
                shifts[num_found].second = w;
                shifts[num_found].first = changes[pid].shift;

                // Increase points counter
                ++num_found;
            }

            // Find all not tested neighbors 
            CMesh::VertexVertexIter it(mesh.vv_iter(phandle));
            for(;it; ++it)
                if(tested_points.find(it.handle()) == tested_points.end())
                    points_to_test.push(it.handle());

        }

        // Final shift vector
        tVector shift(0.0, 0.0, 0.0);

        // Any suitable neighbors found?
        if(num_found > 0 && sum_weights > 0.0)
        {
            double denom(1.0 / sum_weights);

            // Compute final shift vector
            for(size_t i = 0; i < num_found; ++i)
                shift = shift + shifts[i].first * shifts[i].second * denom * 0.5;
        }

        // Store shift vector
        it->shift = shift;
    }

    std::cout << "Points to estimate: " << num_changed << std::endl;

    return true;
}


/**
 * \brief   Compute barycentric coordinates.
 *
 * \param   point   The input cartesian point.
 * \param   t0      The triangle point 0.
 * \param   t1      The triangle point 1.
 * \param   t2      The triangle point 2.
 *
 * \return  Barycentric coordinates of the input point.
 */
CNormalProjectionCorrespondenceSolver::tVector CNormalProjectionCorrespondenceSolver::barycentric(const tVector &point, const tTriangle &t)
{
    tVector n((t[1] - t[0]) ^ (t[2] - t[0]));
    tVector na((t[2] - t[1]) ^ (point - t[1]));
    tVector nb((t[0] - t[2]) ^ (point - t[2]));
    tVector nc((t[1] - t[0]) ^ (point - t[0]));
    double denom(1.0/n.length2());
    double u((n*na)*denom);
    double v((n*nb)*denom);
    double w((n*nc)*denom);

    return tVector(u,v,w);
}

/**
 * \brief   Compute cartesian coordinates of the barycentric point.
 *
 * \param   point   The input barycentric point.
 * \param   t0      The triangle point 0.
 * \param   t1      The triangle point 1.
 * \param   t2      The triangle point 2.
 *
 * \return  Cartesian coordinates of the input point.
 */

CNormalProjectionCorrespondenceSolver::tVector CNormalProjectionCorrespondenceSolver::cartesian(const tVector &point, const tTriangle &t)
{
    return tVector(t[0] * point(0) + t[1] * point(1) + t[2] * point(2));
}

/**
 * \brief   Calculates the normal of point from neighbor points.
 *
 * \param   point_handle    Handle of the point.
 * \param [in,out]  mesh    The mesh.
 *
 * \return  The calculated normal.
 */
CNormalProjectionCorrespondenceSolver::tVector CNormalProjectionCorrespondenceSolver::computeNormal(CMesh::VHandle point_handle, CMesh &mesh, size_t ring /*= 1*/)
{
    typedef std::set<CMesh::VHandle> tUsedVH;
    typedef std::queue<CMesh::VHandle> tHandles;
    tUsedVH used_vertices;
    tHandles vertices_this_ring, vertices_next_ring; 
    
    vertices_next_ring.push(point_handle);
    used_vertices.insert(point_handle);

    CMesh::Normal n;
    tVector normal(0.0, 0.0, 0.0);

    for(size_t r = 0; r <= ring; ++r)
    {
        // Use next ring vertices as current
        std::swap(vertices_this_ring, vertices_next_ring);

        // Clear next ring vertices
        tHandles().swap(vertices_next_ring);

        while (!vertices_this_ring.empty())
        {
            // Get currently tested point
            CMesh::VHandle ph(vertices_this_ring.front());
            vertices_this_ring.pop();

            // Set it as used
            used_vertices.insert(ph);

            // Get its normal
            n = mesh.normal(ph);
            normal += tVector(n[0], n[1], n[2]);

            // For all its neighbors
            CMesh::VVIter it(mesh.vv_iter(ph));
            for(;it; ++it)
            {
                // Test if not already tested
                if(used_vertices.find(ph) != used_vertices.end())
                    continue;

                // Test them next round
                vertices_next_ring.push(it.handle());
            }
        }
    }

    normal.normalize();
    return normal;
}

/**
 * \brief   Average position of point (in the center of neighbor points.
 *
 * \param   vh              The vh.
 * \param [in,out]  mesh    The mesh.
 *
 * \return  .
 */
CNormalProjectionCorrespondenceSolver::tVector CNormalProjectionCorrespondenceSolver::averagePosition(CMesh::VertexHandle vh, CMesh &mesh)
{
    CMesh::Point shift1(0.0, 0.0, 0.0);
    CMesh::VVIter vv_it(mesh.vv_iter(vh));

    // Circulate around given point
    size_t counter1(0);
    for(; vv_it; ++vv_it, ++counter1)
    {
        // Get point and add it to the shift
        shift1 += mesh.point(vv_it);
    }

    if(counter1 > 0)
        shift1 /= float(counter1);
    else
        shift1 = mesh.point(vh);

    return tVector(shift1[0], shift1[1], shift1[2]);
}

/**
 * \brief   Smooths.
 *
 * \param   changes The changes.
 */
void CNormalProjectionCorrespondenceSolver::smooth(const tSChangeVec &changes, CMesh &mesh, double smoothing_weight)
{
    CMeshPtr mcopy(new CMesh(mesh));

    CMesh::VertexIter mit(mesh.vertices_begin()), cit(mcopy->vertices_begin()), mitEnd(mesh.vertices_end());
    size_t point_id(0);
    for(; mit != mitEnd; ++mit, ++cit, ++point_id)
    {
        // Already done point
//         if(changes[point_id].changed)
//             continue;

        const CMesh::Point &cp(mesh.point(mit));
        CMesh::Point p(0.0, 0.0, 0.0);
        double counter(0.0);
        // Circulate around given point and compute center point
        CMesh::VertexVertexIter vv_it(mcopy->vv_iter(cit.handle()));
        for(; vv_it; ++vv_it)
        {
            p += mcopy->point(vv_it);
            counter += 1.0;
        }

        // Move point
        if(counter > 0.9)
        {
            p = p / counter;
            p = p - cp;
            p = p * smoothing_weight;
            mesh.set_point(mit.handle(), cp + p);
        }
    }
}

/**
 * \brief   Finalizes.
 *
 * \param [in,out]  mesh    The mesh.
 */
void CNormalProjectionCorrespondenceSolver::finalize(CMesh &mesh, double max_move)
{
    shake(mesh, max_move);
    return;

    size_t num_steps(0);
    bool unfinished(true);

    // Random numbers generator
    CRandDouble rg(-max_move*0.5, max_move*0.5);

    size_t num_collapsed(0);
    // Iterate
    while(unfinished && num_steps < 100)
    {
        size_t currently_collapsed(0);

        CMesh::VertexIter it(mesh.vertices_begin()), itEnd(mesh.vertices_end());
        for(; it != itEnd; ++it )
        {
            ++num_steps;
            unfinished = false;

            bool too_short_edge(false);
            // Test vertex edges lengths
            CMesh::VEIter ve_it(mesh.ve_iter(it));
            for(; ve_it; ++ve_it)
            {
                // Test edge length
                if(mesh.calc_edge_length(ve_it) < 0.0000001)
                {
                    too_short_edge = true;
                    break;
                }
            }

            // Vertex ok?
            if(!too_short_edge)
                continue;

            // Set flag
            unfinished = true;
            ++currently_collapsed;

            // Move vertex randomly
            CMesh::Point shift(rg(), rg(), rg());
            CMesh::Point p(mesh.point(it));
            mesh.set_point(it, p + shift);
        }

        num_collapsed = std::max(num_collapsed, currently_collapsed);
    }

    if(num_collapsed > 0)
        std::cout << "Num of collapsed vertices: " << num_collapsed << std::endl;
}

/**
 * \brief   Randomly move all vertices to prevent collapsing.
 *
 * \param [in,out]  mesh    The mesh.
 * \param   max_move        The maximum move.
 */
void CNormalProjectionCorrespondenceSolver::shake(CMesh &mesh, double max_move)
{
    // Random numbers generator
    CRandDouble rg(-max_move*0.5, max_move*0.5);

    CMesh::VertexIter it(mesh.vertices_begin()), itEnd(mesh.vertices_end());
    for(; it != itEnd; ++it )
    {
        // Move vertex randomly
        CMesh::Point shift(rg(), rg(), rg());
        CMesh::Point p(mesh.point(it));
        mesh.set_point(it, p + shift);
    }
}
