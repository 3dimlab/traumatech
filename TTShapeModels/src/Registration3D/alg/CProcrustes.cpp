///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#include "alg/CProcrustes.h"

/**
 * \brief   Default constructor.
 */
CProcrustes3D::CProcrustes3D()
{

}

/**
 * \brief   Resets this object.
 */
void CProcrustes3D::reset()
{
    tVectorVecVec::iterator it(m_sources.begin()), itEnd(m_sources.end());
    for(; it != itEnd; ++it)
        delete *it;

    m_sources.clear();
}

/**
 * \brief   Adds a source mesh.
 *
 * \param [in,out]  mesh    The mesh.
 *
 * \return  Internal identification number of mesh.
 */
size_t CProcrustes3D::addSource(CMesh &mesh)
{
    size_t id(m_sources.size()); 
    
    tVectorVec *points(new tVectorVec); 
    convert(mesh, *points); 
    m_sources.push_back(points);

    return id;
}

/**
 * \brief   Applies the given num_steps.
 *
 * \param   num_steps   Number of steps.
 */
bool CProcrustes3D::apply(size_t num_steps /*= 5*/)
{
    // Test inputs
    if(!testInputs())
        return false;

    size_t num_models(m_sources.size());

    // Initialize matrices
    m_transforms.resize(num_models);
    tTransformVec::iterator itt(m_transforms.begin()), ittEnd(m_transforms.end());
    for(; itt != ittEnd; ++itt)
        itt->unit();

    // Take first model as first target 
    m_target = *m_sources[0];

    // For all iterations
    for(size_t iteration = 0; iteration < num_steps; ++iteration)
    {
        // Compute transformation for all models
        tVectorVecVec::const_iterator itp(m_sources.begin()), itpEnd(m_sources.end());
        tTransformVec::iterator itm(m_transforms.begin());
        for(; itp != itpEnd; ++itp)
        {
            // estimate transformation
            *itm = estimate(**itp, m_target, false);

            // Apply on model
            const tTransform &t(*itm);
            tVectorVec::iterator it((**itp).begin()), itEnd((**itp).end());
            for(; it != itEnd; ++it)
                *it = t * *it;
        }

        // For all corresponding points
        size_t num_points(m_target.size());
        double divider(1.0 / static_cast<double>(num_models));
        for(size_t pid = 0; pid < num_points; ++pid)
        {
            tVector p(.0, .0, .0);

            // For all stored models
            tVectorVecVec::const_iterator itp(m_sources.begin()), itpEnd(m_sources.end());
            for(; itp != itpEnd; ++itp)
            {
                // Compute sum of points
                p += (**itp)[pid];
            }

            p = p * divider;

            // Move target point to the new position
            m_target[pid] = p;
        }
    }


    // All done
    return true;
}

/**
 * \brief   Gets a mesh - converts stored points to the mesh.
 *
 * \param [in,out]  mesh    The mesh.
 * \param   id              The identifier.
 *
 * \return  true if it succeeds, false if it fails.
 */
bool CProcrustes3D::getMesh(CMesh &mesh, size_t id) const
{
    // Get points reference
    if(id >= m_sources.size())
        return false;

    const tVectorVec &points(*m_sources[id]);

    if(points.size() != mesh.n_vertices())
        return false;

    CMesh::VIter vit(mesh.vertices_begin()), vitEnd(mesh.vertices_end());
    tVectorVec::const_iterator it(points.begin());

    const tTransform &t(m_transforms[id]);

    // For all mesh points
    for(; vit != vitEnd; ++vit, ++it)
    {
        tVector v(*it);

        // Set mesh point position
        mesh.set_point(vit, CMesh::Point(v[0], v[1], v[2]));
    }

    return true;
}

/**
 * \brief   Gets a lastly used target mesh.
 *
 * \param [in,out]  mesh    The mesh.
 *
 * \return  true if it succeeds, false if it fails.
 */

bool CProcrustes3D::getTargetMesh(CMesh &mesh) const
{
    if(m_target.size() != mesh.n_vertices())
        return false;

    CMesh::VIter vit(mesh.vertices_begin()), vitEnd(mesh.vertices_end());
    tVectorVec::const_iterator it(m_target.begin());

    // For all mesh points
    for(; vit != vitEnd; ++vit, ++it)
    {
        tVector v(*it);
        // Set mesh point position
        mesh.set_point(vit, CMesh::Point(v[0], v[1], v[2]));
    }

    return true;
}

/**
 * \brief   Determines the mean of the given vector of points. Based on Knuth's algorithm
 *
 * \param   points  The points.
 *
 * \return  The mean value.
 */
CProcrustes3D::tVector CProcrustes3D::mean(const tVectorVec &points)
{
    assert(points.size() > 0);

    if(points.size() == 1)
        return points[0];

    // Counter and starting values of algorithm
    double n(0.0);
    tVector mean(.0, .0, .0);

    // Compute mean value.
    tVectorVec::const_iterator it(points.begin()), itEnd(points.end());
    for(; it != itEnd; ++it)
    {
        n += 1.0;
        tVector delta( *it - mean);
        mean = mean + delta/n;
    }

    return mean;
}

/**
 * \brief   Estimates transform of source to the target.
 *
 * \param   source  Source for the.
 * \param   target  Target for the.
 *
 * \return  .
 */
CProcrustes3D::tTransform CProcrustes3D::estimate(const tVectorVec &source, const tVectorVec &target, bool solve_scaling)
{
    assert(source.size() == target.size());
    size_t num_points(source.size());

    // Convert points to the Eigen format
    typedef Eigen::Matrix<double, 3, Eigen::Dynamic> EPointMatrix;
    EPointMatrix points_src (3, num_points);
    EPointMatrix points_tgt (3, num_points);

    for(size_t lc = 0; lc < num_points; ++lc)
    {
        points_src(0, lc) = source[lc](0);
        points_src(1, lc) = source[lc](1);
        points_src(2, lc) = source[lc](2);

        points_tgt(0, lc) = target[lc](0);
        points_tgt(1, lc) = target[lc](1);
        points_tgt(2, lc) = target[lc](2);

    }

    // Some typedefs
    typedef vpl::math::CDMatrix3x3::tStorage tDMatrix3x3;   // Used computational matrix - 3x3 doubles
    typedef vpl::math::CDVector3::tStorage  tDVector3;      // Vector type - three doubles
    typedef Eigen::internal::plain_matrix_type_row_major<EPointMatrix>::type RowMajorMatrixType;

    // Means are divided by number of correspondences
    const double div_num_correspondences(1.0 / static_cast<double>(num_points));

    // computation of means - sum of vectors divided by number of points
    const tDVector3 mean_src(points_src.rowwise().sum() * div_num_correspondences);
    const tDVector3 mean_tgt(points_tgt.rowwise().sum() * div_num_correspondences);

    // demeaning: point = point - mean point
    const RowMajorMatrixType demeaned_src(points_src.colwise () - mean_src);
    const RowMajorMatrixType demeaned_tgt(points_tgt.colwise () - mean_tgt);

    // Variance computation: Eq. (36)-(37)
    const double variance_src(demeaned_src.rowwise ().squaredNorm ().sum () * div_num_correspondences);

    // Covariance matrix computation: Eq. (38)
    const tDMatrix3x3 sigma(div_num_correspondences * demeaned_tgt * demeaned_src.transpose());

    // Singular values decomposition object
    Eigen::JacobiSVD<tDMatrix3x3> svd (sigma, Eigen::ComputeFullU | Eigen::ComputeFullV);

    // Estimated transform matrix - identity in the beginning
    tTransform::tStorage CTM(tTransform::tStorage::Identity(4, 4));

    // Prepare scale vector: Eq. (39)
    tDVector3 S(tDVector3::Ones(3));
    if (sigma.determinant () < 0) 
        S(2) = -1;

    // Determine optimal parameters
    const tDVector3& d = svd.singularValues();

    size_t rank = 0; 
    // Find rank much bigger than the others
    for (size_t i = 0; i < 3; ++i) 
        if (!Eigen::internal::isMuchSmallerThan (d.coeff (i), d.coeff (0))) 
            ++rank;

    // Eq. (44, 45)
    if (rank == 2) 
    {
        if (svd.matrixU().determinant() * svd.matrixV().determinant() > 0) 
            CTM.block(0, 0, 3, 3).noalias() = svd.matrixU() * svd.matrixV().transpose ();
        else 
        {
            const double s(S(2)); 
            S(2) = -1;
            CTM.block(0, 0, 3, 3).noalias() = svd.matrixU() * S.asDiagonal() * svd.matrixV().transpose();
            S(2) = s;
        }
    } 
    else 
    {
        // Eq. 40 - 42
        CTM.block(0, 0, 3, 3).noalias() = svd.matrixU() * S.asDiagonal() * svd.matrixV().transpose ();
    }

    // If scaling should be solved
    if (solve_scaling)
    {
        // Eq. (42)
        const double c(1.0 / variance_src * svd.singularValues().dot(S));

        // Eq. (41)
        CTM.col(3).head(3) = mean_tgt;
        CTM.col(3).head(3).noalias () -= c * CTM.topLeftCorner(3, 3) * mean_src;
        CTM.block(0, 0, 3, 3) *= c;
    }
    else
    {
        CTM.col(3).head(3) = mean_tgt;
        CTM.col(3).head(3).noalias() -= CTM.topLeftCorner(3, 3) * mean_src;
    }

    return tTransform(CTM);
}

/**
 * \brief   Converts input mesh to the internal representation.
 *
 * \param [in,out]  mesh    The mesh.
 * \param [in,out]  points  The points.
 */
void CProcrustes3D::convert(CMesh &mesh, tVectorVec &points)
{
    // Resize output
    points.resize(mesh.n_vertices());

    // Copy vertices
    CMesh::VIter vit(mesh.vertices_begin()), vitEnd(mesh.vertices_end());
    tVectorVec::iterator it(points.begin());

    for(; vit != vitEnd; ++vit, ++it)
    {
        // Get mesh point
        const CMesh::Point &p(mesh.point(vit));

        // store to vector
        *it = tVector(p[0], p[1], p[2]);
    }

    // All done...
}

/**
 * \brief   Tests inputs.
 *
 * \return  true if the test passes, false if the test fails.
 */
bool CProcrustes3D::testInputs()
{
    if(m_sources.size() < 2)
        return false;

    size_t num_points(m_sources[0]->size());
    tVectorVecVec::const_iterator it(m_sources.begin()), itEnd(m_sources.end());
    for(; it != itEnd; ++it)
        if((**it).size() != num_points)
            return false;

    return true;
}
