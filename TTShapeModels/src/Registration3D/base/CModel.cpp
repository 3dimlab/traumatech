///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#include <base/CModel.h>
#include <VPL/Math/Vector.h>
#include <VPL/Math/MatrixFunctions.h>
#include <VPL/Base/Logging.h>

/**
 * \brief   Calculates the bounding box.
 *
 * \param [in,out]  min The minimum.
 * \param [in,out]  max The maximum.
 *
 * \return  true if it succeeds, false if it fails.
 */

bool CMesh::calc_bounding_box(Point &min, Point &max)
{
    bool first = true;

    for (CMesh::VertexIter vit = this->vertices_begin(); vit != this->vertices_end(); ++vit)
    {
        if(this->status(vit).deleted())
            continue;

        CMesh::Point point = this->point(vit.handle());

        if (first)
        {
            first = false;
            min = max = point;
        }
        else
        {
            min[0] = vpl::math::getMin<float>(min[0], point[0]);
            min[1] = vpl::math::getMin<float>(min[1], point[1]);
            min[2] = vpl::math::getMin<float>(min[2], point[2]);

            max[0] = vpl::math::getMax<float>(max[0], point[0]);
            max[1] = vpl::math::getMax<float>(max[1], point[1]);
            max[2] = vpl::math::getMax<float>(max[2], point[2]);
        }
    }

    return (!first);
}

/**
 * \brief   Calculates the oriented bounding box.
 *
 * \param [in,out]  tm      The time.
 * \param [in,out]  extent  The extent.
 *
 * \return  true if it succeeds, false if it fails.
 */

bool CMesh::calc_oriented_bounding_box(tTransform &tm, tVector &extent)
{
    Point average;

    // Compute average vertex
    if(!calc_average_vertex(average))
        return false;

    // Get doubled parameters
    double	axx(average[0]*average[0]), 
        axy(average[0]*average[1]),
        axz(average[0]*average[2]),
        ayy(average[1]*average[1]),
        ayz(average[1]*average[2]),
        azz(average[2]*average[2]);

    // Compute covariance matrix parameters
    double cxx(0.0), cxy(0.0), cxz(0.0), cyy(0.0), cyz(0.0), czz(0.0);
    CMesh::VertexIter vit(this->vertices_begin()), vitEnd(this->vertices_end());
    for ( ; vit != vitEnd; ++vit)
    {
        // Get point reference
        const Point &p(this->point(vit.handle()));

        cxx += p[0]*p[0] - axx; 
        cxy += p[0]*p[1] - axy;
        cxz += p[0]*p[2] - axz;
        cyy += p[1]*p[1] - ayy;
        cyz += p[1]*p[2] - ayz;
        czz += p[2]*p[2] - azz;
    }

    // Build covariance matrix
    vpl::math::CDMatrix cm(3, 3);
    cm(0, 0) = cxx; cm(0, 1) = cxy; cm(0, 2) = cxz;
    cm(1, 0) = cxy; cm(1, 1) = cyy; cm(1, 2) = cyz;
    cm(2, 0) = cxz; cm(2, 1) = cyz; cm(2, 2) = czz;


    // Compute bb
    return calc_obb_from_cm(cm, tm, extent);
}

/**
 * \brief   Calculates the average vertex.
 *
 * \param [in,out]  average The average.
 *
 * \return  true if it succeeds, false if it fails.
 */

bool CMesh::calc_average_vertex(Point &average)
{
    if (this->n_vertices() == 0)
    {
        return false;
    }

    average = CMesh::Point(0.0, 0.0, 0.0);
    for (CMesh::VertexIter vit = this->vertices_begin(); vit != this->vertices_end(); ++vit)
    {
        average += this->point(vit.handle()) * (1.0 / this->n_vertices());
    }

    return true;
}

/**
 * \brief   Translates.
 *
 * \param   x   The float to process.
 * \param   y   The float to process.
 * \param   z   The float to process.
 */

void CMesh::translate(float x, float y, float z)
{
    CMesh::VertexIter v_it, v_end(vertices_end());
    for (v_it=vertices_begin(); v_it!=v_end; ++v_it)
    {
        Point p(point(v_it));
        p[0] += x; p[1] += y; p[2] += z;
        set_point(v_it, p);
    }
}

/**
 * \brief   Transforms.
 *
 * \param   tm  The time.
 */

/*
void CMesh::transform(const osg::Matrix &tm)
{
    CMesh::VertexIter v_it, v_end(vertices_end());
    for (v_it=vertices_begin(); v_it!=v_end; ++v_it)
    {
        Point p(point(v_it));

        // Transform to osg
        osg::Vec3 osg_point(p[0], p[1], p[2]);

        // Do transformation
        osg_point = osg_point * tm;

        // Transform back to openmesh
        p[0] = osg_point[0];
        p[1] = osg_point[1];
        p[2] = osg_point[2];

        set_point(v_it, p);
    }
}
*/

/**
 * \brief   Transforms.
 *
 * \param   tm  The time.
 */
void CMesh::transform(const tTransform &tm)
{
    CMesh::VertexIter v_it, v_end(vertices_end());
    for (v_it=vertices_begin(); v_it!=v_end; ++v_it)
    {
        // Get point
        Point p(point(v_it));

        // Convert to eigen format
        vpl::math::CDVector3 pv;
        pv(0) = p[0]; pv(1) = p[1]; pv(2) = p[2];

        // Transform point
        pv = tm * pv;//.mult(tm, pv);

        // Set point
        p[0] = pv(0);
        p[1] = pv(1);
        p[2] = pv(2);

        set_point(v_it, p);
    }
}

/**
 * \brief   Calculates the obb from centimetres.
 *
 * \param [in,out]  cm      The centimetres.
 * \param [in,out]  tm      The time.
 * \param [in,out]  extent  The extent.
 *
 * \return  true if it succeeds, false if it fails.
 */

bool CMesh::calc_obb_from_cm(vpl::math::CDMatrix &cm, tTransform &tm, tVector &extent)
{
    // Eigenvalues output
    vpl::math::CDVector ev(3);

    // Compute eigen values and vectors
    vpl::math::eig(cm, ev);

    // find the right, up and forward vectors from the eigenvectors
    tVector r( cm(0,0), cm(1,0), cm(2,0) );
    tVector u( cm(0,1), cm(1,1), cm(2,1) );
    tVector f( cm(0,2), cm(1,2), cm(2,2) );
    r.normalize(); u.normalize(), f.normalize();

    // set the rotation matrix using the eigven vectors
    tm = tTransform(    r[0], r[1], r[2], 0,
                        u[0], u[1], u[2], 0,
                        f[0], f[1], f[2], 0,
                        0.0, 0.0, 0.0, 1.0);


    // now build the bounding box extents in the rotated frame
    tVector minim(1e10, 1e10, 1e10), maxim(-1e10, -1e10, -1e10);
    CMesh::VertexIter vit(this->vertices_begin()), vitEnd(this->vertices_end());
    for ( ; vit != vitEnd; ++vit)
    {
        // Get point reference
        const Point &p(this->point(vit.handle()));
        const tVector op(p[0], p[1], p[2]);

        // Compute transformed point
        tVector pp(r*op, u*op, f*op);

        // Get minimum and maximum
        for(int i = 0; i < 3; ++i)
        {
            minim[i] = vpl::math::getMin(pp[i], minim[i]);
            maxim[i] = vpl::math::getMax(pp[i], maxim[i]);
        }
    }

    // The center of the OBB is the average of the minimum and maximum
    tVector center((maxim+minim)*0.5);
    center = tm * center;

    // Modify transformation matrix
    tm.setTrans(center[0], center[1], center[2]);

    // the extents is half of the difference between the minimum and maximum
    extent = (maxim-minim)*0.5;

    return true;
}

/**
 * \brief   Calculates the oriented bb triangles.
 *
 * \param [in,out]  tm      The time.
 * \param [in,out]  extent  The extent.
 *
 * \return  true if it succeeds, false if it fails.
 */

bool CMesh::calc_oriented_bb_triangles(tTransform &tm, tVector &extent)
{
    CMesh::Point tsides[3], mu(0.0, 0.0, 0.0);

    double am(0.0);

    // Compute covariance matrix parameters
    double cxx(0.0), cxy(0.0), cxz(0.0), cyy(0.0), cyz(0.0), czz(0.0);

    // For all triangles
    CMesh::FaceIter fit(this->faces_sbegin()), fitEnd(this->faces_end());
    for(; fit != fitEnd; ++fit)
    {
        // Number of face vertices
        int nfv(0);

        // Face mean point
        Point mui(0.0, 0.0, 0.0);

        // For all face vertices
        CMesh::FaceVertexIter vit(this->fv_iter(fit));
        for(; vit; ++vit, ++nfv)
        {
            if(nfv < 3)
            {
                tsides[nfv] = this->point(vit.handle());
                mui += tsides[nfv];
            }
        }

        // Compute triangle area
        double ai(((tsides[1]-tsides[0]) % (tsides[2]-tsides[0])).length() * 0.5);
        am += ai;

        // Compute average point
        mui /= 3.0;
        mu += mui*ai;

        const CMesh::Point &p(tsides[0]), &q(tsides[1]), &r(tsides[2]);

        // Modify covariance matrix 
        cxx += ( 9.0*mui[0]*mui[0] + p[0]*p[0] + q[0]*q[0] + r[0]*r[0] )*(ai/12.0);
        cxy += ( 9.0*mui[0]*mui[1] + p[0]*p[1] + q[0]*q[1] + r[0]*r[1] )*(ai/12.0);
        cxz += ( 9.0*mui[0]*mui[2] + p[0]*p[2] + q[0]*q[2] + r[0]*r[2] )*(ai/12.0);
        cyy += ( 9.0*mui[1]*mui[1] + p[1]*p[1] + q[1]*q[1] + r[1]*r[1] )*(ai/12.0);
        cyz += ( 9.0*mui[1]*mui[2] + p[1]*p[2] + q[1]*q[2] + r[1]*r[2] )*(ai/12.0);
        czz += ( 9.0*mui[2]*mui[2] + p[2]*p[2] + q[2]*q[2] + r[2]*r[2] )*(ai/12.0);
    }

    // divide out the am fraction from the average position and covariance terms
    mu /= am;
    cxx /= am; cxy /= am; cxz /= am; cyy /= am; cyz /= am; czz /= am;

    // now subtract off the E[x]*E[x], E[x]*E[y], ... terms
    cxx -= mu[0]*mu[0]; cxy -= mu[0]*mu[1]; cxz -= mu[0]*mu[2];
    cyy -= mu[1]*mu[1]; cyz -= mu[1]*mu[2]; czz -= mu[2]*mu[2];

    // Build covariance matrix
    vpl::math::CDMatrix cm(3, 3);
    cm(0, 0) = cxx; cm(0, 1) = cxy; cm(0, 2) = cxz;
    cm(1, 0) = cxy; cm(1, 1) = cyy; cm(1, 2) = cyz;
    cm(2, 0) = cxz; cm(2, 1) = cyz; cm(2, 2) = czz;


    // Compute bb
    return calc_obb_from_cm(cm, tm, extent);
}

/**
 * \brief   Saves the given file.
 *
 * \param   filename    The const std::string &amp; to save.
 */
void CMesh::save(const std::string &filename)
{
    if(filename.length() > 0)
    {
        OpenMesh::IO::Options wopt;
        wopt += OpenMesh::IO::Options::Binary;

        // Write file
        if (!OpenMesh::IO::write_mesh(*this, filename, wopt))
        {
            VPL_LOG_ERROR("Cannot write model file: " << filename << std::endl);
        }
    }
}

/**
 * \brief   Creates indexing object.
 *
 * \param   mesh    The mesh.
 */

void CMeshIndexing::create(CMesh *mesh)
{
    assert(mesh != 0);
    meshPtr = mesh;

    // Resize vector
    reserve(mesh->n_vertices());

    vhid_map.clear();

    // Request normals
    mesh->request_vertex_normals();
    if(!mesh->has_vertex_normals())
        VPL_ERROR("Mesh has not normals.");

    mesh->update_normals();

    // For all vertices
    CMesh::VertexIter it(mesh->vertices_sbegin()), itEnd(mesh->vertices_end());
    size_t ind(0);
    for (; it != itEnd; ++it, ++ind) 
    {
        CMeshIndexDataSimple data;

        // Store handle
        data.vh = it.handle();

        // Get and store vertex in VPL format
        const CMesh::Point &p(mesh->point(it));
        data.point = tVec(p[0], p[1], p[2]);

        // Get and store normal in VPL format
        const CMesh::Normal &n(mesh->normal(it));
        data.normal = tVec(n[0], n[1], n[2]);
        data.normal.normalize();

        // Store index to map
        vhid_map.insert(std::pair<CMesh::VertexHandle, size_t>(data.vh, size()));

        // Store data
        push_back(data);
    }
}

/**
 * \brief   Actualizes this object.
 */
void CMeshIndexing::actualize()
{
    assert(meshPtr != 0);

    meshPtr->update_normals();

    // For all vertices
    CMesh::VertexIter it(meshPtr->vertices_sbegin()), itEnd(meshPtr->vertices_end());
    size_t ind(0);
    for (; it != itEnd; ++it, ++ind) 
    {
        CMeshIndexDataSimple data;

        // Store handle
        data.vh = it.handle();

        // Get and store vertex in VPL format
        const CMesh::Point &p(meshPtr->point(it));
        data.point = tVec(p[0], p[1], p[2]);

        // Get and store normal in VPL format
        const CMesh::Normal &n(meshPtr->normal(it));
        data.normal = tVec(n[0], n[1], n[2]);
        data.normal.normalize();

//         // Store index to map
//         vhid_map.insert(std::pair<CMesh::VertexHandle, size_t>(data.vh, size()));

        // Store data
        (*this)[ind] = data;
    }
}
