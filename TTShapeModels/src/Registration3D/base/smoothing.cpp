///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#include "base/smoothing.h"
#include <queue>
#include <OpenMesh/Tools/Smoother/JacobiLaplaceSmootherT.hh>
#include <OpenMesh/Tools/Smoother/LaplaceSmootherT.hh>

CSmooth::CSmooth(EType type /*= SIMPLE_CENTERING*/)
 : m_type(type) 
 , m_centeringNormalForceWeight(1.0)
 , m_loops(1)
{
}

/**
 * \brief   Applies the given mesh.
 *
 * \param [in,out]  mesh    The mesh.
 */
void CSmooth::apply(CMesh &mesh, float weight)
{
    switch (m_type)
    {
    case CSmooth::SIMPLE_CENTERING:
        applySimpleCentering(mesh, weight);
        break;
    case CSmooth::LARGE_GAUSSIAN:
        applyLargeGaussian(mesh, weight);
        break;
    case CSmooth::JACOBI_LAPLACE:
        applyJacobiLaplace(mesh, weight);
        break;
    default:
        break;
    }
}

/**
 * \brief   Applies the simple gaussian smoothing on mesh.
 *
 * \param [in,out]  mesh    The mesh.
 * \param   weight          The weight of smoothing.
 */
void CSmooth::applySimpleCentering(CMesh &mesh, float weight)
{
    // Original point position property handle
    OpenMesh::VPropHandleT<CMesh::Point> prop_orig_positions;
    mesh.add_property(prop_orig_positions);

    mesh.update_face_normals();
    mesh.update_vertex_normals();

    // Store original positions
    CMesh::VertexIter  v_it, v_end(mesh.vertices_end());
    for (v_it=mesh.vertices_begin(); v_it!=v_end; ++v_it)
    {
        mesh.property(prop_orig_positions, v_it) = mesh.point(v_it);
    }

    CMesh::VertexIter mit(mesh.vertices_begin()), mitEnd(mesh.vertices_end());
    size_t point_id(0);
    for(; mit != mitEnd; ++mit, ++point_id)
    {
        // Already done point
        //         if(changes[point_id].changed)
        //             continue;

        const CMesh::Point &cp(mesh.point(mit));
        CMesh::Point shift(0.0, 0.0, 0.0);
        double counter(0.0);
        // Circulate around given point and compute center point
        CMesh::VertexVertexIter vv_it(mesh.vv_iter(mit.handle()));
        for(; vv_it; ++vv_it)
        {
            shift += mesh.property(prop_orig_positions, vv_it);
            counter += 1.0;
        }

        // Move point
        if(counter > 0.9)
        {
            shift = shift / counter;
            shift = shift - cp;

            // Compute part of shift parallel with point normal
            CMesh::Point n(mesh.normal(mit));
            CMesh::Point parallel((shift | n)/n.sqrnorm() * n);
            // Compute part of shift orthogonal to the normal
            CMesh::Point orthogonal(shift - parallel);

            shift = (parallel * m_centeringNormalForceWeight + orthogonal ) * weight;
            mesh.set_point(mit.handle(), cp + shift);
        }
    }

    // Remove stored property
    mesh.remove_property(prop_orig_positions);
}

/**
 * \brief   Applies the large gaussian.
 *
 * \param [in,out]  mesh    The mesh.
 * \param   weight          The weight.
 */
void CSmooth::applyLargeGaussian(CMesh &mesh, float weight)
{
    // Original point position property handle
    OpenMesh::VPropHandleT<CMesh::Point> prop_orig_positions;
    mesh.add_property(prop_orig_positions);

    // Store original positions
    CMesh::VertexIter  v_it, v_end(mesh.vertices_end());
    for (v_it=mesh.vertices_begin(); v_it!=v_end; ++v_it)
    {
        mesh.property(prop_orig_positions, v_it) = mesh.point(v_it);
    }

    CMesh::VertexIter mit(mesh.vertices_begin()), mitEnd(mesh.vertices_end());
    size_t point_id(0);
    for(; mit != mitEnd; ++mit, ++point_id)
    {

        std::queue<CMesh::VHandle> points_to_test;
        std::set<CMesh::VHandle> points_done;
        
        // Store starting point
        points_to_test.push(mit.handle());

        // Get current point
        const CMesh::Point &cp(mesh.point(mit));
        CMesh::Point p(0.0, 0.0, 0.0);
        double counter(0.0);


        while (!points_to_test.empty())
        {
            // Get handle
            CMesh::VHandle phandle(points_to_test.front());
            points_to_test.pop();

            // Is point already tested?
            if(points_done.find(phandle) != points_done.end())
                continue;

            // Store point as tested
            points_done.insert(phandle);

            // Add point to sum
            p += mesh.property(prop_orig_positions, mit);
            counter += 1.0;

            // Find all not tested neighbors 
            CMesh::VertexVertexIter it(mesh.vv_iter(phandle));
            for(;it; ++it)
                if(points_done.find(it.handle()) == points_done.end())
                    points_to_test.push(it.handle());
        }

        // Move point
        if(counter > 0.9)
        {
            p = p / counter;
            p = p - cp;
            p = p * weight;
            mesh.set_point(mit.handle(), cp + p);
        }
    }

    // Remove stored property
    mesh.remove_property(prop_orig_positions);
}

/**
 * \brief   Applies the Jacobi-Laplace smoothing operator from openmesh lib.
 *
 * \param [in,out]  mesh    The mesh.
 * \param   weight          The weight.
 */
void CSmooth::applyJacobiLaplace(CMesh &mesh, float weight)
{
    OpenMesh::Smoother::JacobiLaplaceSmootherT<CMesh> smoother(mesh);
    OpenMesh::Smoother::SmootherT<CMesh>::Component component = OpenMesh::Smoother::SmootherT<CMesh>::Tangential_and_Normal;
    OpenMesh::Smoother::SmootherT<CMesh>::Continuity continuity = OpenMesh::Smoother::SmootherT<CMesh>::C1;
    smoother.initialize(component, continuity);

    // smoothing cycle
    for (int smooth_iter = 0; smooth_iter < m_loops; ++smooth_iter)
    {
        smoother.smooth(1);
    }
}
