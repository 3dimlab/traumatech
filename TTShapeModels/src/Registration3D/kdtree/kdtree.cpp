///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#include <kdtree/kdtree.h>

/**
 * \brief	Constructor.
 *
 * \param [in,out]	model	If non-null, the model.
 */
CPointCloudAdaptorOM::CPointCloudAdaptorOM(CMesh * model)
	: m_mesh(model)
	, m_bbmin(1.0f, 1.0f, 1.0f)
	, m_bbmax(-1.0f, -1.0f, -1.0f)
{
	VPL_ASSERT(model != 0);

	// Create indexing
	m_indexing = new CMeshIndexing(model);

	// Compute and store bounding box
	model->calc_bounding_box(m_bbmin, m_bbmax);
}

CPointCloudAdaptorOM::CPointCloudAdaptorOM()
    : m_bbmin(1.0f, 1.0f, 1.0f)
    , m_bbmax(-1.0f, -1.0f, -1.0f)
{

}

/**
 * \brief	Returns the distance between the vector "p1[0:size-1]" and the data point with index "idx_p2" stored in the class.
 *
 * \param	p1	  	The first const T *.
 * \param	idx_p2	The second index p.
 * \param	size  	The size.
 *
 * \return	.
 */
double CPointCloudAdaptorOM::kdtree_distance(const double *p1, const size_t idx_p2,size_t size) const
{
	VPL_ASSERT(idx_p2 >= 0 && idx_p2 < m_indexing->size());

	// Get point reference
	const CMesh::Point &p0(m_mesh->point((*m_indexing)[idx_p2].vh));
	double dx(p0[0] - p1[0]), dy(p0[1] - p1[1]), dz(p0[2] - p1[2]);

	return dx*dx + dy*dy + dz*dz;
}

/**
 * \brief	Returns the dim'th component of the idx'th point in the class:
 *			Since this is inlined and the "dim" argument is typically an immediate value, the
 *			"if/else's" are actually solved at compile time.
 *
 * \param	idx	The index.
 * \param	dim	The dim.
 *
 * \return	.
 */

double CPointCloudAdaptorOM::kdtree_get_pt(const size_t idx, int dim) const
{
	VPL_ASSERT(idx >= 0 && idx < m_indexing->size());
	VPL_ASSERT(dim >= 0 && dim < 3);

	// Get point reference
	const CMesh::Point &p0(m_mesh->point((*m_indexing)[idx].vh));
	return p0[dim];
}

/**
 * \brief   Gets a vertex handle.
 *
 * \param   id  The identifier.
 *
 * \return  The vertex handle.
 */
CMesh::VertexHandle &CPointCloudAdaptorOM::getVH(size_t id)
{
    VPL_ASSERT(id >= 0 && id < m_indexing->size());
    return (*m_indexing)[id].vh;
}

//=================================================================================================
//	CLASS CKDTreeOM
//=================================================================================================

/**
 * \brief	Default constructor.
 */
CKDTreeOM::CKDTreeOM()
	: m_tree(0)
{

}

/**
 * \brief	Constructor.
 *
 * \param [in,out]	mesh	If non-null, the mesh.
 */
CKDTreeOM::CKDTreeOM(CMesh *mesh)
	: m_tree(0)
{
    VPL_ASSERT(mesh != 0);
    VPL_ASSERT(mesh->n_vertices() > 0);

    create(mesh);
}

/**
 * \brief	Destructor.
 */
CKDTreeOM::~CKDTreeOM()
{
	clear();
}

/**
 * \brief   Creates this object.
 *
 * \param [in,out]  mesh    If non-null, the mesh.
 *
 * \return  true if it succeeds, false if it fails.
 */
bool CKDTreeOM::create(CMesh *mesh)
{
    // Clear all previous data
    clear();

    // Create mesh adaptor
    m_pc = new CPointCloudAdaptorOM(mesh);

    // Create tree data structure
    m_tree = new tKDTreeAdaptor(3, *m_pc, nanoflann::KDTreeSingleIndexAdaptorParams(10));
    m_tree->buildIndex();
    m_tree->size();
    return true;
}

/**
 * \brief   Clears this object to its blank/initial state.
 */
void CKDTreeOM::clear()
{
    if(m_tree != 0)
        delete m_tree;

    m_tree = 0;
}

/**
 * \brief   Gets a closest point.
 *
 * \param   point           The point.
 * \param [in,out]  result  The result.
 *
 * \return  true if it succeeds, false if it fails.
 */
bool CKDTreeOM::getClosestPoints(const tVec &point, SIndexDistancePairs &result) const 
{
    double pt[3];

    pt[0] = point[0]; pt[1] = point[1]; pt[2] = point[2];

    // Number of points is given by output size
    size_t num(result.indexes.size());
    VPL_ASSERT(num > 0);

    m_tree->knnSearch(pt, num, &result.indexes[0], &result.distances[0]);

    return true;
}
