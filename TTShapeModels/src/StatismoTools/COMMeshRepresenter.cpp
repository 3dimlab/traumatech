///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#include "StatismoTools/COMMeshRepresenter.h"
#include "HDF5Utils.h"
#include "StatismoUtils.h"

/**
 * \brief   Loads data from H5 file.
 *
 * \param   fg  The const H5::Group&amp; to load.
 */
void statismo::COMMeshRepresenter::Load(const H5::Group& fg)
{
    COMMeshRepresenter::DatasetPointerType ref;

    std::string repName = HDF5Utils::readStringAttribute(fg, "name");
    if (repName == "OpenMeshRepresenter") 
    {
        ref = LoadRefLegacy(fg);
    } else {
        ref = LoadRef(fg);
    }
    this->SetReference(ref);
}

/**
 * \brief   Makes a deep copy of this object.
 *
 * \return  null if it fails, else a copy of this object.
 */
statismo::COMMeshRepresenter* statismo::COMMeshRepresenter::Clone() const
{
    return Create(m_reference);
}

/**
 * \brief   Destructor.
 */
statismo::COMMeshRepresenter::~COMMeshRepresenter()
{

}

/**
 * \brief   Point to vector.
 *
 * \param   pt  The point.
 *
 * \return  .
 */
statismo::VectorType statismo::COMMeshRepresenter::PointToVector(const PointType& pt) const
{
    return Eigen::Map<const VectorType>(pt.data(), 3).cast<float>();
}

/**
 * \brief   Sample to sample vector.
 *
 * \param   sample  The sample.
 *
 * \return  .
 */

statismo::VectorType statismo::COMMeshRepresenter::SampleToSampleVector(DatasetConstPointerType sample) const
{
    assert(m_reference != 0);

    VectorType sampleVec = VectorType::Zero(sample->n_vertices() * 3);

    // For all mesh points
    CMesh::VIter it(sample->vertices_begin()), itEnd(sample->vertices_end());
    unsigned id(0);

    // Store point coordinates to the output vector
    for(; it != itEnd; ++it, ++id)
    {
        const CMesh::Point &p(sample->point(it));
        unsigned idx = MapPointIdToInternalIdx(id, 0);
        sampleVec(idx) = p[0];
        idx = MapPointIdToInternalIdx(id, 1);
        sampleVec(idx) = p[1];
        idx = MapPointIdToInternalIdx(id, 2);
        sampleVec(idx) = p[2];
    }

    return sampleVec;

}

/**
 * \brief   Sample vector to sample.
 *
 * \param   sample  The sample.
 *
 * \return  .
 */
statismo::COMMeshRepresenter::DatasetPointerType statismo::COMMeshRepresenter::SampleVectorToSample(const statismo::VectorType& sample) const
{
    assert(m_reference != 0);

    // Create copy of internal mesh
    COMMeshRepresenter::DatasetPointerType pd = new CMesh(*m_reference);

    size_t nv(m_reference->n_vertices());
    nv = pd->n_vertices();

    // For all mesh points
    CMesh::VIter it(pd->vertices_begin()), itEnd(pd->vertices_end());
    unsigned id(0);
    for(; it != itEnd; ++it, ++id) 
    {
        // Convert statismo vector to point
        CMesh::Point pt;
        for (unsigned d = 0; d < GetDimensions(); d++) 
        {
            unsigned idx = MapPointIdToInternalIdx(id, d);
            pt[d] = sample(idx);
        }

        // Set mesh point position
        pd->set_point(it, pt);
    }

    return pd;
}

/**
 * \brief   Point sample from sample.
 *
 * \param   sample  The sample.
 * \param   ptid    The ptid.
 *
 * \return  .
 */
statismo::COMMeshRepresenter::ValueType statismo::COMMeshRepresenter::PointSampleFromSample(DatasetConstPointerType sample, unsigned ptid) const
{
    if (ptid >= sample->n_vertices()) 
    {
        throw StatisticalModelException(
            "invalid ptid provided to PointSampleFromSample");
    }

    return ValueType(sample->point(sample->vertex_handle(ptid)));
}

/**
 * \brief   Point sample to point sample vector.
 *
 * \param   v   The const ValueType&amp; to process.
 *
 * \return  .
 */
statismo::VectorType statismo::COMMeshRepresenter::PointSampleToPointSampleVector(const ValueType& v) const
{
    VectorType vec(GetDimensions());
    for (unsigned i = 0; i < GetDimensions(); i++) {
        vec(i) = v[i];
    }
    return vec;
}

/**
 * \brief   Point sample vector to point sample.
 *
 * \param   pointSample The point sample.
 *
 * \return  .
 */

statismo::COMMeshRepresenter::ValueType statismo::COMMeshRepresenter::PointSampleVectorToPointSample(const statismo::VectorType& v) const
{
    ValueType value;
    for (unsigned i = 0; i < GetDimensions(); i++) {
        value[i] = v(i);
    }
    return value;
}

/**
 * \brief   Saves mesh to H5 file.
 *
 * \param   fg  The const H5::Group&amp; to save.
 */

void statismo::COMMeshRepresenter::Save(const H5::Group& fg) const
{
    statismo::MatrixType vertexMat = statismo::MatrixType::Zero(3, m_reference->n_vertices());

    // For all mesh points
    CMesh::VIter itv(m_reference->vertices_begin()), itvEnd(m_reference->vertices_end());
    unsigned id(0);
    for(; itv != itvEnd; ++itv, ++id) 
    {
        // Copy point to output matrix
        const PointType &pt(m_reference->point(itv));
        for (unsigned d = 0; d < 3; d++) 
            vertexMat(d, id) = pt[d];
        
    }

    HDF5Utils::writeMatrix(fg, "./points", vertexMat);

    // check the dimensionality of a face (i.e. the number of points it has). 
    // We assume that all the faces are triangles.
    unsigned numPointsPerCell = 0;
    if (m_reference->n_faces() > 0) 
    {
        numPointsPerCell = 3;
    }

    // Prepare faces storage
    typedef statismo::GenericEigenType<unsigned int>::MatrixType UIntMatrixType;
    UIntMatrixType facesMat = UIntMatrixType::Zero(numPointsPerCell, m_reference->n_faces());

    // For all mesh faces
    CMesh::FIter itf(m_reference->faces_begin()), itfEnd(m_reference->faces_end());
    id = 0;
    for(; itf != itfEnd; ++itf, ++id)
    {
//        CMesh::Face &cell(m_reference->face(itf));
        // For three face points
        CMesh::FVIter itfv(m_reference->fv_begin(itf));
        for(int d = 0; d < 3; ++d, ++itfv)
        {
            assert(itfv);

            // Get face vertex handle id
            facesMat(d, id) = itfv.handle().idx();
        }
    }
    
    HDF5Utils::writeMatrixOfType<unsigned int>(fg, "./cells", facesMat);
}

/**
 * \brief   Gets a point identifier for point.
 *
 * \param   point   The point.
 *
 * \return  The point identifier for point.
 */
unsigned statismo::COMMeshRepresenter::GetPointIdForPoint(const PointType& point) const
{
    double e(std::numeric_limits<float>::min()*10.0);
    // Iterate through all points
    CMesh::VIter it(m_reference->vertices_begin()), itEnd(m_reference->vertices_end());
    for(; it != itEnd; ++it)
    {
        const CMesh::Point &p(m_reference->point(it));
        bool ok(true);
        for(int i = 0; i < 3; ++i)
            if(fabs(p[i]- point[i]) > e)
            {
                ok = false;
                break;
            }
        if(ok)
            return it->idx(); 
    }

    return 0;
}

/**
 * \brief   Sets a reference.
 *
 * \param   reference   The reference.
 */
void statismo::COMMeshRepresenter::SetReference(const DatasetConstPointerType reference)
{
    m_reference = new CMesh(*reference);

    DomainType::DomainPointsListType ptList;

    // For all mesh points
    CMesh::VIter it(m_reference->vertices_begin()), itEnd(m_reference->vertices_end());
    for(; it != itEnd; ++it)
    {
        ptList.push_back(m_reference->point(it));
    }

    m_domain = DomainType(ptList);
}

/**
 * \brief   Loads a reference legacy.
 *
 * \param   fg  The foreground.
 *
 * \return  null if it fails, else the reference legacy.
 */
CMesh* statismo::COMMeshRepresenter::LoadRefLegacy(const H5::Group& fg) const
{
    return 0;
}

/**
 * \brief   Loads a reference.
 *
 * \param   fg  The foreground.
 *
 * \return  null if it fails, else the reference.
 */

CMesh* statismo::COMMeshRepresenter::LoadRef(const H5::Group& fg) const
{
    statismo::MatrixType vertexMat;
    HDF5Utils::readMatrix(fg, "./points", vertexMat);

    typedef statismo::GenericEigenType<unsigned int>::MatrixType UIntMatrixType;
    UIntMatrixType cellsMat;
    HDF5Utils::readMatrixOfType<unsigned int>(fg, "./cells", cellsMat);

    // create the reference from this information
    COMMeshRepresenter::DatasetPointerType ref = new CMesh();

    unsigned nVertices = vertexMat.cols();
    unsigned nCells = cellsMat.cols();

    std::vector<CMesh::VertexHandle> handles(nVertices);
    // For all vertices
    for (unsigned i = 0; i < nVertices; ++i)
    {
        handles[i] = ref->add_vertex(CMesh::Point(vertexMat(0, i), vertexMat(1, i), vertexMat(2, i)));
    }

    // For all faces
    unsigned cellDim = cellsMat.rows();
    for (unsigned i = 0; i < nCells; ++i)
    {
        // New face
        std::vector<CMesh::VertexHandle> face_handles;

        // Add points
        for (unsigned d = 0; d < cellDim; d++) 
        {
            face_handles.push_back(handles[cellsMat(d, i)]);
        }

        // Add face to mesh
        ref->add_face(face_handles);
    }
 
    return ref;
}
