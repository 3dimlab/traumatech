///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#include <StatismoTools/StatismoLoader.h>

/**
 * \brief   Default constructor.
 */
CStatismoLoader::CStatismoLoader()
{

}

/**
 * \brief   Loads a file.
 *
 * \param   filename    Filename of the file.
 *
 * \return  true if it succeeds, false if it fails.
 */
bool CStatismoLoader::loadFile(const std::string &filename)
{
    const H5std_string FILE_NAME( filename );

    // Test if file exists and is in HD5 format
    if(! H5::H5File::isHdf5(FILE_NAME))
        return false;

    H5::H5File file( FILE_NAME, H5F_ACC_RDONLY );
    

}

/**
 * \brief   Loads group data from file.
 *
 * \param   fg  The const H5::Group&amp; to load.
 *
 * \return  true if it succeeds, false if it fails.
 */

bool CStatismoLoader::load(const H5::Group& fg)
{
    return false;
}
