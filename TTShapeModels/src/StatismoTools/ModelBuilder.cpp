///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#include <StatismoTools/ModelBuilder.h>
#include <StatismoTools/COMMeshRepresenter.h>

/**
 * \brief   Default constructor.
 */
statismo::CModelBuilder::CModelBuilder()
    : m_numPoints(0)
{

}

/**
 * \brief   Builds the model.
 *
 * \return  null if it fails, else.
 */
statismo::CStatismoModel * statismo::CModelBuilder::buildModel()
{
    size_t num_meshes(m_meshes.size());
    if(num_meshes == 0)
        return 0;
    
    if(m_numPoints == 0)
        return 0;

    // Create representer from the first mesh
    COMMeshRepresenterPtr representer(COMMeshRepresenter::Create(*m_meshes.begin()));

    // Build the sample matrix X
    MatrixType X(num_meshes, m_numPoints*3);
    {


        tMeshVec::const_iterator it(m_meshes.begin()), itEnd(m_meshes.end());
        size_t i(0);
        for(; it != itEnd; ++it, ++i)
            X.row(i) = representer->SampleToSampleVector(it->get());
    }
    
    // build the model
    CStatismoModel* model = buildModel(*representer, X);
//    MatrixType scores;
//     if (computeScores) {
//         scores = this->ComputeScores(X, model);
//     }


//     typename BuilderInfo::ParameterInfoList bi;
//     bi.push_back(BuilderInfo::KeyValuePair("NoiseVariance ", Utils::toString(noiseVariance)));
// 
//     typename BuilderInfo::DataInfoList dataInfo;
//     i = 0;
//     for (typename DataItemListType::const_iterator it = sampleDataList.begin();
//         it != sampleDataList.end();
//         ++it, i++) {
//             std::ostringstream os;
//             os << "URI_" << i;
//             dataInfo.push_back(BuilderInfo::KeyValuePair(os.str().c_str(),(*it)->GetDatasetURI()));
//     }
// 
// 
//     // finally add meta data to the model info
//     BuilderInfo builderInfo("PCAModelBuilder", dataInfo, bi);
// 
//     ModelInfo::BuilderInfoList biList;
//     biList.push_back(builderInfo);
// 
//     ModelInfo info(scores, biList);
//     model->SetModelInfo(info);

    return model;    



}

/**
 * \brief   Builds a model.
 *
 * \param   representer The representer.
 * \param   Samples     The samples.
 *
 * \return  null if it fails, else.
 */
statismo::CStatismoModel * statismo::CModelBuilder::buildModel(const COMMeshRepresenter &representer, const MatrixType &Samples)
{
    typedef Eigen::JacobiSVD<MatrixType> SVDType;
    typedef Eigen::JacobiSVD<MatrixTypeDoublePrecision> SVDDoublePrecisionType;

    // Number of meshes and number of points
    unsigned n = Samples.rows();
    unsigned p = Samples.cols();

    // Tolerance
    const double TOLERANCE(1e-5);

    RowVectorType mu = Samples.colwise().mean(); // needs to be row vector
    MatrixType X0 = Samples.rowwise() - mu;

    // We destinguish the case where we have more variables than samples and
    // the case where we have more samples than variable.
    // In the first case we compute the (smaller) inner product matrix instead of the full covariance matriSamples.
    // It is known that this has the same non-zero singular values as the covariance matriSamples.
    // Furthermore, it is possible to compute the corresponding eigenvectors of the covariance matrix from the
    // decomposition.

    if (n < p) {
        // we compute the eigenvectors of the covariance matrix by computing an SVD of the
        // n x n inner product matrix 1/(n-1) X0X0^T
        MatrixType Cov = X0 * X0.transpose() * 1.0/(n-1);
        SVDDoublePrecisionType SVD(Cov.cast<double>(), Eigen::ComputeThinV);
        VectorType singularValues = SVD.singularValues().cast<ScalarType>();
        MatrixType V = SVD.matrixV().cast<ScalarType>();

        unsigned numComponentsAboveTolerance = ((singularValues.array() - TOLERANCE) > 0).count();

        // there can be at most n-1 nonzero singular values in this case. Everything else must be due to numerical inaccuracies
        unsigned numComponentsToKeep = std::min(numComponentsAboveTolerance, n - 1);
        // compute the pseudo inverse of the square root of the singular values
        // which is then needed to recompute the PCA basis
        VectorType singSqrt = singularValues.array().sqrt();
        VectorType singSqrtInv = VectorType::Zero(singSqrt.rows());
        for (unsigned i = 0; i < numComponentsToKeep; i++) {
            assert(singSqrt(i) > TOLERANCE);
            singSqrtInv(i) = 1.0 / singSqrt(i);
        }

        // we recover the eigenvectors U of the full covariance matrix from the eigenvectors V of the inner product matriSamples.
        // We use the fact that if we decompose X as X=UDV^T, then we get X^TX = UD^2U^T and XX^T = VD^2V^T (exploiting the orthogonormality
        // of the matrix U and V from the SVD). The additional factor sqrt(n-1) is to compensate for the 1/sqrt(n-1) in the formula
        // for the covariance matriSamples.
        MatrixType pcaBasis = (X0.transpose() * V * singSqrtInv.asDiagonal() / sqrt(n-1.0)).topLeftCorner(p, numComponentsToKeep);;

        if (numComponentsToKeep == 0) {
            throw StatisticalModelException("All the eigenvalues are below the given tolerance. Model cannot be built.");
        }

        VectorType sampleVarianceVector = singularValues.topRows(numComponentsToKeep);
        VectorType pcaVariance = (sampleVarianceVector);

        CStatismoModel::tStatismoImpl* model = CStatismoModel::tStatismoImpl::Create(&representer, mu, pcaBasis, pcaVariance, 0.0);

        return new CStatismoModel(model);
    } else {
        // we compute an SVD of the full p x p  covariance matrix 1/(n-1) X0^TX0 directly
        SVDType SVD(X0.transpose() * X0 * 1.0/(n-1), Eigen::ComputeThinU);
        VectorType singularValues = SVD.singularValues();
        unsigned numComponentsToKeep = ((singularValues.array() - TOLERANCE) > 0).count();
        MatrixType pcaBasis = SVD.matrixU().topLeftCorner(p, numComponentsToKeep);

        if (numComponentsToKeep == 0) {
            throw StatisticalModelException("All the eigenvalues are below the given tolerance. Model cannot be built.");
        }

        VectorType sampleVarianceVector = singularValues.topRows(numComponentsToKeep);
        VectorType pcaVariance = (sampleVarianceVector);

        CStatismoModel::tStatismoImpl* model = CStatismoModel::tStatismoImpl::Create(&representer, mu, pcaBasis, pcaVariance, 0.0);

        return new CStatismoModel(model);
    }
}

/**
 * \brief   Adds a model.
 *
 * \param [in,out]  mesh    The mesh.
 *
 * \return  true if it succeeds, false if it fails.
 */
bool statismo::CModelBuilder::addModel(CMesh &mesh)
{
    size_t id(m_meshes.size()); 

    if(m_numPoints == 0)
        m_numPoints = mesh.n_vertices();

    // Wrong number of points?
    if(m_numPoints != mesh.n_vertices())
        return false;

    m_meshes.push_back(new CMesh(mesh));

    return true;// id;
}

/**
 * \brief   Resets this object.
 */
void statismo::CModelBuilder::reset()
{
    m_meshes.clear();
    m_numPoints = 0;
}

