///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#include "StatismoTools/StatismoModel.h"
#include <VPL/Base/Logging.h>

/**
 * \brief   Copy constructor.
 *
 * \param   model   The model.
 */
statismo::CStatismoModel::CStatismoModel(const CStatismoModel &model)
{
    if(model.m_representer != 0)
        m_representer = model.m_representer->Create(model.m_representer->GetReference());

    if(model.m_statismo != 0)
        m_statismo = tStatismoImpl::Create(model.m_representer.get(), model.m_statismo->GetMeanVector(), model.m_statismo->GetPCABasisMatrix(), model.m_statismo->GetPCAVarianceVector(), model.m_statismo->GetNoiseVariance());
}


/**
 * \file    C:\Work\parking\tt_registration\src\StatismoTools\StatismoModel.cpp
 *
 * \brief   Implements the statismo model class.
 */
void statismo::CStatismoModel::setNewSModel(tStatismoImpl *smodel)
{
    if(m_statismo != 0)
        delete m_statismo;

    m_statismo = smodel;
}

/**
 * \brief   Loads model from the given file.
 *
 * \param   filename    The const std::string &amp; to load.
 */
bool statismo::CStatismoModel::Load(const std::string &filename)
{
    try 
    {
        setNewSModel(tStatismoImpl::Load(m_representer.get(), filename));
    } 
    catch (statismo::StatisticalModelException& s) 
    {
        VPL_ERROR(s.what());
        return false;
    }

    return true;
}

/**
 * \brief   Loads model from the H5 group.
 *
 * \param   modelRoot   The const H5::Group&amp; to load.
 */
bool statismo::CStatismoModel::Load(const H5::Group& modelRoot)
{
    try 
    {
        setNewSModel(tStatismoImpl::Load(m_representer.get(), modelRoot));
    } 
    catch (statismo::StatisticalModelException& s) 
    {
        VPL_ERROR(s.what());
        return false;
    }

    return true;
}

/**
 * \brief   Saves the given file.
 *
 * \param   filename    The const std::string &amp; to save.
 *
 * \return  true if it succeeds, false if it fails.
 */
bool statismo::CStatismoModel::Save(const std::string &filename)
{
    try
    {
        m_statismo->Save(filename);
    }
    catch (const std::exception& Exception )
    {
        VPL_ERROR(Exception.what());
        return false;
    }
    return true;
}

/**
 * \brief   Determines the mean value.
 *
 * \return  The mean value.
 */
statismo::COMMeshRepresenter::DatasetPointerType statismo::CStatismoModel::mean() const
{
    return m_statismo->DrawMean();
}

/**
 * \brief   Gets the random.
 *
 * \return  .
 */
statismo::COMMeshRepresenter::DatasetPointerType statismo::CStatismoModel::random() const 
{
    return m_statismo->DrawSample(true);
}


/**
 * \brief   Gets the variances.
 *
 * \param [in,out]  variances   The variances.
 *
 * \return  The variances.
 */
size_t statismo::CStatismoModel::getVariances(tCoefficients &variances) const
{
    // Get variances vector from statismo
    const statismo::VectorType v(m_statismo->GetPCAVarianceVector());

    // Get number of components
    size_t num(v.rows());

    assert(num > 0);

    // Resize output
    variances.resize(num);

    // For all components
    tCoefficients::iterator it(variances.begin()), itEnd(variances.end());
    size_t c(0);
    for(; it != itEnd; ++it, ++c)
        *it = v[c];

    return num;
}

/**
 * \brief   Samples.
 *
 * \param   coefficients    The coefficients.
 *
 * \return  .
 */
statismo::COMMeshRepresenter::DatasetPointerType statismo::CStatismoModel::sample(const tCoefficients &coefficients) const
{
    assert(coefficients.size() > 0);

    // Use only possible number of coefficients
    size_t num_components(numCoefficients());
    size_t num_used(std::min<size_t>(num_components, coefficients.size()));
    statismo::VectorType cv(num_components, 1);

    

    // Copy coefficients to the statismo format vector
    for(size_t i = 0; i < num_used; ++i)
        cv(i, 0) = coefficients[i];

    // Add missing coefficients
    size_t last(coefficients.size());
    if(last < num_components)
        for(size_t i = last; i < num_components; ++i)
            cv(i, 0) = 0.0;

    // Try to draw sample
    return m_statismo->DrawSample(cv);
}

