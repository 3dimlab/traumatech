#include "StatisticModelCreator.h"
#include <VPL/System/FileBrowser.h>
// STL
#include <iostream>
#include <string>


//==============================================================================
/*
 * Global module constants.
 */

//! Module description
const std::string MODULE_DESCRIPTION    = "Module loads all (by default stl) files in the input directory and writes statistic model to the output file.";

// Additional command line arguments
const std::string MODULE_ARGUMENTS      = "id:of:mask";

const std::string MODULE_ARG_INPUT_DIRECTORY    = "id";
const std::string MODULE_ARG_OUTPUT_FILE		= "of";
const std::string MODULE_ARG_FILE_MASK          = "mask";


//==============================================================================
/*
 * Implementation of the class CRegistration3D.
 */
CStatisticModelCreator::CStatisticModelCreator(const std::string& sDescription)
    : vpl::mod::CModule(sDescription)
    , m_ssIDir("")
    , m_ssOFile("")
    , m_ssFileMask("*.stl")
{
    allowArguments(MODULE_ARGUMENTS);
}


CStatisticModelCreator::~CStatisticModelCreator()
{
}


bool CStatisticModelCreator::startup()
{
    // Note
    VPL_LOG_INFO("Module startup");


    // Input directory and files mask
    if( !m_Arguments.exists(MODULE_ARG_INPUT_DIRECTORY) )
    {
        VPL_LOG_ERROR('<' << m_sFilename << "> Input directory was not specified: type -h for help" << std::endl);
        return false;
    }
    m_Arguments.value(MODULE_ARG_INPUT_DIRECTORY, m_ssIDir);
    m_Arguments.value(MODULE_ARG_FILE_MASK, m_ssFileMask);

    // Output file name
    if( !m_Arguments.exists(MODULE_ARG_OUTPUT_FILE) )
    {
        VPL_LOG_ERROR('<' << m_sFilename << "> Output file name was not specified: type -h for help" << std::endl);
        return false;
    }
    m_Arguments.value(MODULE_ARG_OUTPUT_FILE, m_ssOFile);

    // all O.K.
    return true;
}


bool CStatisticModelCreator::main()
{
    // Note
    VPL_LOG_INFO("Module main function");

    // Try to load models
    OpenMesh::IO::Options ropt;
    ropt += OpenMesh::IO::Options::Binary;

    // Initialize the file browser
    vpl::sys::CFileBrowserPtr spFileBrowser;

    // Get the current directory
    std::string CWD = spFileBrowser->getDirectory();

    // Change the current directory to input
    if( !spFileBrowser->setDirectory(m_ssIDir) )
    {
        VPL_LOG_ERROR('<' << m_sFilename << "> Wrong input directory (" << m_ssIDir << "): type -h for help" << std::endl);
        return false;
    }

    // Load all input files in the directory
    int iCount = 0;
    vpl::sys::CFileBrowser::SFileAttr File;
    bool bResult = spFileBrowser->findFirst(m_ssFileMask, File);

    // Load all STL files in the directory
    for( ; bResult; bResult = spFileBrowser->findNext(File) )
    {
        CMeshPtr mesh(new CMesh);

        // Try to load mesh
        if (!OpenMesh::IO::read_mesh(*mesh, File.m_sName, ropt))
        {
            VPL_LOG_ERROR('<' << m_sFilename << "> Cannot load model file: " << File.m_sName << ". Wrong file name? " << std::endl);
        }
        else
        {
            VPL_LOG_INFO("Loaded " << File.m_sName);

            m_builder.addModel(*mesh);
        }

    }

    // Set back to current 
    spFileBrowser->setDirectory(CWD);

    // Try to create model
    statismo::CStatismoModelPtr model(m_builder.buildModel());

    if(model.get() == 0)
    {
        VPL_LOG_ERROR('<' << m_sFilename << "> Failed to create statistical model." << std::endl);
    }

    model->Save(m_ssOFile);

    // Returning 'true' means to continue processing the input channel
    return false;
}


void CStatisticModelCreator::shutdown()
{
    // Note
    VPL_LOG_INFO("Module shutdown");
}


void CStatisticModelCreator::writeExtendedUsage(std::ostream& Stream)
{
    Stream << "Extended usage: [-format ssType]" << std::endl;
    Stream << "Options:" << std::endl;
    
    Stream << " -" << MODULE_ARG_INPUT_DIRECTORY << " - input directory where to search for input files." << std::endl;
    Stream << " -" << MODULE_ARG_OUTPUT_FILE     << " - output file name." << std::endl;
    Stream << " -" << MODULE_ARG_FILE_MASK       << " - mask of input files. Default is *.stl" << std::endl;
    Stream << std::endl;
}



//==============================================================================
/*
 * Function main() which creates and executes console application.
 */
int main(int argc, char *argv[])
{
    // Creation of a module using smart pointer
    CStatisticModelCreatorPtr spModule(new CStatisticModelCreator(MODULE_DESCRIPTION));

    // Initialize and execute the module
    if( spModule->init(argc, argv) )
    {
        spModule->run();
    }

    // Console application finished
    return 0;
}

