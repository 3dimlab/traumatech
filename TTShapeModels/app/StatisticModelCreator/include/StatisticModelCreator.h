#ifndef StatisticModelCreatorApp_H_included
#define StatisticModelCreatorApp_H_included

#include <VPL/Module/Module.h>
#include <base/CModel.h>
#include "StatismoTools/ModelBuilder.h"
#include <VPL/Math/TransformMatrix.h>

//==============================================================================
/*!
 * Module loads PNG image and converts it to the MDSTk format.
 */
class CStatisticModelCreator : public vpl::mod::CModule
{
public:
    //! Smart pointer type
    //! - Declares type tSmartPtr
    VPL_SHAREDPTR(CStatisticModelCreator);


public:
    //! Default constructor
    CStatisticModelCreator(const std::string& sDescription);

    //! Virtual destructor
    virtual ~CStatisticModelCreator();

protected:
    //! Virtual method called on startup
    virtual bool startup();

    //! Virtual method called by the processing thread
    virtual bool main();

    //! Called on console shutdown
    virtual void shutdown();

    //! Called on writing a usage statement
    virtual void writeExtendedUsage(std::ostream& Stream);
   

protected:
	//! Input directory
	std::string m_ssIDir;

    //! Output file name
    std::string m_ssOFile;

    //! Input file mask
    std::string m_ssFileMask;

    //! Statismo model builder
    statismo::CModelBuilder m_builder;
};


//==============================================================================
/*!
 * Smart pointer to console application.
 */
typedef CStatisticModelCreator::tSmartPtr     CStatisticModelCreatorPtr;


#endif // StatisticModelCreatorApp_H_included

