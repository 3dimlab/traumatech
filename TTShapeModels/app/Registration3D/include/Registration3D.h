///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#ifndef Registration3DApp_H_included
#define Registration3DApp_H_included

#include <VPL/Module/Module.h>
#include <base/CModel.h>

#include <VPL/Math/TransformMatrix.h>

//==============================================================================
/*!
 * Module loads PNG image and converts it to the MDSTk format.
 */
class CRegistration3D : public vpl::mod::CModule
{
public:
    //! Smart pointer type
    //! - Declares type tSmartPtr
    VPL_SHAREDPTR(CRegistration3D);

    //! Transform matrix type
    typedef vpl::math::CTransformMatrix<double> tTransform;

public:
    //! Default constructor
    CRegistration3D(const std::string& sDescription);

    //! Virtual destructor
    virtual ~CRegistration3D();

protected:
    //! Virtual method called on startup
    virtual bool startup();

    //! Virtual method called by the processing thread
    virtual bool main();

    //! Called on console shutdown
    virtual void shutdown();

    //! Called on writing a usage statement
    virtual void writeExtendedUsage(std::ostream& Stream);

    //! Apply ICP algorithm
    bool applyICP();

    //! Write transform to the std output
    void writeTransform();

protected:
	//! Input file names
	std::string m_ssStaticModel, m_ssInputModel, m_ssOutputModel;

	//! Meshes
	CMeshPtr m_modelStatic, m_model;

    //! Should scale transform be used?
    bool m_bUseScale;

    //! Resulting transform
    tTransform m_transform;

    //! Use binary stl as output file format
    bool m_bUseBinarySTL;
};


//==============================================================================
/*!
 * Smart pointer to console application.
 */
typedef CRegistration3D::tSmartPtr     CRegistration3DPtr;


#endif // Registration3DApp_H_included

