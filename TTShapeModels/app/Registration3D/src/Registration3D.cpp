///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#include "Registration3D.h"
#include <alg/icp.h>
// STL
#include <iostream>
#include <string>


//==============================================================================
/*
 * Global module constants.
 */

//! Module description
const std::string MODULE_DESCRIPTION    = "Module loads two STL model files and tries to register second on first. Output is second model with applied transform.";

// Additional command line arguments
const std::string MODULE_ARGUMENTS      = "sm:im:om:scale:format";

const std::string MODULE_ARG_STATIC		= "sm";
const std::string MODULE_ARG_INPUT		= "im";
const std::string MODULE_ARG_OUTPUT		= "om";
const std::string MODULE_ARG_SCALE      = "scale";
const std::string MODULE_ARG_FORMAT     = "format";


//==============================================================================
/*
 * Implementation of the class CRegistration3D.
 */
CRegistration3D::CRegistration3D(const std::string& sDescription)
    : vpl::mod::CModule(sDescription)
    , m_ssStaticModel("")
    , m_ssInputModel("")
    , m_bUseScale(false)
    , m_bUseBinarySTL(true)
{
    allowArguments(MODULE_ARGUMENTS);
}


CRegistration3D::~CRegistration3D()
{
}


bool CRegistration3D::startup()
{
    // Note
    VPL_LOG_INFO("Module startup");

    // Test of existence of input and output channel
//     if( getNumOfInputs() != 0 || getNumOfOutputs() != 1 )
//     {
//         VPL_LOG_ERROR('<' << m_sFilename << "> Wrong number of input and output channels" << std::endl);
//         return false;
//     }

    // Input file names
    m_Arguments.value(MODULE_ARG_STATIC, m_ssStaticModel);
    m_Arguments.value(MODULE_ARG_INPUT, m_ssInputModel);

    // Output file name
    m_Arguments.value(MODULE_ARG_OUTPUT, m_ssOutputModel);

    // Use scale?
    std::string ssUseScale("");
    m_Arguments.value(MODULE_ARG_SCALE, ssUseScale);
    std::transform(ssUseScale.begin(), ssUseScale.end(), ssUseScale.begin(), ::tolower);
    m_bUseScale = ssUseScale.compare("true") == 0;

    // Use binary format
    std::string ssFormat("binary");
    m_Arguments.value(MODULE_ARG_FORMAT, ssFormat);
    std::transform(ssFormat.begin(), ssFormat.end(), ssFormat.begin(), ::tolower);
    m_bUseBinarySTL = ssFormat.compare("ascii") != 0;

    // O.K.
    return true;
}


bool CRegistration3D::main()
{
    // Note
    VPL_LOG_INFO("Module main function");

    // Try to load models
    OpenMesh::IO::Options ropt;
    ropt += OpenMesh::IO::Options::Binary;

    m_modelStatic = new CMesh;
    if (!OpenMesh::IO::read_mesh(*m_modelStatic, m_ssStaticModel, ropt))
    {
        VPL_LOG_ERROR('<' << m_sFilename << "> Cannot load model file: " << m_ssStaticModel << ". Wrong file name? " << std::endl);
    }

    m_model = new CMesh;
    if (!OpenMesh::IO::read_mesh(*m_model, m_ssInputModel, ropt))
    {
        VPL_LOG_ERROR('<' << m_sFilename << "> Cannot load model file: " << m_ssInputModel << ". Wrong file name? " << std::endl);
    }

    // Apply icp solver
    applyICP();

    // Write found transform
    writeTransform();

    // If output model shoud be written
    if(m_ssOutputModel.length() > 0)
    {
        // Apply transform on mesh
        m_model->transform(m_transform.asEigen());
     
        OpenMesh::IO::Options wopt;
        if(m_bUseBinarySTL)
            wopt += OpenMesh::IO::Options::Binary;

        // Write file
        if (!OpenMesh::IO::write_mesh(*m_model, m_ssOutputModel, wopt))
        {
            VPL_LOG_ERROR('<' << m_sFilename << "> Cannot write model file: " << m_ssOutputModel << std::endl);
        }
    }

    // Returning 'true' means to continue processing the input channel
    return false;
}


void CRegistration3D::shutdown()
{
    // Note
    VPL_LOG_INFO("Module shutdown");
}


void CRegistration3D::writeExtendedUsage(std::ostream& Stream)
{
    Stream << "Module applies Iterative Closest Point (ICP) algorithm on one model to register (align) it on the second (static) one. Used transformation can contain scale (default is not to use it)." << std::endl;
    Stream << "Options:" << std::endl;
    Stream << " -" << MODULE_ARG_STATIC << " - static model file name. Dynamic model is aligned to static model." << std::endl;
    Stream << " -" << MODULE_ARG_INPUT  << " - dynamic model file name." << std::endl;
    Stream << " -" << MODULE_ARG_OUTPUT << " - output model file name." << std::endl;
    Stream << " -" << MODULE_ARG_SCALE  << " - should scale be used to register models? Possible values: true/false. False is default." << std::endl;
    Stream << " -" << MODULE_ARG_FORMAT << " - output file format. Possible values: binary/ascii. Binary is default." << std::endl;
}

/**
 * \brief   Applies the ICP algorithm to register models.
 *
 * \return  true if it succeeds, false if it fails.
 */

bool CRegistration3D::applyICP()
{
    // Create ICP solver
//    CPointPointICP icp_solver;
//    CPointPlaneICP icp_solver;
    CPlanePlaneICP icp_solver;

    // Should scale be used?
    icp_solver.setComputeScale(m_bUseScale);
    icp_solver.setMinimalChangeThreshold(0.005);

    bool rv(icp_solver.apply(m_modelStatic, m_model, 100));
    if(rv)
        m_transform = icp_solver.getTransform();

    return rv;
}

/**
 * \brief   Writes the transform.
 */
void CRegistration3D::writeTransform()
{
    std::cout << "Found transform: " << std::endl;
    for( int i = 0; i < 4; ++i)
    {
        for( int j = 0; j < 3; ++j)
        {
            std::cout << m_transform(i, j) << ", ";
        }
        std::cout << m_transform(i, 3) << std::endl;
    }
}


//==============================================================================
/*
 * Function main() which creates and executes console application.
 */
int main(int argc, char *argv[])
{
    // Creation of a module using smart pointer
    CRegistration3DPtr spModule(new CRegistration3D(MODULE_DESCRIPTION));

    // Initialize and execute the module
    if( spModule->init(argc, argv) )
    {
        spModule->run();
    }

    // Console application finished
    return 0;
}

