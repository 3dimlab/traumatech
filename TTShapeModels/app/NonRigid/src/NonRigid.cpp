///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#include "NonRigid.h"
#include <alg/icp.h>
#include <alg/ClosestPointCorrespondenceSolver.h>
#include <alg/NormalProjectionCorrespondenceSolver.h>
#include <alg/CTransformInterpolator.h>
#include <alg/CNonRigidTransform.h>

// STL
#include <iostream>
#include <string>
#include <deque>


//==============================================================================
/*
 * Global module constants.
 */

//! Module description
const std::string MODULE_DESCRIPTION    = "Module loads two model files and tries to transform input model to achieve the same shape as static model has.";

// Additional command line arguments
const std::string MODULE_ARGUMENTS      = "sm:im:om:scale:format:transform:removeinternals";

const std::string MODULE_ARG_STATIC		= "sm";
const std::string MODULE_ARG_INPUT		= "im";
const std::string MODULE_ARG_OUTPUT		= "om";
const std::string MODULE_ARG_SCALE      = "scale";
const std::string MODULE_ARG_FORMAT     = "format";
const std::string MODULE_ARG_TRANSFORM  = "transform";
const std::string MODULE_ARG_INTERNALS  = "removeinternals";

const size_t NUM_OF_PARTS = 5;

//==============================================================================
/*
 * Implementation of the class CRegistration3D.
 */
CNonRigidTestApp::CNonRigidTestApp(const std::string& sDescription)
    : vpl::mod::CModule(sDescription)
    , m_ssStaticModel("")
    , m_ssSourceModel("")
    , m_bUseScale(false)
    , m_bRemoveInternals(true)
    , m_bUseBinarySTL(true)
    , m_bUseICP(true)
{
    allowArguments(MODULE_ARGUMENTS);
    m_transformScaled.unit();
    m_transformSimple.unit();
}


CNonRigidTestApp::~CNonRigidTestApp()
{
}


bool CNonRigidTestApp::startup()
{
    // Note
    VPL_LOG_INFO("Module startup");

    // Input file names
    m_Arguments.value(MODULE_ARG_STATIC, m_ssStaticModel);
    m_Arguments.value(MODULE_ARG_INPUT, m_ssSourceModel);

    // Output file name
    m_Arguments.value(MODULE_ARG_OUTPUT, m_ssOutputModel);

    // Use scale?
    std::string ssUseScale("false");
    m_Arguments.value(MODULE_ARG_SCALE, ssUseScale);
    std::transform(ssUseScale.begin(), ssUseScale.end(), ssUseScale.begin(), ::tolower);
    m_bUseScale = ssUseScale.compare("true") == 0;

    // Remove internal points?
    std::string ssRemoveInternals("true");
    m_Arguments.value(MODULE_ARG_INTERNALS, ssRemoveInternals);
    std::transform(ssRemoveInternals.begin(), ssRemoveInternals.end(), ssRemoveInternals.begin(), ::tolower);
    m_bRemoveInternals = ssRemoveInternals.compare("true") == 0;

    // Use binary format
    std::string ssFormat("binary");
    m_Arguments.value(MODULE_ARG_FORMAT, ssFormat);
    std::transform(ssFormat.begin(), ssFormat.end(), ssFormat.begin(), ::tolower);
    m_bUseBinarySTL = ssFormat.compare("ascii") != 0;

    // Apply alignment transform on output model
    std::string ssUseTransform("");
    m_Arguments.value(MODULE_ARG_TRANSFORM, ssUseTransform);
    std::transform(ssUseTransform.begin(), ssUseTransform.end(), ssUseTransform.begin(), ::tolower);
    m_bUseICP = !ssUseTransform.compare("false") == 0;

     // O.K.
    return true;
}


bool CNonRigidTestApp::main()
{
    // Note
    VPL_LOG_INFO("Module main function");

    // Try to load models
    OpenMesh::IO::Options ropt;
    ropt += OpenMesh::IO::Options::Binary;

    m_modelTarget = new CMesh;
    if (!OpenMesh::IO::read_mesh(*m_modelTarget, m_ssStaticModel, ropt))
    {
        VPL_LOG_ERROR('<' << m_sFilename << "> Cannot load model file: " << m_ssStaticModel << ". Wrong file name? " << std::endl);
        return false;
    }

    if(m_bRemoveInternals)
    {
        size_t num_removed(removeInternals(*m_modelTarget));
        std::cout << "Simplifying target model. Number of points removed: " << num_removed << std::endl;
    }

//    std::cout << "NV Target: " << m_modelTarget->n_vertices() << std::endl;

    m_modelSource = new CMesh;
    if (!OpenMesh::IO::read_mesh(*m_modelSource, m_ssSourceModel, ropt))
    {
        VPL_LOG_ERROR('<' << m_sFilename << "> Cannot load model file: " << m_ssSourceModel << ". Wrong file name? " << std::endl);
        return false;
    }

    if(m_bRemoveInternals)
    {
        size_t num_removed(removeInternals(*m_modelSource));
        std::cout << "Simplifying source model. Number of points removed: " << num_removed << std::endl;
    }

    m_modelResult = new CMesh;

//    std::cout << "NV Source: " << m_modelSource->n_vertices() << std::endl;

    // Apply icp solver
    if(m_bUseICP && (!applyICP()))
    {
        VPL_LOG_ERROR('<' << m_sFilename << "> Cannot apply ICP algorithm." << std::endl);
        return false;
    }

    // Compute scaled source model
    m_modelScaledSource = new CMesh(*m_modelSource);
    m_modelScaledSource->transform(m_transformScaled);

    // Save initial models
//     m_modelSource->save("d:/_source.stl");
//     m_modelScaledSource->save("d:/_scaled_source.stl");
//     m_modelTarget->save("d:/_target.stl");

    // Try to compute partial transforms
    CNonRigidSimple transformations(NUM_OF_PARTS);

    // Copy target model
    m_modelResult = new CMesh(*m_modelScaledSource);

    if(!transformations.solve(*m_modelTarget, *m_modelScaledSource))
    {
        VPL_LOG_ERROR('<' << m_sFilename << "> Cannot apply partial ICP algorithm." << std::endl);
        return false;
    }
    
//    storeMesh(*m_modelScaledSource, "d:/scaled_source.stl");
    
    if(!transformations.applyOnMesh(*m_modelResult))
    {
        VPL_LOG_ERROR('<' << m_sFilename << "> Cannot apply found partial ICP transforms on model." << std::endl);
        return false;
    }
 
//    m_modelResult->save("d:/_bended.stl");

    // Apply correspondence search algorithm
   if(!applyCorrespondences())
    {
        VPL_LOG_ERROR('<' << m_sFilename << "> Cannot found correspondences." << std::endl);
        return false;
    }

    // Write found transform
    writeTransform();

//    m_modelResult->save("d:/_result.stl");

    // If output model shoud be written
    if(m_ssOutputModel.length() > 0 && m_modelResult->n_vertices() > 0)
    {
        m_modelResult->transform(m_transformScaled);
//        m_modelSource->transform(m_transformScaled);

        OpenMesh::IO::Options wopt;
        if(m_bUseBinarySTL)
            wopt += OpenMesh::IO::Options::Binary;

//        std::cout << "NV Result: " << m_modelResult->n_vertices() << std::endl;

        // Write file
        if (!OpenMesh::IO::write_mesh(*m_modelResult, m_ssOutputModel, wopt))
        {
            VPL_LOG_ERROR('<' << m_sFilename << "> Cannot write model file: " << m_ssOutputModel << std::endl);
            return false;
        }
    }
//*/

    // Returning 'true' means to continue processing the input channel
    return false;
}


void CNonRigidTestApp::shutdown()
{
    // Note
    VPL_LOG_INFO("Module shutdown");
}


void CNonRigidTestApp::writeExtendedUsage(std::ostream& Stream)
{
    Stream << "Extended usage: -sm static_model_file -im input_model_file -om output_model_file [-scale true/false] [-format] [-transform]" << std::endl;
    Stream << "Two models are loaded - static (target) model and input (source) model. Module tries to find correspondences - convert input model to the same topology as target model" << std::endl;
    Stream << "but with the shape of the input (source) model. The result is stored to the output model file." << std::endl;
    Stream << "Options:" << std::endl;
    Stream << "-sm Static model file name - model is used as target for transformations." << std::endl;
    Stream << "-im Input model file name - model is transformed " << std::endl;
    Stream << "-om Output model file name" << std::endl;
    Stream << "-transform true/false - should be ICP algorithm used to fit input model on static model before correspondence solving? True is default." << std::endl;
    Stream << "-scale true/false - should ICP algorithm use scale transformation? False is default." << std::endl;
    Stream << "-format ascii/binary - output file format" << std::endl;
    Stream << "-removeinternals true/false- should be input and static model be simplified by removing internal structures? True is default." << std::endl;
}

/**
 * \brief   Applies the ICP algorithm to register models.
 *
 * \return  true if it succeeds, false if it fails.
 */

bool CNonRigidTestApp::applyICP()
{


    // Create ICP solver
//    CPointPointICP icp_solver;
//    CPointPlaneICP icp_solver;
    CPlanePlaneICP icp_solver;

    // Find transform
    icp_solver.setComputeScale(true);
    icp_solver.setMinimalChangeThreshold(0.005);
    bool rv(icp_solver.apply(m_modelTarget, m_modelSource, 100));

    // Find scaled transform if possible
    if(rv)
    {
        m_transformScaled = icp_solver.getTransform();

        // m_transformSimple = computeUnscaled(m_transformScaled);
        
        icp_solver.setComputeScale(false);
        icp_solver.setMinimalChangeThreshold(0.04);
        icp_solver.apply(m_modelTarget, m_modelSource, 100);
        m_transformSimple = icp_solver.getTransform();
    }

    return rv;
}

/**
 * \brief   Writes the transform.
 */
void CNonRigidTestApp::writeTransform()
{
    std::cout << "Found transform scaled: " << std::endl;
    for( int i = 0; i < 4; ++i)
    {
        for( int j = 0; j < 3; ++j)
        {
            std::cout << m_transformScaled(i, j) << ", ";
        }
        std::cout << m_transformScaled(i, 3) << std::endl;
    }

    std::cout << std::endl << "Found transform unscaled: " << std::endl;
    for( int i = 0; i < 4; ++i)
    {
        for( int j = 0; j < 3; ++j)
        {
            std::cout << m_transformSimple(i, j) << ", ";
        }
        std::cout << m_transformSimple(i, 3) << std::endl;
    }

}

/**
 * \brief   Applies the correspondences transform.
 *
 * \return  true if it succeeds, false if it fails.
 */
bool CNonRigidTestApp::applyCorrespondences()
{
    CNormalProjectionCorrespondenceSolver correspondences_solver;

    m_modelResult = correspondences_solver.apply(m_modelTarget, m_modelSource, m_transformScaled, m_transformSimple);

    std::cout << "Number of source points: " << m_modelSource->n_vertices() << std::endl;
    std::cout << "Number of target points: " << m_modelTarget->n_vertices() << std::endl;
    std::cout << "Number of result points: " << m_modelResult->n_vertices() << std::endl;

    return m_modelResult != 0 && m_modelResult->n_vertices() > 0;
}

/**
 * \brief   Calculates the unscaled transformation matrix.
 *
 * \param   tm  The time.
 *
 * \return  The calculated unscaled.
 */
CNonRigidTestApp::tTransform CNonRigidTestApp::computeUnscaled(const tTransform &tm)
{
    Eigen::Transform<double, 3, Eigen::Affine> transform(tm.asEigen());
    Eigen::Matrix3d rm, sm;
    transform.computeRotationScaling(&rm, &sm);

    // Get translation vector
    vpl::math::CDVector3 tv(tm.getTrans<double>());

    // Finalize matrix
    tTransform om;
    om.asEigen().topLeftCorner(3, 3) = rm;
    return om * tTransform::translate(tv);
}

/**
 * \brief   Removes the internals described by mesh.
 *
 * \param [in,out]  mesh    If non-null, the mesh.
 *
 * \return  .
 */
size_t CNonRigidTestApp::removeInternals(CMesh &mesh)
{
    size_t num_removed(0);

    // queue of triangles to visit
    std::deque<CMesh::FaceHandle> toVisit;

    // submesh index map with number of submeshes
    std::map<int, int> counts;

    // last used submesh index
    int lastused = 0;

    int maxGroupIndex = -1;

    // add property and clear it
    OpenMesh::FPropHandleT<int> fProp_submeshIndex;
    mesh.add_property<int>(fProp_submeshIndex, "submeshIndex");

    for (CMesh::FaceIter fit = mesh.faces_begin(); fit != mesh.faces_end(); ++fit)
    {
        mesh.property<int>(fProp_submeshIndex, fit) = -1;
    }

    // get first triangle of the mesh
    CMesh::FaceIter faceIt = mesh.faces_begin();
    while (faceIt != mesh.faces_end())
    {
        CMesh::FaceHandle face = faceIt.handle();

        // start new submesh index
        counts[lastused] = 0;
        mesh.property<int>(fProp_submeshIndex, face) = lastused;

        // place the triangle into the visiting queue
        toVisit.push_back(face);

        // go through all triangles to visit ( that lie on the same submesh )
        while (toVisit.size() > 0)
        {
            CMesh::FaceHandle curr = toVisit.front();
            toVisit.pop_front();

            // get all neighbourhoods and place them in the queue
            for (CMesh::FaceFaceIter ffit = mesh.ff_begin(curr); ffit != mesh.ff_end(curr); ++ffit)
            {
                CMesh::FaceHandle neighbour = ffit.handle();
                if (mesh.property<int>(fProp_submeshIndex, neighbour) == -1)
                {
                    mesh.property<int>(fProp_submeshIndex, neighbour) = lastused;
                    toVisit.push_back(neighbour);
                    counts[lastused]++;
                }
            }
        }

        if ((maxGroupIndex == -1) || (counts[lastused] > counts[maxGroupIndex]))
        {
            maxGroupIndex = lastused;
        }

        // end of continuous area, find next one
        while ((faceIt != mesh.faces_end()) && (mesh.property<int>(fProp_submeshIndex, faceIt.handle()) != -1))
        {
            ++faceIt;
        }

        lastused++;
    }

    // erase tris of nonmax groups
    for (CMesh::FaceIter fit = mesh.faces_begin(); fit != mesh.faces_end(); ++fit)
    {
        if (mesh.property<int>(fProp_submeshIndex, fit.handle()) != maxGroupIndex)
        {
            mesh.delete_face(fit.handle());
            ++num_removed;
        }
    }

    // clean    
    mesh.remove_property(fProp_submeshIndex);
    mesh.garbage_collection();

    return num_removed;
}


//==============================================================================
/*
 * Function main() which creates and executes console application.
 */
int main(int argc, char *argv[])
{
    // Creation of a module using smart pointer
    CNonRigidTestPtr spModule(new CNonRigidTestApp(MODULE_DESCRIPTION));

    // Initialize and execute the module
    if( spModule->init(argc, argv) )
    {
        spModule->run();
    }

    // Console application finished
    return 0;
}

