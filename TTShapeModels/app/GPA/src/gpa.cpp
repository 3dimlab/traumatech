///////////////////////////////////////////////////////////////////////////////
// This file is part of
// 
// TraumaTech - Computer-based Planning in Trauma Treatment
// Copyright 2015 3Dim Laboratory s.r.o.
// All rights reserved.
// 
// This software was created with the support of TA CR (https://www.tacr.cz/)
// under project number TA04011606.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
///////////////////////////////////////////////////////////////////////////////

#include "gpa.h"
#include <VPL/System/FileBrowser.h>

#include <alg/CProcrustes.h>
// STL
#include <iostream>
#include <string>


//==============================================================================
/*
 * Global module constants.
 */

//! Module description
const std::string MODULE_DESCRIPTION    = "Module loads all stl files from the input directory and tries to apply generalised procrustes analysis on them.";

// Additional command line arguments
const std::string MODULE_ARGUMENTS      = "id:od:iterations";

const std::string MODULE_ARG_INPUT_DIRECTORY	= "id";
const std::string MODULE_ARG_OUTPUT_DIRECTORY	= "od";
const std::string MODULE_ARG_NUM_ITERATIONS	    = "iterations";


//==============================================================================
/*
 * Implementation of the class CRegistration3D.
 */
GPAApp::GPAApp(const std::string& sDescription)
    : vpl::mod::CModule(sDescription)
    , m_bUseBinarySTL(true)
    , m_numIterations(5)
{
    allowArguments(MODULE_ARGUMENTS);
}


GPAApp::~GPAApp()
{
}


bool GPAApp::startup()
{
    // Note
    VPL_LOG_INFO("Module startup");

    // Input directory
    if( !m_Arguments.exists(MODULE_ARG_INPUT_DIRECTORY) )
    {
        VPL_LOG_ERROR('<' << m_sFilename << "> Input directory was not specified: type -h for help" << std::endl);
        return false;
    }
    m_Arguments.value(MODULE_ARG_INPUT_DIRECTORY, m_ssIDir);

    // Output directory
    if( !m_Arguments.exists(MODULE_ARG_OUTPUT_DIRECTORY) )
    {
        VPL_LOG_ERROR('<' << m_sFilename << "> Output directory was not specified: type -h for help" << std::endl);
        return false;
    }
    m_Arguments.value(MODULE_ARG_OUTPUT_DIRECTORY, m_ssODir);

    // Number of iterations
    if( !m_Arguments.exists(MODULE_ARG_NUM_ITERATIONS) )
    {
        VPL_LOG_INFO('<' << m_sFilename << "> Number of iterations was not specified, default value was used: type -h for help" << std::endl);
    }
    else
    {
        int num;
        m_Arguments.value(MODULE_ARG_NUM_ITERATIONS, num);

        if(num < 1)
        {
            VPL_LOG_ERROR('<' << m_sFilename << "> Wrong number of iterations (" << num << "): type -h for help" << std::endl);
            return false;
        }

        m_numIterations = static_cast<size_t>(num);
    }

    // O.K.
    return true;
}


bool GPAApp::main()
{
    // Note
    VPL_LOG_INFO("Module main function");

    // Try to load models
    OpenMesh::IO::Options ropt;
    ropt += OpenMesh::IO::Options::Binary;

    // Initialize the file browser
    vpl::sys::CFileBrowserPtr spFileBrowser;

    // Get the current directory
    std::string CWD = spFileBrowser->getDirectory();

    // Change the current directory to output to test it
    if( !spFileBrowser->setDirectory(m_ssODir) )
    {
        VPL_LOG_ERROR('<' << m_sFilename << "> Wrong output directory (" << m_ssODir << "): type -h for help" << std::endl);
        return false;
    }

    // Set back to current 
    spFileBrowser->setDirectory(CWD);

    // Change the current directory to input
    if( !spFileBrowser->setDirectory(m_ssIDir) )
    {
        VPL_LOG_ERROR('<' << m_sFilename << "> Wrong input directory (" << m_ssIDir << "): type -h for help" << std::endl);
        return false;
    }

    // Load all input files in the directory
    int iCount = 0;
    vpl::sys::CFileBrowser::SFileAttr File;
    std::string fileMask("*.stl");
    bool bResult = spFileBrowser->findFirst(fileMask, File);

    // Load all STL files in the directory
    for( ; bResult; bResult = spFileBrowser->findNext(File) )
    {
        CMeshPtr mesh(new CMesh);

        // Try to load mesh
        if (!OpenMesh::IO::read_mesh(*mesh, File.m_sName, ropt))
        {
            VPL_LOG_ERROR('<' << m_sFilename << "> Cannot load model file: " << File.m_sName << ". Wrong file name? " << std::endl);
        }
        else
        {
            VPL_LOG_INFO("Loaded " << File.m_sName);

            // Store mesh
            m_meshes.push_back(mesh);

            // Store file name for further use
            m_ssFiles.push_back(File.m_sName);
        }

    }

    // Apply analysis
    apply();

    // Set back to current working directory
    spFileBrowser->setDirectory(CWD);

    // Change the current directory to input
    if( !spFileBrowser->setDirectory(m_ssODir) )
    {
        VPL_LOG_ERROR('<' << m_sFilename << "> Wrong output directory (" << m_ssODir << "): type -h for help" << std::endl);
        return false;
    }

    // For all file names
    std::vector<std::string>::const_iterator itn(m_ssFiles.begin()), itnEnd(m_ssFiles.end());
    tMeshVec::iterator itm(m_meshes.begin());
    for(; itn != itnEnd; ++itn, ++itm)
    {
        OpenMesh::IO::Options wopt;
        if(m_bUseBinarySTL)
            wopt += OpenMesh::IO::Options::Binary;

        // Write file
        if (!OpenMesh::IO::write_mesh(**itm, *itn, wopt))
        {
            VPL_LOG_ERROR('<' << m_sFilename << "> Cannot write model file: " << *itn << std::endl);
        }
    }

    // Returning 'true' means to continue processing the input channel
    return false;
}


void GPAApp::shutdown()
{
    // Note
    VPL_LOG_INFO("Module shutdown");
}


void GPAApp::writeExtendedUsage(std::ostream& Stream)
{
    Stream << " -" << MODULE_ARG_INPUT_DIRECTORY    << " - input directory." << std::endl;
    Stream << " -" << MODULE_ARG_OUTPUT_DIRECTORY   << " - output directory." << std::endl;
    Stream << " -" << MODULE_ARG_NUM_ITERATIONS     << " - number of iterations used. In each iteration medium model is used to compute alignment and then new medium model is computed from the aligned models. Default value is 5." << std::endl;
}

/**
 * \brief   Applies the ICP algorithm to register models.
 *
 * \return  true if it succeeds, false if it fails.
 */

bool GPAApp::apply()
{
    CProcrustes3D procrustes_analyzer;

    // Add all models
    tMeshVec::const_iterator mit(m_meshes.begin()), mitEnd(m_meshes.end());
    for(; mit != mitEnd; ++mit)
    {
        procrustes_analyzer.addSource(**mit);
    }

    // Try to apply analysis
    if(!procrustes_analyzer.apply(m_numIterations))
        return false;

    // Get modified models
    size_t counter(0);
    for(mit = m_meshes.begin(); mit != mitEnd; ++mit, ++counter)
    {
        procrustes_analyzer.getMesh(**mit, counter);
    }

    return true;
}



//==============================================================================
/*
 * Function main() which creates and executes console application.
 */
int main(int argc, char *argv[])
{
    // Creation of a module using smart pointer
    CGPAPtr spModule(new GPAApp(MODULE_DESCRIPTION));

    // Initialize and execute the module
    if( spModule->init(argc, argv) )
    {
        spModule->run();
    }

    // Console application finished
    return 0;
}

