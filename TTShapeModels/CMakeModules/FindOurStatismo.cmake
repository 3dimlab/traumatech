#///////////////////////////////////////////////////////////////////////////////
#// This file comes from Kitware library and was modified for 
#// 
#// TraumaTech - Computer-based Planning in Trauma Treatment
#// 
#// The original license can be found below.
#// 
#// Changes are Copyright 2015 3Dim Laboratory s.r.o.
#// All rights reserved.
#// 
#// This software was created with the support of TA CR (https://www.tacr.cz/)
#// under project number TA04011606.
#// 
#// Licensed under the Apache License, Version 2.0 (the "License");
#// you may not use this file except in compliance with the License.
#// You may obtain a copy of the License at
#//
#//   http://www.apache.org/licenses/LICENSE-2.0
#// 
#// Unless required by applicable law or agreed to in writing, software
#// distributed under the License is distributed on an "AS IS" BASIS,
#// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#// See the License for the specific language governing permissions and
#// limitations under the License.
#// 
#// Changelog:
#//    [2015/12] - Added license header for Traumatech
#//
#///////////////////////////////////////////////////////////////////////////////

# - Find Statismo
# Find the native Statismo includes and library
#
#  STATISMO_INCLUDE_DIRS   - where to find carve.h, etc.
#  STATISMO_LIBRARIES      - List of libraries when using Carve.
#  STATISMO_FOUND          - True if Carve found.

#=============================================================================
# Copyright 2001-2009 Kitware, Inc.
#
# Distributed under the OSI-approved BSD License (the "License");
# see accompanying file Copyright.txt for details.
#
# This software is distributed WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the License for more information.
#=============================================================================
# (To distributed this file outside of CMake, substitute the full
#  License text for the above reference.)
#
# Modified for Traumatech by Vit Stancl
#

# Initialize the search path
SET( STATISMO_DIR_SEARCH "" ) 

# User defined environment variable STATISMO_ROOT_DIR
IF( EXISTS $ENV{STATISMO_ROOT_DIR} )
  FILE( TO_CMAKE_PATH $ENV{STATISMO_ROOT_DIR} STATISMO_DIR_SEARCH)
  SET( STATISMO_ROOT_DIR ${STATISMO_ROOT_DIR} ${STATISMO_DIR_SEARCH}/include )
ENDIF( EXISTS $ENV{STATISMO_ROOT_DIR} )

# User defined CMake cache variable
IF( STATISMO_ROOT_DIR )
  SET( STATISMO_DIR_SEARCH ${STATISMO_DIR_SEARCH} ${STATISMO_ROOT_DIR} ${STATISMO_ROOT_DIR}/include )
ENDIF( STATISMO_ROOT_DIR )

set( STATISMO_INC_SEARCHSUFFIXES
       STATISMO
       core
       statismo/core
       include 
       include/core
       include/statismo/core
       Statismo/include/statismo/core
       )
       
FIND_PATH(STATISMO_INCLUDE_DIR
    NAMES StatisticalModel.h
    PATHS
    ${STATISMO_DIR_SEARCH}
    "${TRIDIM_MOOQ_DIR}"
    "${TRIDIM_3RDPARTY_DIR}"
    PATH_SUFFIXES ${STATISMO_INC_SEARCHSUFFIXES}
)

SET(STATISMO_DEBUG_NAMES statismo_core-d)
FIND_LIBRARY(STATISMO_DEBUG_LIBRARY
    NAMES
        ${STATISMO_DEBUG_NAMES}
    PATHS
      ${STATISMO_DIR_SEARCH}
      "${TRIDIM_MOOQ_DIR}/lib"
      "${TRIDIM_MOOQ_DIR}/Statismo/lib"
      "${TRIDIM_3RDPARTY_DIR}/lib"
      "${TRIDIM_3RDPARTY_DIR}/Statismo/lib"
)

SET(STATISMO_NAMES statismo_core)
FIND_LIBRARY(STATISMO_LIBRARY
    NAMES
        ${STATISMO_NAMES}
    PATHS
      ${STATISMO_DIR_SEARCH}
      "${TRIDIM_MOOQ_DIR}/lib"
      "${TRIDIM_MOOQ_DIR}/Statismo/lib"
      "${TRIDIM_3RDPARTY_DIR}/lib"
      "${TRIDIM_3RDPARTY_DIR}/Statismo/lib"
)

if( CMAKE_CONFIGURATION_TYPES OR CMAKE_BUILD_TYPE )
  set( STATISMO_LIBRARY
    optimized ${STATISMO_LIBRARY}
    debug ${STATISMO_DEBUG_LIBRARY}
    )
endif()  

MARK_AS_ADVANCED(STATISMO_LIBRARY STATISMO_INCLUDE_DIR)

# handle the QUIETLY and REQUIRED arguments and set STATISMO_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(STATISMO DEFAULT_MSG STATISMO_INCLUDE_DIR STATISMO_LIBRARY)

IF (STATISMO_FOUND)
    SET(STATISMO_INCLUDE_DIRS ${STATISMO_INCLUDE_DIR})
    SET(STATISMO_LIBRARIES    ${STATISMO_LIBRARY})
ENDIF()

IF( NOT STATISMO_FOUND )
  SET( STATISMO_ROOT_DIR "" CACHE PATH "" )
ENDIF( NOT STATISMO_FOUND ) 