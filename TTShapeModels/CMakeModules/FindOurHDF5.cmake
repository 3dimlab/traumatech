#///////////////////////////////////////////////////////////////////////////////
#// This file comes from Kitware library and was modified for 
#// 
#// TraumaTech - Computer-based Planning in Trauma Treatment
#// 
#// The original license can be found below.
#// 
#// Changes are Copyright 2015 3Dim Laboratory s.r.o.
#// All rights reserved.
#// 
#// This software was created with the support of TA CR (https://www.tacr.cz/)
#// under project number TA04011606.
#// 
#// Licensed under the Apache License, Version 2.0 (the "License");
#// you may not use this file except in compliance with the License.
#// You may obtain a copy of the License at
#//
#//   http://www.apache.org/licenses/LICENSE-2.0
#// 
#// Unless required by applicable law or agreed to in writing, software
#// distributed under the License is distributed on an "AS IS" BASIS,
#// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#// See the License for the specific language governing permissions and
#// limitations under the License.
#// 
#// Changelog:
#//    [2015/12] - Added license header for Traumatech
#//
#///////////////////////////////////////////////////////////////////////////////

# - Find HDF5
# Find the native HDF5 includes and library
#
#  HDF5_INCLUDE_DIRS   - where to find carve.h, etc.
#  HDF5_LIBRARIES      - List of libraries when using Carve.
#  HDF5_FOUND          - True if Carve found.

#=============================================================================
# Copyright 2001-2009 Kitware, Inc.
#
# Distributed under the OSI-approved BSD License (the "License");
# see accompanying file Copyright.txt for details.
#
# This software is distributed WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the License for more information.
#=============================================================================
# (To distributed this file outside of CMake, substitute the full
#  License text for the above reference.)
#
# Modified for 3DimViewer by Vit Stancl
#

# Initialize the search path
SET( HDF5_DIR_SEARCH "" ) 

# User defined environment variable HDF5_ROOT_DIR
IF( EXISTS $ENV{HDF5_ROOT_DIR} )
  FILE( TO_CMAKE_PATH $ENV{HDF5_ROOT_DIR} HDF5_DIR_SEARCH)
  SET( HDF5_ROOT_DIR ${HDF5_ROOT_DIR} ${HDF5_DIR_SEARCH}/include )
ENDIF( EXISTS $ENV{HDF5_ROOT_DIR} )

# User defined CMake cache variable
IF( HDF5_ROOT_DIR )
  SET( HDF5_DIR_SEARCH ${HDF5_DIR_SEARCH} ${HDF5_ROOT_DIR} ${HDF5_ROOT_DIR}/include )
ENDIF( HDF5_ROOT_DIR )

FIND_PATH(HDF5_INCLUDE_DIR
    NAMES H5Cpp.h
    PATHS
    ${HDF5_DIR_SEARCH}
    "${TRIDIM_MOOQ_DIR}/include"
    "${TRIDIM_MOOQ_DIR}/HDF5/include"    
    "${TRIDIM_3RDPARTY_DIR}/include"
    "${TRIDIM_3RDPARTY_DIR}/HDF5/include"
)

SET(HDF5_DEBUG_NAMES libhdf5_cpp_D)
FIND_LIBRARY(HDF5_CPP_DEBUG_LIBRARY
    NAMES
        ${HDF5_DEBUG_NAMES}
    PATHS
      ${HDF5_DIR_SEARCH}
      "${TRIDIM_MOOQ_DIR}/lib"
      "${TRIDIM_MOOQ_DIR}/HDF5/lib"
      "${TRIDIM_3RDPARTY_DIR}/lib"
      "${TRIDIM_3RDPARTY_DIR}/HDF5/lib"
)

SET(HDF5_NAMES libhdf5_cpp)
FIND_LIBRARY(HDF5_CPP_RELEASE_LIBRARY
    NAMES
        ${HDF5_NAMES}
    PATHS
      ${HDF5_DIR_SEARCH}
      "${TRIDIM_MOOQ_DIR}/lib"
      "${TRIDIM_MOOQ_DIR}/HDF5/lib"
      "${TRIDIM_3RDPARTY_DIR}/lib"
      "${TRIDIM_3RDPARTY_DIR}/HDF5/lib"
)

SET(HDF5_DEBUG_NAMES libhdf5_D)
FIND_LIBRARY(HDF5_DEBUG_LIBRARY
    NAMES
        ${HDF5_DEBUG_NAMES}
    PATHS
      ${HDF5_DIR_SEARCH}
      "${TRIDIM_MOOQ_DIR}/lib"
      "${TRIDIM_MOOQ_DIR}/HDF5/lib"
      "${TRIDIM_3RDPARTY_DIR}/lib"
      "${TRIDIM_3RDPARTY_DIR}/HDF5/lib"
)

SET(HDF5_NAMES libhdf5)
FIND_LIBRARY(HDF5_RELEASE_LIBRARY
    NAMES
        ${HDF5_NAMES}
    PATHS
      ${HDF5_DIR_SEARCH}
      "${TRIDIM_MOOQ_DIR}/lib"
      "${TRIDIM_MOOQ_DIR}/HDF5/lib"
      "${TRIDIM_3RDPARTY_DIR}/lib"
      "${TRIDIM_3RDPARTY_DIR}/HDF5/lib"
)

if( CMAKE_CONFIGURATION_TYPES OR CMAKE_BUILD_TYPE )
  set( HDF5_LIBRARY
    optimized ${HDF5_RELEASE_LIBRARY} 
    optimized ${HDF5_CPP_RELEASE_LIBRARY}
    debug ${HDF5_DEBUG_LIBRARY} 
    debug ${HDF5_CPP_DEBUG_LIBRARY}
    )
endif()  

MARK_AS_ADVANCED(HDF5_LIBRARY HDF5_INCLUDE_DIR)

# handle the QUIETLY and REQUIRED arguments and set HDF5_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(HDF5 DEFAULT_MSG HDF5_INCLUDE_DIR HDF5_LIBRARY)

IF (HDF5_FOUND)
    SET(HDF5_INCLUDE_DIRS ${HDF5_INCLUDE_DIR})  
    SET(HDF5_LIBRARIES    ${HDF5_LIBRARY})
ENDIF()

IF( NOT HDF5_FOUND )
  SET( HDF5_ROOT_DIR "" CACHE PATH "" )
ENDIF( NOT HDF5_FOUND ) 