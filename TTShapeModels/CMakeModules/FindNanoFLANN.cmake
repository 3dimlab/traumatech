#/////////////////////////////////////////////////////////////////////////////
# This file is part of
# 
# TraumaTech - Computer-based Planning in Trauma Treatment
# Copyright 2015 3Dim Laboratory s.r.o.
# All rights reserved.
# 
# This software was created with the support of TA CR (https://www.tacr.cz/)
# under project number TA04011606.
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
#/////////////////////////////////////////////////////////////////////////////

# - Try to find Eigen3 lib
#
# This module supports requiring a minimum version, e.g. you can do
#   find_package(NanoFLANN 3.1.2)
# to require version 3.1.2 or newer of NanoFLANN.
#
# Once done this will define
#
#  NANOFLANN_FOUND - system has eigen lib with correct version
#  NANOFLANN_INCLUDE_DIR - the eigen include directory
#

# User defined environment variable OSG_ROOT_DIR
IF( EXISTS $ENV{NANOFLANN_ROOT_DIR} )
  FILE( TO_CMAKE_PATH $ENV{NANOFLANN_ROOT_DIR} NANOFLANN_DIR_SEARCH)
  SET( NANOFLANN_DIR_SEARCH ${NANOFLANN_DIR_SEARCH} ${NANOFLANN_DIR_SEARCH}/include )
ENDIF( EXISTS $ENV{NANOFLANN_ROOT_DIR} )

# User defined CMake cache variable
IF( NANOFLANN_ROOT_DIR )
  SET( NANOFLANN_DIR_SEARCH ${NANOFLANN_DIR_SEARCH} ${NANOFLANN_ROOT_DIR} ${NANOFLANN_ROOT_DIR}/include)
ENDIF( NANOFLANN_ROOT_DIR )

# Predefined search directories
if( WIN32 )
  set( NANOFLANN_INC_SEARCHPATH
       "${TRIDIM_3RDPARTY_DIR}"
       "${TRIDIM_MOOQ_DIR}"
       "${NANOFLANN_DIR_SEARCH}"
       )
  set( NANOFLANN_INC_SEARCHSUFFIXES
       NANOFLANN
       include 
       include/nanoflann 
       nanoflann/include
       )
else()
  set( NANOFLANN_INC_SEARCHPATH
       "${TRIDIM_3RDPARTY_DIR}/include"
       "${TRIDIM_MOOQ_DIR}"
       "${NANOFLANN_DIR_SEARCH}"
       /sw/include
       /usr/include
       /usr/local/include
       /opt/include
       /opt/local/include
       )
  set( NANOFLANN_INC_SEARCHSUFFIXES
       NanoFLANN 
       include 
       include/nanoflann 
       nanoflann/include
       )
endif()


if (NOT NANOFLANN_FOUND)

  find_path(NANOFLANN_INCLUDE_DIR NAMES nanoflann.hpp PATH_SUFFIXES ${NANOFLANN_INC_SEARCHSUFFIXES}
            PATHS 
            ${NANOFLANN_INC_SEARCHPATH}
            )

  if (NANOFLANN_INCLUDE_DIR)
    set(NANOFLANN_FOUND "YES")
  else(NANOFLANN_INCLUDE_DIR)
    set(NANOFLANN_FOUND "NO")
  endif (NANOFLANN_INCLUDE_DIR)

#  include(FindPackageHandleStandardArgs)
#  find_package_handle_standard_args(NANOFLANN DEFAULT_MSG NANOFLANN_INCLUDE_DIR NANOFLANN_VERSION_OK)

  mark_as_advanced(NANOFLANN_INCLUDE_DIR)

endif(NOT NANOFLANN_FOUND)

IF( NOT NANOFLANN_FOUND )
  SET( NANOFLANN_ROOT_DIR "" CACHE PATH "" )
ENDIF( NOT NANOFLANN_FOUND )