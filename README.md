    **********************************************************************
    * This file is part of
    * 
    * TraumaTech - Computer-based Planning in Trauma Treatment
    * Copyright 2015 3Dim Laboratory s.r.o.
    * All rights reserved.
    * 
    * This software was created with the support of TA CR (https://www.tacr.cz/)
    * under project number TA04011606.
    * 
    * Licensed under the Apache License, Version 2.0 (the "License");
    * you may not use this file except in compliance with the License.
    * You may obtain a copy of the License at
    * 
    *   http://www.apache.org/licenses/LICENSE-2.0
    * 
    * Unless required by applicable law or agreed to in writing, software
    * distributed under the License is distributed on an "AS IS" BASIS,
    * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    * See the License for the specific language governing permissions and
    * limitations under the License.
    *
    **********************************************************************

Table of Contents

1. What is TraumaTech?
2. Available Software Components
3. Contacts


1 What is TraumaTech?
=====================

Main goal of TraumaTech is to support the operating procedures for the trauma
treatment of fractures and bone defects with the use of modern methods 
of computer data processing and computer preoperative planning.

The project focuses on the preoperative phase and the development
of a software system that will assist surgeons in identifying fractures,
computer reconstruction of bone fragments in the correct position and selecting
the appropriate implant.

Using modern image processing techniques and data visualization
software will facilitate the assessment of fracture and creation of a surgery plan.

---

2 Available Software Components
===============================



---

3 Contacts
==========

* 3Dim Laboratory s.r.o.
* E-mail: info@3dim-laboratory.cz
* Web: http://www.3dim-laboratory.cz/
